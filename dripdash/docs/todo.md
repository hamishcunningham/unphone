DripDash: cloud-side data collection and browser front-end(s) for ponics tronics...
===

TODOs:

- bring FakerElf over too
- update clone command in docs
- rename .md to .mkd

Following mostly done?

- tech stack normalisation and documentation
  - update libraries
  - pwa for Vue stuff? e.g. https://cli.vuejs.org/core-plugins/pwa.html
- some additional (scaffolded?) app basics to think about
  - addresses
  - locations, obfuscated locations, areas
- tech stack (to document)
  - Node
  - Express
  - Sequelize (and Epilogue for REST API support for WaterElf?)
  - PostgreSQL or MariaDB
  - Vue
  - Gitlab CI
    - ? docker?
  - AWS EC2
  - VSCode
  - Discourse
- related projects
  - MyHarvest port
  - integration experiments
    - FarmOS / OpenTEAM
    - OpenFarm
    - Discourse
    - OpenFoodNetwork
    - FarmBot (with ponics growbed? wicking bed for spuds?)
