# Installation & Usage

## Prerequisites

Before installing DripDash, you need `nodejs` and `git`. 
```bash
sudo apt-get install nodejs git
```

DripDash is built for Node v10 or higher.

Planning on using DripDash with a PostgreSQL database? You'll also need a database server setup and the connection string handy. Use a local PostgreSQL server for development.

Finally, you'll need an Auth0 account.

### Auth0 Setup

DripDash uses Auth0 for user authentication. We let them handle the complexities of security, password resets and third party OAuth integrations.

Create a new Auth0 Application, selecting "Single Page Application" when prompted.

Under *Settings* for your new app, set the following URLs:
- Allowed Callback URLs
- Allowed Logout URLs
- Allowed Web Origins

For development, these will all be set to `http://localhost:8080`. For production, the configuration will depend on your deployment. Check Auth0's docs for more details.

Secondly, you'll need to create an Auth0 API for the DripDash server.
Set the identifier to something sensible, as you'll need to copy it into your `.env` as the audience.

Set the "Signing Algorithm" to `RS256`.

## Installation

1. To install DripDash, firstly clone this repository.  

```bash
# Clone via SSH (recommended)
git clone git@gitlab.com:hamishcunningham/unphone.git

# Clone via HTTPS
git clone https://gitlab.com/hamishcunningham/unphone.git
```

2. Navigate into the `dripdash` folder then install the required dependencies.

```bash
cd unphone/dripdash
npm install
```

3. Create `.env` and add your database credentials, or use the example. You'll also need to copy in your Auth0 credentials.
    - Domain and Client ID from your SPA Application settings.
    - Audience, otherwise referred to as the Identifier in your Auth0 API Settings.

```bash
cp .env.example .env
nano .env
```

4. Migrate the database (creating the required tables and relations).

```bash
npx prisma migrate dev
```

Dripdash is now installed and ready to rock! Jump to [Usage](#usage)

### Updating
To update DripDash, pull the latest changes then install & migrate.

```bash
git pull
npm install
npx prisma migrate dev
```

### Database Configuration

DripDash is setup out-of-the box to support a local PostgreSQL server for development, logging in as the postgres user. When you run migrate for the first time, prisma should automatically create the database required.

If you're running a PostgreSQL database on the same server as DripDash, check out the instructions [here](postgresql-setup.md).

If you're using a hosted PostgreSQL instance (e.g. AWS), place the connection
string in `.env`. For more information, view prisma's
[setup guide for PostgreSQL](https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch-prisma-migrate-node-postgres#connect-your-database).

## Running DripDash

### Development

Run the development server (including the API and collector).
```bash
npm run serve
```

Run code linting and highlight any errors.
```bash
npm run lint
```

### Production

Create a production-ready build of DripDash
```bash
npm run build
```

Run the production build, on the default port.
```bash
npm run production
```

Run the production build, on a custom port.
```bash
PORT=1234 npm run production
```

> Note: We don't recommend using DripDash with `sudo`, or directly on port 80.  
> Instead, [use a web server such as `nginx` or `apache` as a proxy](nginx-setup.md).

### Backup
The easiest way to backup DripDash completely is to dump the database. The following instructions are for PostgreSQL.
```bash
# Dump the 'dripdash' database to dripdash.bak
sudo -u postgres pg_dump dripdash > dripdash.bak

# Restore from a .bak (into an existing, but empty database)
sudo -u postgres psql dripdash_restored < database.bak
```
### HTTPS
DripDash (and the external collector if enabled) use HTTP by default.  
To use HTTPS, we recommend using NGINX or Apache as a HTTPS proxy, and Let's Encrypt as the CA.

Here's a brief guide to [setting up DripDash on port 80, with HTTPS](nginx-setup.md).
