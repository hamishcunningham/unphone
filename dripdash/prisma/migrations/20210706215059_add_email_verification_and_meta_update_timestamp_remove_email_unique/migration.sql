-- DropIndex
DROP INDEX "User.email_unique";

-- AlterTable
ALTER TABLE "User" ADD COLUMN     "email_verified" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "meta_updated" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP;
