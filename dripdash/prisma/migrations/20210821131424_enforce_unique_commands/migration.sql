/*
  Warnings:

  - A unique constraint covering the columns `[deviceId,subject,action]` on the table `Command` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "Command.deviceId_subject_action_unique" ON "Command"("deviceId", "subject", "action");
