**⚠️ DripDash is still a work in progress, check back for updates.**

![DripDash Header](public/drip-dash-header.svg)

DripDash is an all-in-one aquaponics monitoring, control and data logging tool,
built with Vue on Node.js.

## :memo: About DripDash

Aquaponics is hard. It's a three-way ecosystem with plants, bacteria and fish
living in balanced symbiosis. DripDash, along with the rest of the unPhone project,
aims to make aquaponics easier.

DripDash is constructed in three parts:
- The frontend, constructed with Vue.js for control and monitoring.
- The backend, integrating express, the database and GraphQL API.
- The collector, a separate endpoint for communicating WaterElf devices.

DripDash is part of the [unPhone Project](https://unphone.net/).

## :bookmark_tabs: Documentation

- [Installation & Usage](docs/installation.md)
- [Tech Stack](docs/tech-stack.md)
- [API](docs/API.md)

## :zap: Quick Start Guide

For more detailed instructions, check out the **[Documentation](docs/README.md)**.

**Prerequisites:** `nodejs`, `npm` and `git`. A PostgreSQL server running locally. An Auth0 account with a single page application and an API.

Set "Allowed Callback URLs", "Allowed Logout URLs" and "Allowed Web Origins" to `http://localhost:8080` on your Auth0 SPA Settings.

```bash
# Assuming nodejs and git are already present.

# 1 - Clone the repository.
git clone git@gitlab.com:hamishcunningham/unphone.git
cd unphone/dripdash

# 2 - Install dependencies.
npm install

# 3 - Setup the database connection and Auth0 credentials by editing .env
cp .env.example .env

# 3.1 - Create the required schema in the database.
npx prisma migrate dev

# 4a - Start for development.
npm run serve

# 4b - Alternatively, start for production.
# You'll need to update the Auth0 settings to match your production URLs first.
npm run build
npm run production
```

## :beetle: Issues & Bugs

Found a bug? Feel free to raise an issue or submit a PR and we'll take a look at it.


## :wave: Contributors & Thanks

DripDash is built and maintained by:
- Edward (onfe) - https://onfe.uk