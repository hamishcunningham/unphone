// import {
//   ApolloClient,
//   ApolloLink,
//   HttpLink,
//   InMemoryCache,
//   Observable
// } from "apollo-boost";

import { onError } from "@apollo/client/link/error";
import {
  ApolloClient,
  ApolloLink,
  HttpLink,
  InMemoryCache,
  Observable,
  from
} from "@apollo/client/core";

import Vue from "vue";
import VueApollo from "vue-apollo";
import { getInstance } from "./auth";

const httpLink = new HttpLink({ uri: `${window.location.origin}/api` });

const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors || networkError) {
    document.dispatchEvent(
      new CustomEvent("apollo-status", { detail: "fail" })
    );
  }
});

const authLink = new ApolloLink((operation, forward) => {
  return new Observable(observable => {
    let sub = null;

    document.dispatchEvent(
      new CustomEvent("apollo-status", { detail: "fetching" })
    );

    const authService = getInstance();
    authService.getTokenSilently().then(token => {
      operation.setContext({
        headers: {
          Authorization: `Bearer ${token}`
        }
      });

      sub = forward(operation).subscribe(observable);
    });

    return () => (sub ? sub.unsubscribe() : null);
  });
});

const postLink = new ApolloLink((operation, forward) => {
  return forward(operation).map(response => {
    document.dispatchEvent(
      new CustomEvent("apollo-status", { detail: "fetched" })
    );
    return response;
  });
});

export const apolloClient = new ApolloClient({
  // request -> apply auth -> send http request.
  link: from([postLink, authLink, errorLink, httpLink]),
  cache: new InMemoryCache()
});

Vue.use(VueApollo);

const apolloProvider = new VueApollo({
  defaultClient: apolloClient
});

export default apolloProvider;
