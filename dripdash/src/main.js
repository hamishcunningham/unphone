import Vue from "vue";
import "./plugins/bootstrap-vue";
import "./plugins/font-awesome-vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./plugins/utils";
import VueMeta from "vue-meta";
import apolloProvider from "./plugins/apollo";
import { Auth0Plugin } from "./plugins/auth";

Vue.config.productionTip = false;
Vue.use(VueMeta);

Vue.use(Auth0Plugin, {
  domain: process.env.VUE_APP_AUTH0_DOMAIN,
  clientId: process.env.VUE_APP_AUTH0_CLIENTID,
  audience: process.env.VUE_APP_AUTH0_AUDIENCE,
  onRedirectCallback: appState => {
    router.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname
    );
  }
});

window._dripdash = new Vue({
  router,
  store,
  apolloProvider,
  render: h => h(App)
}).$mount("#app");
