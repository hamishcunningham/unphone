import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import store from "./store";
import { authGuard } from "./plugins/authGuard";

Vue.use(Router);
Vue.use(store);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/add-device",
      name: "Add Device",
      component: () =>
        import(/* webpackChunkName: "add-device" */ "./views/AddDevice.vue"),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/device/:id",
      name: "device",
      component: () =>
        import(/* webpackChunkName: "device" */ "./views/Device.vue"),
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/device/:id/:detail",
      name: "deviceDetail",
      component: () =>
        import(
          /* webpackChunkName: "deviceDetail" */ "./views/DeviceDetail.vue"
        ),
      meta: {
        requiresAuth: true
      }
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // Check that we're authenticated with Auth0.
    return authGuard(to, from, next);
  }
  // No authentication flag on this route. continue.
  return next();
});

export default router;
