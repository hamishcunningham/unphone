const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const express = require("express");
const router = express.Router();

router.all("/:id/", async function(req, res) {

  // if no id was supplied, or it is unreasonably short, error.
  if (!req.params.id || req.params.id.length < 3) {
    res.status(400).send("ID not supplied or invalid.");
    return;
  };

  // search for the device in the database.
  const device = await prisma.device.findUnique({
    where: { id: req.params.id }
  });

  if (device) {
    // device is claimed.

    // insert bed numbers if not present.
    req.body.beds.map((val, idx) => {
      val.bedNumber ??= idx;
    });

    // add the data entry
    await prisma.dataEntry.create({
      data: {
        timestamp: new Date(),
        device: {
          connect: { id: req.params.id }
        },

        waterTemp: req.body.waterTemp,
        airTemp: req.body.airTemp,
        humidity: req.body.humidity,
        light: req.body.light,
        ph: req.body.pH,
        rssi: req.body.rssi,
        beds: req.body.beds ? { create: req.body.beds } : null
      }
    })

    // check if there's any pending commands to send back to the device.
    let pendingCommands = await getPendingCommands(device);

    // if there's no pending commands send a 204 (no content)
    if (pendingCommands.length == 0) {
      res.status(204).send();
      return;
    }

    res.status(200).json(pendingCommands);

  } else {
    // unclaimed device

    // create or update a record for this unclaimed device.
    await prisma.unregisteredDevice.upsert({
      where: { id: req.params.id },
      create: {
        id: req.params.id,
        online: new Date()
      },
      update: {
        id: req.params.id,
        online: new Date()
      }
    })

    res.sendStatus(201);
  }
});

async function getPendingCommands(device) {
  const fiveminsago = new Date(Date.now() - (1000 * 60 * 5))

  let pendingCommands = await prisma.command.findMany({
    where: {
      device: device,
      created: {
        gte: fiveminsago
      }
    },
    select: {
      id: true,
      subject: true,
      action: true
    }
  })

  // delete old pending commands, and the ones we're about to send
  // (so they don't get sent twice).
  await prisma.command.deleteMany({
    where: {
      OR: [
        {
          // it's a command we're about to send.
          id: {
            in: pendingCommands.map(cmd => cmd.id)
          }
        },
        {
          // or it's an old one.
          created: {
            lte: fiveminsago
          }
        }
      ]
    }
  })

  // the target device doesn't need the ids.
  pendingCommands.forEach(cmd => delete cmd.id);

  return pendingCommands;
}

module.exports = router;
