const { loadSchemaSync } = require("@graphql-tools/load");
const { GraphQLFileLoader } = require("@graphql-tools/graphql-file-loader");
const { addResolversToSchema } = require("@graphql-tools/schema");
const { ApolloServer } = require("apollo-server-express");
const { join } = require('path');
const tokens = require("./tokens");
const fetch = require('node-fetch');

const resolvers = require("./resolvers");

const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();


const typeDefs = loadSchemaSync(
  join(__dirname, "schema.graphql"),
  { loaders: [new GraphQLFileLoader()] }
);

const schema = addResolversToSchema({
  schema: typeDefs,
  resolvers: resolvers
});

const apollo = new ApolloServer({
  schema,
  context: async ({ req }) => {
    // on each request...

    // Check if an auth header is present.
    const authHeader = req.headers.authorization?.split(' ');

    let tokenData = null;
    let user = null;

    // Verify the token with Auth0, to see if the user is logged in.
    if (authHeader && authHeader[0] == "Bearer" && authHeader[1]) {
      tokenData = await tokens.verify(authHeader[1])
    }

    // if they're logged in, check if the user exists already.
    if (tokenData) {
      user = await prisma.user.findUnique({
        where: {
          id: tokenData.sub
        }
      });

      // if it doesn't, create a user in our DB to match the Auth0 user.
      if (!user) {
        console.log(`Creating user with ID ${tokenData.sub}`);

        let userData = {
          id: tokenData.sub
        };

        getUserMetadata(req.headers.authorization)
        .then(data => {
          // and if successful we add them to the userData.
          userData.email = data.email || null;
          userData.name = data.name || null;
          userData.picture = data.picture || null;
          userData.email_verified = data.email_verified || false;
        })
        .catch(err => console.log(err))
        .finally( async () => {
          // Finally, we add the user to our database.
          // This can fail if the user has been created (async) in the background.
          try {
            user = await prisma.user.create({ data: userData })
          } catch (e) {
            console.log(`Failed to create user: ${e}`)
          }
        })
      } else if ((!user.email_verified && (Date.now() - user.meta_updated.getTime()) > 1000 * 60) || ((Date.now() - user.meta_updated.getTime()) > 1000 * 60 * 30)) {
        // if the email isn't verified and the metadata is > 1 min old or if the metadata is > 30 mins old.
        // Update the user metadata. (email (incl. verif), picture and name).
        getUserMetadata(req.headers.authorization)
        .then(async data => {
          // update the user metadata.
          await prisma.user.update({
            where: {
              id: tokenData.sub
            },
            data: {
              email: data.email,
              name: data.name,
              picture: data.picture,
              email_verified: data.email_verified,
              meta_updated: new Date()
            }
          })
        })
        .catch(async err => {
          console.error(err);

          // even if we failed to get the data, we must still update
          // the meta datetime so we don't exceed API limits.
          await prisma.user.update({
            where: {
              id: tokenData.sub
            },
            data: {
              meta_updated: new Date()
            }
          })
        })
      }
    }

    // Immediately (before the promise above has necessarily resolved) we return
    // the new context. If the user profile is required by the resolver it'll
    // await until it resolves.
    return { user, prisma };
  }
});

function getUserMetadata(token) {
  // we attempt to fetch the user's email, picture and name from Auth0.
  return fetch(`https://${process.env.VUE_APP_AUTH0_DOMAIN}/userinfo`, {
    method: "GET",
    headers: {
      "Authorization": token
    }
  })
  .then(res => res.json())
}

module.exports = apollo;