const jwt = require("jsonwebtoken");
const jwksClient = require('jwks-rsa');


const client = jwksClient({
  jwksUri: `https://${process.env.VUE_APP_AUTH0_DOMAIN}/.well-known/jwks.json`
});

function getKey(header, cb){
  client.getSigningKey(header.kid, function(err, key) {
    var signingKey = key.publicKey || key.rsaPublicKey;
    cb(null, signingKey);
  });
}

const options = {
  audience: process.env.VUE_APP_AUTH0_AUDIENCE,
  issuer: `https://${process.env.VUE_APP_AUTH0_DOMAIN}/`,
  algorithms: ['RS256']
};

module.exports = {
  verify(token) {
    return new Promise((res, rej) => {
      try {
        jwt.verify(token, getKey, options, (err, decoded) => {
          if(err) {
            return rej(err);
          }
          res(decoded);
        });
      } catch (e) {
        rej(e);
      }
    });
  }
};
