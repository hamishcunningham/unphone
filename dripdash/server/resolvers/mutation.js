const { UserInputError } = require("apollo-server-express");

const Mutation = {
  createGroup: async (_, { name }, ctx) => {
    if (!ctx.user) return null;
    if (name.length === 0) return null;
    const group = await ctx.prisma.group.create({
      data: {
        name,
        members: {
          connect: { id: ctx.user.id }
        }
      }
    })
    return group;
  },
  leaveGroup: async (_, { id }, ctx) => {
    // todo: it's possible to leave a group whilst leaving your devices in it.
    if (!ctx.user) return null;
    const group = await ctx.prisma.group.update({
      where: { id },
      data: {
        members: {
          disconnect: [ { id: ctx.user.id }]
        } 
      },
      include: {
        members: {
          select: {
            id: true
          }
        }
      }
    }).catch( () => {
      return null;
    });

    // if the group is now empty, delete it.
    if (group && group.members.length === 0) {
      await ctx.prisma.group.delete({ where: { id }});
    }
    return group;
  },

  addUserToGroup: async (_, { email, groupId }, ctx) => {
    // As user emails are not guaranteed unique we ensure we only add users to
    // groups who have verified their email addresses.
    let usersToAdd = await ctx.prisma.user.findMany({
      where: {
        email,
        email_verified: true
      }
    });

    if (usersToAdd.length < 1) {
      throw new UserInputError("No users found to add to group.");
    }

    let userIds = usersToAdd.map(user => ({ id: user.id }))

    return await ctx.prisma.group.update({
      where: { id: groupId },
      data: {
        members: {
          connect: userIds
        }
      }
    })
  },

  addDeviceToGroup: async (_, { deviceId, groupId }, ctx) => {
    return await ctx.prisma.group.update({
      where: { id: groupId },
      data: {
        devices: {
          connect: { id: deviceId }
        }
      }
    })
  },

  removeDeviceFromGroup: async (_, { deviceId, groupId }, ctx) => {
    return await ctx.prisma.group.update({
      where: { id: groupId },
      data: {
        devices: {
          disconnect: [{ id: deviceId }]
        }
      }
    })
  },

  addDevice: async (_, { id }, ctx) => {
    if (!ctx.user) return null;
    
    await ctx.prisma.unregisteredDevice.delete({
      where: { id }
    })

    return await ctx.prisma.device.create({
      data: {
        id,
        owner: {
          connect: { id: ctx.user.id }
        }
      }
    })
  },

  removeDevice: async (_, { id }, ctx) => {
    await ctx.prisma.bed.deleteMany({
      where: {
        dataEntry: {
          deviceId: id
        }
      }
    })
    await ctx.prisma.dataEntry.deleteMany({ where: { deviceId: id}})
    const result = await ctx.prisma.device.delete({ where: { id } })
    return !!result;
  },

  renameDevice: async (_, { id, name }, ctx) => {
    return await ctx.prisma.device.update({
      where: { id },
      data: { name }
    })
  },

  setDeviceBedState: async (_, {device, bed, state}, ctx) => {
    return await ctx.prisma.command.upsert({
      where: {
        // composite unique of device, subject and action
        deviceId_subject_action: {
          deviceId: device,
          subject: `bed ${bed}`,
          action: `${state}`
        }
      },

      update: {
        // if it's the same as a command already there, just update the created to now.
        created: new Date()
      },

      // otherwise create a new entry.
      create: {
        device: {
          connect: { id: device }
        },

        subject: `bed ${bed}`,
        action: `${state}`
      }
    })
  }
}

module.exports = Mutation;