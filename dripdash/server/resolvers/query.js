const Query = {
  self: async (_, {}, ctx) => {
    const user = await ctx.user;
    return user;
  },
  unregisteredDevices: async (_, {}, ctx) => {
    if (!ctx.user) return null;
    const thirtyminsago = new Date(Date.now() - (1000 * 60 * 30))
    const devices = await ctx.prisma.unregisteredDevice.findMany({
      where: {
        online: {
          gte: thirtyminsago
        }
      }
    })

    await ctx.prisma.unregisteredDevice.deleteMany({
      where: {
        online: {
          lt: thirtyminsago
        }
      }
    })

    return devices;
  },
  device: async (_, { id }, ctx) => {
    if (!ctx.user) return null;
    return await ctx.prisma.device.findUnique({ where: { id } })
  }
}

module.exports = Query;