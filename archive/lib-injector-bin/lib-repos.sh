# lib-repos.sh
#
# a list of all the library repos we use, for the build to patch
# for the unPhone IOExpander

# git repos that need patching
https://github.com/adafruit/Adafruit-GFX-Library  patch # graphics abstraction
https://github.com/adafruit/Adafruit_HX8357_Library patch # LCD (spin6)
https://github.com/adafruit/Adafruit_STMPE610     patch # touchscreen (spin6)
#https://github.com/PaulStoffregen/XPT2046_Touchscreen patch # touchscreen (spin7)
https://github.com/garethhcoleman/XPT2046_Touchscreen patch # updated touchscreen lib with calibrated Batt Voltage for spin 7+
https://github.com/adafruit/SdFat                 patch # used by image reader
https://github.com/adafruit/Adafruit_BusIO.git    patch # I2C/SPI abstraction
# old LMIC/LoRa:  https://github.com/mcci-catena/arduino-lmic.git patch
https://github.com/mcci-catena/arduino-lorawan    patch # new MCCI LoRaWAN
https://github.com/mcci-catena/Catena-mcciadk.git patch # MCCI porting kit
https://github.com/mcci-catena/arduino-lmic.git   patch # MCCI fork of LMIC

# esp32 core package libraries that need patching
# TODO delete old: ../sdks/Arduino/hardware/espressif/esp32/libraries/SD patch # SD cards
../../the-internet-of-things/support/tooling/Arduino/hardware/espressif/esp32/libraries/SD patch

# libs that don't need patching
https://github.com/adafruit/Adafruit_LSM303DLHC    # accelerometer (spin6)
https://github.com/adafruit/Adafruit_LSM9DS1       # accelerometer (spin7)
https://github.com/adafruit/Adafruit_LIS3MDL       # magnetometer (spin7)
https://github.com/adafruit/Adafruit_LSM6DS        # accelerometer (spin9)
https://github.com/adafruit/Adafruit_Sensor        # high-level sensor abst
https://github.com/sui77/rc-switch                 # 433 MHz switching
https://github.com/marcoschwartz/aREST             # REST lib used by robocar
https://github.com/adafruit/Adafruit_Motor_Shield_V2_Library # motor feather
https://github.com/beegee-tokyo/DHTesp             # temp and humidity sensor
https://github.com/adafruit/Adafruit_TSL2561       # light/IR sensor (square)
https://github.com/adafruit/Adafruit_TSL2591_Library # light/IR sensor (round)
https://github.com/adafruit/Adafruit_NeoPixel      # neopixels
https://github.com/gmag11/WifiLocation             # google's geolocation API
GP2Y1010_DustSensor                                # dust sensor
            # (used to be at: https://github.com/nara256/GP2Y1010_DustSensor)
https://github.com/me-no-dev/ESPAsyncWebServer.git # asynchronous web svr
https://github.com/me-no-dev/AsyncTCP.git          # async tcp used by svr

# https://github.com/nkolban/esp32-snippets.git    # Neil Kolban's library

https://github.com/Arduino-IRremote/Arduino-IRremote.git # IR remotes
# replaces older version which was not a usual lib format
# https://github.com/SensorsIot/Definitive-Guide-to-IR/tree/master/ESP32-IRremote

# TODO -b development
https://github.com/tzapu/WiFiManager.git           # wifi connection mgmt

https://github.com/PaulStoffregen/OneWire          # 1 wire comms
https://github.com/bblanchon/ArduinoJson           # manipulate JSON data
https://github.com/milesburton/Arduino-Temperature-Control-Library # DallasTem
https://github.com/openenergymonitor/EmonLib       # open energy monitor

# latest version requires SPIFlash and SdFat, but these don't compile on IDE
# 1.9.0... ho hum (TODO: work on IDF 4, and with patch on IDE 1.9.0)
https://github.com/adafruit/Adafruit_ImageReader   # bmp draw to LCD library
https://github.com/adafruit/Adafruit_SPIFlash      # used by image reader
https://github.com/adafruit/Adafruit_EPD           # ditto

https://github.com/adafruit/Adafruit_DotStar       # dotstar LEDs
https://github.com/adafruit/Adafruit_DotStarMatrix # dotstar matrix featherwing
https://github.com/adafruit/Adafruit_NeoMatrix     # NeoPix matrices

ESPWebServer                                       # simple web server ex 8266
https://github.com/adafruit/Adafruit_IO_Arduino    # adafruit cloud API
https://github.com/adafruit/Adafruit_MQTT_Library  # used by adafruit.io
https://github.com/arduino-libraries/ArduinoHttpClient # ditto
https://github.com/adafruit/Adafruit_SSD1306       # oled featherwing display
https://github.com/JChristensen/movingAvg          # simple running averages

https://github.com/adafruit/Adafruit_VS1053_Library # MP3/synth/ogg (no patch)
https://github.com/adafruit/Adafruit_INA219        # power measurement library
