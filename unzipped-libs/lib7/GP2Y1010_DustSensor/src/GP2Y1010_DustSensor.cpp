// GP2Y1010_DustSensor.cpp

// DON'T EDIT FROM HERE TO "END"
// (this stuff is injected to support unPhone's TCA9555 IO expander chip)
#ifndef UNPHONE_TCA_INJECTED
#define UNPHONE_TCA_INJECTED
#include <Wire.h>  // (we need digitalRead etc. defined before redefining)
class unPhoneTCA { // this is an excerpt of the full definition that...
public:            // ...just declares the injected methods
  static void pinMode(uint8_t pin, uint8_t mode);
  static void digitalWrite(uint8_t pin, uint8_t value);
  static uint8_t digitalRead(uint8_t pin);
};
#define digitalWrite unPhoneTCA::digitalWrite // call...
#define digitalRead  unPhoneTCA::digitalRead  // ...ours...
#define pinMode      unPhoneTCA::pinMode      // ...please
#endif
// END -- normal library code follows

/*
  GP2Y1010_DustSensor.cpp - SHARP GP2Y1010AU0F Dust sensor library for ESP-WROOM-02/32(esp8266/ESP32) or Arduino
  version 0.4
  
  License MIT
*/

#include "GP2Y1010_DustSensor.h"
#include "Arduino.h"
#include <math.h>

// public

GP2Y1010_DustSensor::GP2Y1010_DustSensor(){
}

GP2Y1010_DustSensor::GP2Y1010_DustSensor(int ledPin, int measurePin){
	begin(ledPin,measurePin);

}

GP2Y1010_DustSensor::~GP2Y1010_DustSensor(){
}

void GP2Y1010_DustSensor::begin(int ledPin, int measurePin){
	led_pin = ledPin;
	measure_pin = measurePin;
	pinMode(led_pin, OUTPUT);
}

void GP2Y1010_DustSensor::setADCbit(int bit){
	analog_bit = bit;
	analog_bit_num = pow(2., (double)analog_bit);
}

int GP2Y1010_DustSensor::getADCbit(){
	return analog_bit;
}

void GP2Y1010_DustSensor::setInputVolts(float volts){
	inputvolts = volts;
}

float GP2Y1010_DustSensor::getInputVolts(){
	return inputvolts;
}

float GP2Y1010_DustSensor::getDustDensity() {

	digitalWrite(led_pin, LOW);
	delayMicroseconds(SAMPLINGTIME);

	float mesured = analogRead(measure_pin);	//read analog pin / Dust value
	delayMicroseconds(DELTATIME);

	digitalWrite(led_pin, HIGH);
	delayMicroseconds(SLEEPTIME);

	// culc dust density
	float dust = (0.17 * (mesured * (inputvolts / analog_bit_num)) - 0.1) * 1000.;
	if( dust<0 )	dust=0.;
	
	return dust;
}
