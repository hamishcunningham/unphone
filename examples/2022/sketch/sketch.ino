// sketch.ino

#include <Arduino.h>
#include <freertos/task.h>
#include <Update.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <WiFiManager.h> // force inclusion to fix pio LDF WebServer.h issue
#include <HTTPClient.h>
#include <ESPmDNS.h>
#include "private.h"
#include "sketch.h"
#include "unphone.h"
#include "UIController.h"

String apSSID = String("2022-");         // SSID of the AP
String apPassword = _DEFAULT_AP_KEY;     // passkey for the AP
const char BUILD_VERSION[] =
  ("__sketch__ " __DATE__ " " __TIME__); // build data

bool useWifi = true;
WiFiMulti wifiMulti;
HTTPClient http;
UIController *uiCont;
int firmwareVersion = 1; // keep up-to-date! (used to check for updates)
int loopIter = 0;

void setup() {
  // say hi, store MAC, blink etc.
  Serial.begin(115200);
  Serial.printf("Starting: %s\n", BUILD_VERSION);
  Serial.printf("Hello from 2022...\n");
  getMAC(MAC_ADDRESS);          // store the MAC address
  apSSID.concat(MAC_ADDRESS);   // add the MAC to the AP SSID
  Serial.printf("\nsetup...\nESP32 MAC = %s\n", MAC_ADDRESS);

  unPhone::begin();

  // power management
  unPhone::printWakeupReason(); // what woke us up?
  unPhone::checkPowerSwitch();  // if power switch is off, shutdown
  Serial.printf("battery voltage = %3.3f\n", unPhone::batteryVoltage());
  unPhone::expanderPower(true); // turn expander power on

  // flash the internal RGB LED and then the IR_LEDs
  unPhone::rgb(0, 0, 0); delay(300); unPhone::rgb(0, 0, 1); delay(300);
  unPhone::rgb(0, 1, 1); delay(300); unPhone::rgb(1, 0, 1); delay(300);
  unPhone::rgb(1, 1, 0); delay(300); unPhone::rgb(1, 1, 1); delay(300);
  for(uint8_t i = 0; i<4; i++) {
    digitalWrite(unPhone::IR_LEDS, HIGH); delay(300);
    digitalWrite(unPhone::IR_LEDS, LOW); delay(300);
  }

  /*
  TODO
  (this is the old provisioning cycle, but we've replaced it temporarily with
  wifiMulti as below)

  // wifi provisioning
  Serial.printf("doing wifi manager\n");
  uiCont->message("(doing wifi management)");
  joinmeManageWiFi(apSSID.c_str(), apPassword.c_str());
  uiCont->message("");
  Serial.printf("wifi manager done\n\n");
  uiCont->redraw();          // perhaps we're on wifi now, redraw

  // init the web server, say hello
  initWebServer();
  Serial.printf("firmware is at version %d\n", firmwareVersion);

  // check for and perform firmware updates as needed
  delay(2000); // let wifi settle
  uiCont->message("(firmware update check)");
  joinmeOTAUpdate(
    firmwareVersion, _GITLAB_PROJ_ID, "", // use _GITLAB_TOKEN if private repo
    "examples%2FBigDemoIDF%2Ffirmware%2F"
  );
  uiCont->message("");
  */

  // display the first screen
  uiCont = new UIController(ui_configure);
  if(uiCont == NULL || !uiCont->begin())
    E("WARNING: ui.begin failed!\n")

  // buzz a bit
  for(int i = 0; i < 3; i++) {
    unPhone::vibe(true);  delay(150);
    unPhone::vibe(false); delay(150);
  }

  // get a connection
  if(useWifi) {
/* TODO probably time to start using std:: to manage this!
#include <string>
#include <map>
#include <iterator>
using namespace std;
map<string, string> ssidMap;
ssidMap.insert(make_pair(_MULTI_SSID1, _MULTI_KEY1));
...
map<string, string>::iterator it = ssidMap.begin();
for (pair<string, string> element : ssidMap) {
  string name = element.first;
  string key = element.second;
  Serial.printf("wifiMulti.addAP %s\n", name);
  wifiMulti.addAP(name, key);
}
*/
#ifdef _MULTI_SSID1
    Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID1);
    wifiMulti.addAP(_MULTI_SSID1, _MULTI_KEY1);
#endif
#ifdef _MULTI_SSID2
    Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID2);
    wifiMulti.addAP(_MULTI_SSID2, _MULTI_KEY2);
#endif
#ifdef _MULTI_SSID3
    Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID3);
    wifiMulti.addAP(_MULTI_SSID3, _MULTI_KEY3);
#endif
#ifdef _MULTI_SSID4
    Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID4);
    wifiMulti.addAP(_MULTI_SSID4, _MULTI_KEY4);
#endif
#ifdef _MULTI_SSID5
    Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID5);
    wifiMulti.addAP(_MULTI_SSID5, _MULTI_KEY5);
#endif
#ifdef _MULTI_SSID6
    Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID6);
    wifiMulti.addAP(_MULTI_SSID6, _MULTI_KEY6);
#endif
#ifdef _MULTI_SSID7
    Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID7);
    wifiMulti.addAP(_MULTI_SSID7, _MULTI_KEY7);
#endif
    Serial.print("trying to connect to wifi...");
    while(true) {
      if ((wifiMulti.run() == WL_CONNECTED)) {
        Serial.println("connected");
        break;
      }
      Serial.print(".");
      delay(100);
    }
    if(!MDNS.begin("sketch"))
      Serial.println("Error setting up MDNS responder!");
    initWebServer();
  }

  UIController::provisioned = true; // tell the controller done provisioning
  uiCont->redraw();             // perhaps we're on wifi now, redraw


  // send a LoRaWAN message to TTN
  Serial.printf("Calling unPhone::loraSetup()\n");
  unPhone::loraSetup(); // init the board and queue a hello message

  // WiFi.printDiag(Serial);
  Serial.println("Done with setup()");
}

bool sentLoopMessage = false;   // loop lora message...
int loopMessageIter = 0;        // ...control

void loop() {
  D("entering main loop\n")
  while(1) {
    micros(); // update overflow
    uiCont->run();
    unPhone::checkPowerSwitch(); // if power switch is off shutdown

    // allow the protocol CPU IDLE task to run periodically
    loopIter++;
    if(loopIter % 2500 == 0) {
      if(loopIter % 25000 == 0) {
        D("completed loop %d, yielding 1000th time since last\n", loopIter)

        // send a lora message at first idle slot (to avoid conflicting with
        // the setup message)
        if(!sentLoopMessage) {
          Serial.println("sending loop lora message");
          char buf[1024];
          sprintf(buf, "hello from the loop, MAC is %s", MAC_ADDRESS);
          unPhone::loraSend(buf);
          sentLoopMessage = true;
        }
        delay(100); // 100 appears min to allow IDLE task to fire
      }
    }

    // register button presses
    if(unPhone::button1())
      Serial.println("button1");
    if(unPhone::button2())
      Serial.println("button2");
    if(unPhone::button3())
      Serial.println("button3");

    unPhone::loraLoop(); // service lora transactions
  }
}
