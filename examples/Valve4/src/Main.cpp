// Main.cpp

#include <esp_system.h>
#include <sdkconfig.h>
#include <Arduino.h>
#include <Adafruit_MotorShield.h>

/////////////////////////////////////////////////////////////////////////////
// utilities
//
// delay/yield macros
#define WAIT_A_SEC   vTaskDelay(    1000/portTICK_PERIOD_MS); // 1 second
#define WAIT_SECS(n) vTaskDelay((n*1000)/portTICK_PERIOD_MS); // n seconds
#define WAIT_MS(n)   vTaskDelay(       n/portTICK_PERIOD_MS); // n millis

/* TODO
- physical assembly
  - the motor and wiring is too exposed
  - move to a single nut driver (perhaps with washer glued to the underside?)
  - reduce overall vertical height
- valve timing (in flowControlTask?)
  - Valve::get/setDrainPeriod(uint16_t seconds = 30);
  - Valve::get/setFillPeriod(uint16_t seconds = 30);
  - Valve::begin() {
    - set timer (drain seconds)
    - set timer (fill seconds)
  - flow control
    - uint8_t numValves = 3;
    - allocate the valves
    - begin one-shot timers for each valve, spaced by drain period + fill
      period + x
- why the instability around motor running and Serial?
  - this appears to be entirely due to build inconsistencies; the Arduino IDE
    always seems to work, but PIO often doesn't and IDF sometimes doesn't; see
    https://github.com/platformio/platform-espressif32/issues/411
- Makefile
  - delete some libraries, use lib/
  - remove reliance on separate IDF, just reference arduino-esp32
*/

/////////////////////////////////////////////////////////////////////////////
// gpio interrupt events
static xQueueHandle gpio_evt_queue = NULL;   // switch event queue
static void IRAM_ATTR gpio_isr_handler(void *arg) {
  uint32_t gpio_num = (uint32_t) arg;
  xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}
static bool isLow(uint8_t gpioNum) { return digitalRead(gpioNum) == LOW; }

/////////////////////////////////////////////////////////////////////////////
// valve abstraction
class Valve {
public:
  Adafruit_MotorShield AFMS;            // featherwing motor driver
  Adafruit_DCMotor *myMotor;            // the motor abstraction instance

  uint8_t travelTop;    // GPIO of sensor switch at travel top
  uint8_t travelBottom; // GPIO of sensor switch at travel bottom
  uint8_t trigger;      // GPIO of state change trigger switch
  uint8_t motorNumber;  // featherwing connection number

  typedef enum { UNKNOWN, OPEN, CLOSED, OPENING, CLOSING } State; // lifecycle
  const char *stateName(); // usual C++ enum nightmare... maintain names below
  State state = UNKNOWN; // state is not known when we boot up
  const uint8_t MOTOR_START_SPEED = 255; // what speed to start from

  Valve() { };                                  // construction
  Valve(uint8_t, uint8_t, uint8_t, uint8_t);    // construction
  void begin();                                 // task initialisation
  // TODO these names are a bit misleading? only flip() is external API
  void open();                                  // shift to draining
  void close();                                 // shift to filling
  void run(uint8_t);                            // run the motor
  void complete();                              // finish a state change
  void flip();                                  // flip state
  void flip(bool, bool);                        // flip state
  bool atTop();                                 // travel switch pressed
  bool atBottom();                              // travel switch pressed
  bool triggered();                             // trigger switch pressed
  esp_err_t configGpio(uint64_t);               // set up GPIO
  bool handleGpioEvent(uint32_t);               // travel & trigger switches
  long lastTrigger = -1;                        // millis at previous trigger
  const int TRIGGER_DOUBLECLICK = 400;          // < than this is doubleclick

  static void gpioEventQueueTask(void *);       // drains the GPIO event queue
};

const char *Valve::stateName() {
  switch(state) {
    case UNKNOWN: return "UNKNOWN";
    case OPEN:    return "OPEN";
    case CLOSED:  return "CLOSED";
    case OPENING: return "OPENING";
    case CLOSING: return "CLOSING";
    default:      return "UNRECOGNISED";
  }
}

Valve::Valve(
  uint8_t travelTop, uint8_t travelBottom, uint8_t trigger, uint8_t motorNumber
) {
  // the state sensing switches
  this->travelTop = travelTop;
  this->travelBottom = travelBottom;
  this->trigger = trigger;

  // the motor and driver
  this->motorNumber = motorNumber;
  AFMS = Adafruit_MotorShield();
  myMotor = AFMS.getMotor(motorNumber);
} // Valve::Valve(...)

void Valve::begin() {
  // init the motor controller featherwing
  AFMS.begin();

  // configure the travel swiches to interrupt on falling edge
  configGpio(
    (1ULL << travelTop) | (1ULL << travelBottom) | (1ULL << trigger)
  );

  // create a queue to handle gpio event from isr
  gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));

  // install gpio isr service and hook up isr handlers
  gpio_install_isr_service(0); // prints an error if already there; ignore!
  gpio_isr_handler_add(
    (gpio_num_t) travelTop,    gpio_isr_handler, (void *) travelTop
  );
  gpio_isr_handler_add(
    (gpio_num_t) travelBottom, gpio_isr_handler, (void *) travelBottom
  );
  gpio_isr_handler_add(
    (gpio_num_t) trigger,      gpio_isr_handler, (void *) trigger
  );

  Serial.printf(
    "minimum free heap size: %d bytes\n", esp_get_minimum_free_heap_size()
  );

  xTaskCreate(
    &gpioEventQueueTask,
    "gpioEventQueueTask",
    configMINIMAL_STACK_SIZE + 1024, // min is too small, triggers canary
    this, // pass this so the task can refer to a valve instance
    (configMAX_PRIORITIES - 1),
    NULL
  );

  // make sure motor is off, and guess state
  myMotor->run(RELEASE);
  state = UNKNOWN;
  bool t = atTop(), b = atBottom();
  if(! (t && b) ) { // both closed!
    if(t)
      state = OPEN;
    else if(b)
      state = CLOSED;
  }
} // Valve::begin()

// flow control task
void Valve::gpioEventQueueTask(void *pvParameter) {
  Valve *vp = (Valve *) pvParameter;

  while(1) {
    // deal with events from the travel sensors and trigger switch
    uint32_t gpioNum;
    if(xQueueReceive(gpio_evt_queue, &gpioNum, (TickType_t) 0)) {
      xQueueReset(gpio_evt_queue); // dump any pending events (debounce #1)
      Serial.printf("event on %d...", gpioNum);

      if(vp->handleGpioEvent(gpioNum)) // ask the valve to deal with the event
        Serial.printf("...gpio[%d] interrupt processed\n", gpioNum);
      else
        continue;
    }

    vTaskDelay(1); // yield (at least one tick)
  }
}

// travel & trigger switch event handler; false on discard
bool Valve::handleGpioEvent(uint32_t gpioNum) {
  // verify switch state (debounce #2A)
  bool
    top  = ( atTop()     && atTop()     ),
    bot  = ( atBottom()  && atBottom()  ),
    trig = ( triggered() && triggered() );
  if(! (top || bot || trig) ) { // dump this one (= a phantom event)
    Serial.printf("...%d discarded\n", gpioNum);
    return false;
  }
  Serial.printf(
    "[top(%d)=%d, bot(%d)=%d, trig(%d)=%d] ",
    travelTop, top, travelBottom, bot, trigger, trig
  );

  // if we're moving and have hit the end, stop
  if( (state == OPENING && top) || (state == CLOSING && bot) )
    complete();
  else if(trig) { // change state if appropriate
    long now = millis();
    if( (lastTrigger > -1) && (now - lastTrigger) < TRIGGER_DOUBLECLICK )
      myMotor->run(RELEASE); // effectively a PAUSE state; single click resumes
    else
      flip(top, bot);

    lastTrigger = now;
  }

  return true;
} // Valve::handleGpioEvent()

// end a state change
void Valve::complete() {
  myMotor->run(RELEASE);
  switch(state) {
    case OPENING: state = OPEN;   break;
    case CLOSING: state = CLOSED; break;
    default:                      break;
  }
  Serial.printf("state is %s; ", stateName());
}

// if we're draining, close; if we're filling, open
void Valve::flip() {
  flip(( atTop() && atTop() ), ( atBottom() && atBottom() )); // (debounce #2B)
}

// if we're draining, close; if we're filling, open
void Valve::flip(bool top, bool bot) {
  switch(state) {
    case OPENING:
    case OPEN:    if(!bot) close(); break;
    case CLOSING:
    case CLOSED:  if(!top) open();  break;
    case UNKNOWN: if(!top) open();  break;
  }
  Serial.printf("state is %s; ", stateName());
}

// read the switches
bool Valve::atTop()     { return isLow(travelTop);     }
bool Valve::atBottom()  { return isLow(travelBottom);  }
bool Valve::triggered() { return isLow(trigger);       }

// movement (motor) control
void Valve::open()  { state = OPENING; run(BACKWARD); }
void Valve::close() { state = CLOSING; run(FORWARD); }
void Valve::run(uint8_t direction) {
  myMotor->setSpeed(MOTOR_START_SPEED);
  myMotor->run(direction);
}

// configure the switch pins (INPUT, falling edge interrupts)
esp_err_t Valve::configGpio(uint64_t pinsBitMask) {
  gpio_config_t io_conf;                        // params for switches
  io_conf.mode = GPIO_MODE_INPUT;               // set as input mode
  io_conf.pin_bit_mask = pinsBitMask;           // bit mask of pins to set
  io_conf.pull_up_en = GPIO_PULLUP_DISABLE;     // disable pull-up mode
  io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE; // disable pull-down mode
  io_conf.intr_type = GPIO_INTR_NEGEDGE;        // interrupt on falling edge
  return gpio_config(&io_conf);                 // do the configuration
}

Valve v = Valve(GPIO_NUM_14, GPIO_NUM_32, GPIO_NUM_21, 1);

/////////////////////////////////////////////////////////////////////////////
// flow control abstraction
class FlowController {
  static const uint8_t MAX_VALVES = 9; // = 3 stacked motor feathers + i2c exp
  static const uint8_t DEFAULT_NUM_VALVES = 3; // single motor driver board
  const uint16_t DEFAULT_VALVE_PERIODS[2] = { 45, 15 }; // flood, drain (mins)
  const uint8_t DEFAULT_VALVE_PINS[DEFAULT_NUM_VALVES][4] {
    { GPIO_NUM_14, GPIO_NUM_32, GPIO_NUM_21, 1 }, // top, bot, trig, motor #
    { GPIO_NUM_27, GPIO_NUM_12, GPIO_NUM_13, 2 }, // top, bot, trig, motor #
    { GPIO_NUM_4,  GPIO_NUM_36, GPIO_NUM_39, 3 }, // top, bot, trig, motor #
  };

  uint8_t numValves = DEFAULT_NUM_VALVES;
  Valve valves[MAX_VALVES];
  FlowController(uint8_t, uint16_t[], uint8_t[]);
  void begin();
};
FlowController::FlowController(
  uint8_t numValves, uint16_t valvePeriods[], uint8_t valvePins[]
) {
  this->numValves = numValves;
}
void FlowController::begin() {
  uint8_t i = 0;
  for(uint8_t i = 0; i < numValves; i++) {
    // TODO valves[i] = Valve(valvePins[0], valvePins[1], valvePins[2], valvePins[3]);
  }
  // TODO for( ; i < MAX_VALVES; i++) valves[i] = NULL;
}

/////////////////////////////////////////////////////////////////////////////
// internal LED
//
// LED pin to blink:
// you can run 'make menuconfig' to choose the GPIO to blink,
// or you can edit the following line and set a number here
#ifndef CONFIG_BLINK_GPIO
#define BLINK_GPIO (gpio_num_t) 13
#else
#define BLINK_GPIO (gpio_num_t) CONFIG_BLINK_GPIO
#endif

// LED blink task
void blinkTask(void *pvParameter)
{
  pinMode(BLINK_GPIO, OUTPUT);
  while(1) {
    gpio_set_level(BLINK_GPIO, 0); // blink off (output low)
    WAIT_A_SEC

    gpio_set_level(BLINK_GPIO, 1); // blink on (output high)
    WAIT_A_SEC
  }
}

/////////////////////////////////////////////////////////////////////////////
// main entry points
void setup() {
  Serial.begin(115200);

  // the blink example
  xTaskCreate(
    &blinkTask, "blinkTask", configMINIMAL_STACK_SIZE,
    NULL, tskIDLE_PRIORITY + 1, NULL
  );

  // IDF version
  Serial.printf(
    "\ninternal IDF version: %d.%d.%d\n",
    ESP_IDF_VERSION_MAJOR, ESP_IDF_VERSION_MINOR, ESP_IDF_VERSION_PATCH
  );

  // the flow control valves
  v.begin();
}
void loop() {
  Serial.printf(
    "valve (with motor %d) state = %s\n", v.motorNumber, v.stateName()
  );
  WAIT_SECS(9)

  // run self-test if all switches are closed
  if(v.atTop() && v.atBottom() && v.triggered()) {
    Serial.println("running test routine...");
  }
}
