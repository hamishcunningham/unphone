// main.c: PlatformIO / ESP-Prog debugging example

#include <stdio.h>
#include "esp_log.h"

int thing = 23;

void app_main() {
    ESP_LOGI("thing", "blah blah");
    printf("hello, thing = %d!\n", thing);
}