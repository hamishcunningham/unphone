#!/bin/bash
# workarounds for bugs in pio build

[ -L lib ] && rm lib
[ -e src/private.h ] || cp $HOME/the-internet-of-things/support/private-template.h src/private.h
[ -e lib ] || ln -s $HOME/unphone/lib
