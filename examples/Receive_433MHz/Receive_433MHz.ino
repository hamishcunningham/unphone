/*
  Simple example for receiving
  adapted slightly by gareth coleman 2022
  
  433MHz Receiver barely works when powered by 3.3V
  So connect VCC to 5V USB power for better results
  This will slightly overload the GPIO pin with 5V instead of the normal 3.3V
  officialy unsupported, Expressif state "out of spec and we cannot guarantee
  the long-term stability of a solution like that."

  However, for short testing runs, seems to work.
  
  https://github.com/sui77/rc-switch/
*/

#include <RCSwitch.h>
RCSwitch myRadio = RCSwitch();

void setup() {
  Serial.begin(115200);
  myRadio.enableReceive(digitalPinToInterrupt(21));  // Receiver on pin #21
  Serial.println("Started 433MHz receiver");
}

void loop() {
  if (myRadio.available()) {
    Serial.print("Received ");
    Serial.println(myRadio.getReceivedValue());
    myRadio.resetAvailable();
  }
}
