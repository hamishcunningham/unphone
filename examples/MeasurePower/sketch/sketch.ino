// sketch.ino
// adapted by Gareth Coleman from ina_poweroled.ino by Tony DiCola (modded by ladyada)
// Released under a MIT license: http://opensource.org/licenses/MIT

#include <unphone.h>
#include <Adafruit_INA219.h>

Adafruit_INA219 ina219;

void setup()
{
  unPhone::begin();
  unPhone::expanderPower(true); // turn expander power on
  unPhone::tftp->setTextSize(3);
  unPhone::tftp->fillScreen(HX8357_BLACK);
  unPhone::tftp->drawRect(0, 0, 320, 480, HX8357_WHITE);
  unPhone::tftp->setCursor(6, 10);
  // unPhone::tftp->setTextColor(HX8357_RED);
  unPhone::tftp->print("Power Measurement");
  unPhone::tftp->setTextSize(4);
  //delay(10);    // allow INA219 time to initialise if needed straight after expander power on!
  if (!ina219.begin())
  {
    Serial.println("Failed to find INA219 chip");
    unPhone::vibe(true);
    unPhone::rgb(1, 0, 0); // Light the internal LED red
    delay(500);
    unPhone::vibe(false);
    while (1)
      ;
  }
  unPhone::rgb(0, 1, 0); // flash the internal LED green
  unPhone::vibe(true);
  delay(300);
  unPhone::rgb(0, 0, 0);
  unPhone::vibe(false);
  delay(300);
  unPhone::rgb(0, 1, 0);
  unPhone::vibe(true);
  delay(300);
  unPhone::rgb(0, 0, 0);
  unPhone::vibe(false);
  delay(300);
  unPhone::backlight(true);
}

void loop()
{
  update_power_display();
  unPhone::rgb(0, 0, 1); // flash the internal LED blue
  delay(100);
  unPhone::rgb(0, 0, 0);
  delay(100);
}

void update_power_display()
{
  // Read voltage and current from INA219.
  float shuntvoltage = ina219.getShuntVoltage_mV();
  float busvoltage = ina219.getBusVoltage_V();
  float current_mA = ina219.getCurrent_mA();

  // Compute load voltage, power, and milliamp-hours.
  float loadvoltage = busvoltage + (shuntvoltage / 1000);
  float power_mW = loadvoltage * current_mA;

  //   // Mode 0, display volts and amps.
  unPhone::tftp->fillRect(1, 60, 318, 72, HX8357_BLACK);
  //unPhone::tftp->setCursor(80, 60);
  //unPhone::tftp->print("V:");
  //unPhone::tftp->print(loadvoltage);
  unPhone::tftp->setCursor(50, 104);
  unPhone::tftp->print(current_mA);
  unPhone::tftp->print(" mA");
}

void printSIValue(float value, char *units, int precision, int maxWidth)
{
  // Print a value in SI units with the units left justified and value right justified.
  // Will switch to milli prefix if value is below 1.

  // Add milli prefix if low value.
  if (fabs(value) < 1.0)
  {
    unPhone::tftp->print('m');
    maxWidth -= 1;
    value *= 1000.0;
    precision = max(0, precision - 3);
  }

  // Print units.
  unPhone::tftp->print(units);
  maxWidth -= strlen(units);

  // Leave room for negative sign if value is negative.
  if (value < 0.0)
  {
    maxWidth -= 1;
  }

  // Find how many digits are in value.
  int digits = ceil(log10(fabs(value)));
  if (fabs(value) < 1.0)
  {
    digits = 1; // Leave room for 0 when value is below 0.
  }

  // Handle if not enough width to display value, just print dashes.
  if (digits > maxWidth)
  {
    // Fill width with dashes (and add extra dash for negative values);
    for (int i = 0; i < maxWidth; ++i)
    {
      unPhone::tftp->print('-');
    }
    if (value < 0.0)
    {
      unPhone::tftp->print('-');
    }
    return;
  }

  // Compute actual precision for printed value based on space left after
  // printing digits and decimal point.  Clamp within 0 to desired precision.
  int actualPrecision = constrain(maxWidth - digits - 1, 0, precision);

  // Compute how much padding to add to right justify.
  int padding = maxWidth - digits - 1 - actualPrecision;
  for (int i = 0; i < padding; ++i)
  {
    unPhone::tftp->print(' ');
  }

  // Finally, print the value!
  unPhone::tftp->print(value, actualPrecision);
}