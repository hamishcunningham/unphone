#!/bin/bash
# inject-ioexp.sh

# standard locals
P="$0"
BASEDIR="$(cd `dirname ${P}`/..)"

USAGE="`basename ${P}` [-h(elp)] [-d(ebug)]
patch ./lib/*.c etc for unPhone IOExpander"
DBG=:
OPTIONSTRING=hd
R='\033[0;31m' # red (use with echo -e)
G='\033[0;32m' # green
B='\033[1;34m' # blue
N='\033[0m'    # no color

# specific locals
LIBS_TREE=${BASEDIR}/lib
INJECT_CODE="${BASEDIR}/../../bin/lib-injector.cpp"
LIBS_TO_PATCH=

# message & exit if exit num present
e() { (
  C=""; case $1 in R|G|B|N) C=${!1}; shift; ;; esac; echo -e "${C}$*${N}";
) }
usage() { e G "Usage: $USAGE"; [ ! -z "$1" ] && exit $1; }

# process options
while getopts $OPTIONSTRING OPTION
do
  case $OPTION in
    h)	usage 0 ;;
    d)	DBG=echo ;;
    *)	usage 1 ;;
  esac
done 
shift `expr $OPTIND - 1`

# patch the libraries
cd $LIBS_TREE
e G "patching in `pwd`..."

# TODO find . -name '*.cpp' -type f -exec egrep -ls 'digitalRead|digitalWrite|pinMode' '{}' \;
OIFS="$IFS"
IFS='
'
for f in `find . -type f |grep -v '\/examples\/' |egrep '\.cpp'`
do
  IFS="$OIFS"
  echo "$f"
  if egrep -q 'digitalWrite|digitalRead|pinMode' "$f"
  then
    # this library file twiddles pins so we need to inject IOExpander stuff
    e G injecting unPhone library into $B "$f"
    if grep -q IOExpander "$f"
    then
      # the expander's already injected, don't do owt
      e B "$f" $G already contains IOExpander
    else
      # prepend the IOExpander stuff
      ( echo "// `basename ${f}`"; cat ${INJECT_CODE}; cat "${f}"; ) >$$.tmp
      mv $$.tmp "$f"
      e G "done $B $f"
    fi
  fi
done

# HX8357 hack
cd "$BASEDIR"
e B munging the LCD library...
LCD_LIB="`find . -name Adafruit_HX8357.cpp`"
e G from "$LCD_LIB"...
sleep 2

#
# the LCD is reversed by default; this hack fixes:
CHANGES=0
for p in `egrep -n '^[ ]*0xC0,$' "${LCD_LIB}" |sed 's,:.*$,,g'`
do
  # change the library file; old line numbers were: 149|204, 226|171
  e G MADCTL $p
  case $p in
    341|225) CHANGES=$((CHANGES + 1)); sed -i \
      "${p}s/0xC0,/0x88, \/\/ modified (from 0xC0) for unPhone spin 4+/" \
      "${LCD_LIB}" ;;
  esac
done
if [ $CHANGES == 2 ]
then
  e B 'rewrote two MADCTL 0xC0 lines'
else
  e B "found only $CHANGES 0xC0 lines... injected already, or lib changed...?"
fi
