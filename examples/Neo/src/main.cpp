// main.cpp

#include <Arduino.h>
#include <esp_log.h>
#include "sdkconfig.h"
#include "unphone.h"

void setup() {
  UNPHONE_DBG = true;
  unPhone::begin();

  // power management
  unPhone::printWakeupReason(); // what woke us up?
  unPhone::checkPowerSwitch();  // if power switch is off, shutdown

  Serial.printf("hello\n");
  ESP_LOGW("setup", "hello again");
}

void loop() {
  Serial.printf("hello again again\n");
  delay(100000);
} 
