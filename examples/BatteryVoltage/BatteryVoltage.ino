#include <unphone.h>

float VT,rawVT;
void setup() {
  unPhone::begin();
  Serial.printf("Hello, voltage test\n");
  unPhone::printWakeupReason(); // what woke us up?
  unPhone::tftp->fillScreen(HX8357_BLACK);
  unPhone::tftp->setTextSize(2);
  unPhone::tftp->setTextColor(HX8357_WHITE);
  unPhone::backlight(true);
  digitalWrite(unPhone::IR_LEDS,HIGH);
}

void loop() {
  unPhone::checkPowerSwitch();  // if power switch is off, shutdown
  VT,rawVT =0;
  for (int i=0;i<10;i++){
    VT=VT+unPhone::batteryVoltage();
    rawVT=rawVT+unPhone::tsp->updateADC(ADC_VBAT);
  }
  Serial.printf("Battery voltage = %.3f\n", VT/10);
  Serial.printf("Raw reading = %.1f\n", rawVT/10);
  unPhone::tftp->fillRect(0, 300, 290, 32 , HX8357_BLACK);
  unPhone::tftp->setCursor(0,300);
  unPhone::tftp->printf("Battery voltage = %.3f\n", VT/10);
  unPhone::tftp->printf("Raw reading = %.1f\n", rawVT/10);
  delay(500);
}
