/***************************************************
 Hacked so comprehensively no point/need attributing it to any one thing anymore...
 Mostly rewritten and bugs created by Gareth, original cleverness from Hamish, Gee, adafruit, other library authors, the internet etc
 ****************************************************/
#define UNPHONE_SPIN 8
#include <unphone.h>              // unphone specifics
#include <lmic.h>                 // IBM LMIC (LoraMAC-in-C) library
#include <hal/hal.h>              // hardware abstraction for LMIC on Arduino
//#include <dummy.h>                // fool the build into including all of ...
//#include <ideetron/dummy2.h>      // ... lib/arduino-lmic/aes

XPT2046_Touchscreen ts(IOExpander::TOUCH_CS);  // no IRQ
Adafruit_LSM9DS1 accel = Adafruit_LSM9DS1();   // i2c
sensors_event_t a, m, g, temp;
Adafruit_HX8357 tft = Adafruit_HX8357(IOExpander::LCD_CS, unPhone::LCD_DC, IOExpander::LCD_RESET);

#include <SdFat.h>                // SD card & FAT filesystem library
SdFat SD;

byte read8(byte address, byte reg);
byte getRegister(byte address, byte reg);

bool slideState;
volatile int loopCounter = 0;
bool doFlash = true;

byte BM_I2Cadd = 0x6b;
byte BM_Status   = 0x08; // System Status Register 
byte BM_Version  = 0x0a; // Vender / Part / Revision Status Register 

const lmic_pinmap lmic_pins = {
     .nss = IOExpander::LORA_CS,
     .rxtx = LMIC_UNUSED_PIN,
     .rst = IOExpander::LORA_RESET,
     .dio = { 5, 18, LMIC_UNUSED_PIN },
     .rxtx_rx_active = 0,
     .rssi_cal = 0,
     .spi_freq = 8000000,
};

byte getRegister(byte address, byte reg) {
  byte result;
  result=read8(address,reg);
  return result;
}

byte read8(byte address, byte reg) {
  byte value;
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.endTransmission();
  Wire.requestFrom(address, (byte)1);
  value = Wire.read();
  Wire.endTransmission();
  return value;
}

void screenDraw(void) {
  tft.fillScreen(HX8357_BLACK);
  tft.drawRect(0, 0, 320, 480, HX8357_WHITE);
  tft.setTextSize(3);
  tft.setCursor(0,0);
  tft.setTextColor(HX8357_RED);
  tft.print("Red");
  tft.setCursor(230,0);
  tft.setTextColor(HX8357_GREEN);
  tft.print("Green");
  tft.setCursor(0,460);
  tft.setTextColor(HX8357_BLUE);
  tft.print("Blue");
  tft.setCursor(212,460);
  tft.setTextColor(HX8357_YELLOW);
  tft.print("Yellow");
  IOExpander::digitalWrite(IOExpander::LED_GREEN, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, LOW);
  IOExpander::digitalWrite(IOExpander::LED_RED, LOW);

  delay(1000);

  tft.fillScreen(HX8357_BLACK);
  tft.setTextColor(HX8357_RED);
  tft.setCursor(30,50);
  tft.print("But3");

  tft.setCursor(135,50);
  tft.print("But2");

  tft.setCursor(240,50);
  tft.print("But1");

  tft.setCursor(100,100);  
  tft.print("Slide ");
  if (slideState) tft.print("<-"); else tft.print("->");
  tft.drawRect(40, 200, 250, 70, HX8357_MAGENTA);
  tft.setTextSize(4);
  tft.setCursor(70,220);
  tft.setTextColor(HX8357_CYAN);
  tft.print("Touch me");
}

void screenTouched(void) {
  tft.fillRect(40, 200, 250, 70, HX8357_BLACK);
  tft.drawRect(15, 200, 290, 70, HX8357_CYAN);
  tft.setTextSize(4);
  tft.setCursor(25,220);
  tft.setTextColor(HX8357_GREEN);
  tft.print("I'm touched");
  doFlash=false;
  IOExpander::digitalWrite(IOExpander::VIBE,LOW);
  digitalWrite(unPhone::IR_LEDS,LOW);
  IOExpander::digitalWrite(IOExpander::LED_GREEN, LOW);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_RED, HIGH);
}

void screenError(const char* message) {
  IOExpander::digitalWrite(IOExpander::LED_GREEN, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_RED, LOW);
  tft.fillScreen(HX8357_BLACK);
  tft.setCursor(0,10);
  tft.setTextSize(2);
  tft.setTextColor(HX8357_WHITE);
  tft.print(message);
  delay(5000);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT,LOW);
  while(true);
}

void setup() {
  Wire.begin();
  IOExpander::begin();
  pinMode(unPhone::BUTTON1,INPUT_PULLUP);
  pinMode(unPhone::BUTTON2,INPUT_PULLUP);
  pinMode(unPhone::BUTTON3,INPUT_PULLUP);
  slideState = IOExpander::digitalRead(IOExpander::POWER_SWITCH);
  Serial.begin(115200);
  Serial.println("Spin 8 test rig");
  pinMode(unPhone::EXPANDER_POWER,OUTPUT);
  
  Serial.printf("setting IR_LED at %d to OUTPUT\n", unPhone::IR_LEDS);
  pinMode(unPhone::IR_LEDS,OUTPUT);
  
  Serial.print("getVersionNumber() says spin ");
  Serial.println(IOExpander::getVersionNumber());
  
  tft.begin(HX8357D);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT,HIGH);
  screenDraw();
  Serial.println("screen displayed");
  tft.setTextColor(HX8357_GREEN);
  IOExpander::digitalWrite(IOExpander::LED_GREEN, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_RED, HIGH);
  if(! ts.begin()) {
    Serial.println("failed to start touchscreen controller");
    screenError("failed to start touchscreen controller");
  } else {
    ts.setRotation(1); // should actually be 3 I think
    Serial.println("Touchscreen started OK");
    tft.setCursor(30,380);
    tft.setTextSize(2);
    tft.print("Touchscreen started");
  }

  if(!accel.begin()) {
    Serial.println("failed to start accelerometer");
    screenError("failed to start accelerometer");
  } else {
    Serial.println("Accelerometer started OK");
    tft.setCursor(30,350);
    tft.setTextSize(2);
    tft.print("Accelerometer started");
  }

  // init the SD card
  // see Adafruit_ImageReader/examples/FeatherWingHX8357/FeatherWingHX8357.ino
  IOExpander::digitalWrite(IOExpander::SD_CS, LOW);
  tft.setCursor(30,410);
  tft.setTextSize(2);
  if(!SD.begin(IOExpander::SD_CS, SD_SCK_MHZ(25))) { // ESP32 25 MHz limit
    Serial.println("failed to start SD card");
    tft.setTextColor(HX8357_RED);
    tft.print("SD Card missing/failed");
    tft.setTextColor(HX8357_GREEN);
  } else {
    Serial.println("SD Card started OK");
    tft.setTextColor(HX8357_GREEN);
    tft.print("SD Card started");
  }
  IOExpander::digitalWrite(IOExpander::SD_CS, HIGH);

  if (getRegister(BM_I2Cadd,BM_Version)!=192){
    Serial.println("failed to start Battery management");
    screenError("failed to start Battery management");
  } else {
    Serial.println("Battery management started OK");
    tft.setCursor(30,440);
    tft.setTextSize(2);
    tft.print("Batt management started");
  }
    tft.setCursor(30,320);
    tft.setTextSize(2);
    tft.println("LORA failed");
    os_init(); // if lora fails then will stop at this init, allowing fail to be seen
    tft.fillRect(30, 320, 250, 32, HX8357_BLACK);
    tft.setCursor(30,320);
    tft.println("LoRa started");
    LMIC_reset();

  tft.setTextSize(2);
  tft.setCursor(120,150);
  tft.print("spin: ");
  tft.println(UNPHONE_SPIN);

  IOExpander::digitalWrite(IOExpander::LED_GREEN, LOW);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, LOW);
  IOExpander::digitalWrite(IOExpander::LED_RED, HIGH);
  delay(300);
  IOExpander::digitalWrite(IOExpander::LED_GREEN, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, LOW);
  IOExpander::digitalWrite(IOExpander::LED_RED, LOW);
  delay(300);
  IOExpander::digitalWrite(IOExpander::LED_GREEN, LOW);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_RED, LOW);
  delay(300);
 }

void loop(void) {
  if (doFlash){
    loopCounter++;
    if (loopCounter<50) {
      IOExpander::digitalWrite(IOExpander::VIBE,HIGH);
      digitalWrite(unPhone::EXPANDER_POWER,HIGH); // enable expander power supply
      digitalWrite(unPhone::IR_LEDS,HIGH);
    } else if (loopCounter>=50) {
       IOExpander::digitalWrite(IOExpander::VIBE,LOW);
      digitalWrite(unPhone::EXPANDER_POWER,HIGH); // disable expander power supply
      digitalWrite(unPhone::IR_LEDS,LOW);
    }
    if (loopCounter>=99) loopCounter=0;
  }
  
  if (ts.touched()) {
    TS_Point p = ts.getPoint();
    if (p.z>40 && p.x>1000 && p.x<3500 && p.y>1300 && p.y<3900){
      screenTouched();
    }
  }

  if (digitalRead(unPhone::BUTTON1)==LOW) {
    tft.setTextSize(3);
    tft.setCursor(240,50);
    tft.print("But1");    
  }

  if (digitalRead(unPhone::BUTTON2)==LOW) {
    tft.setTextSize(3);
    tft.setCursor(135,50);
    tft.print("But2");    
  }

  if (digitalRead(unPhone::BUTTON3)==LOW) {
    tft.setTextSize(3);
    tft.setCursor(30,50);
    tft.print("But3");    
  }

  if (IOExpander::digitalRead(IOExpander::POWER_SWITCH)!=slideState) {
    slideState=!slideState;
    tft.fillRect(100, 100, 150, 32, HX8357_BLACK);
    tft.setTextSize(3);
    tft.setCursor(130,100);  
    tft.print("Slid");
  }

  tft.fillRect(50, 280, 250, 32, HX8357_BLACK);
  tft.setTextSize(2);
  tft.setCursor(50,280);
  accel.getEvent(&a, &m, &g, &temp);
  tft.print("X: "); tft.println(a.acceleration.x);
  tft.setCursor(150,280);
  tft.print("Y: "); tft.println(a.acceleration.y);
}
