// Elfin main.cpp

#include "waterelf.h"
#include "private.h" // stuff not for checking in

// OTA support //////////////////////////////////////////////////////////////
const char *myName = "Elfin";
const int firmwareVersion = 1; // keep up-to-date! (used to check for updates)

// LEDs
uint8_t ledPins[] = {
  GPIO_NUM_26,
  GPIO_NUM_25,
  GPIO_NUM_21,
  GPIO_NUM_4,
  GPIO_NUM_27,
  GPIO_NUM_33,
  GPIO_NUM_15,
  GPIO_NUM_32,
  GPIO_NUM_14,
};
uint8_t numLedPins = sizeof(ledPins) / sizeof(uint8_t);

WaterElf elf = WaterElf();

// setup ////////////////////////////////////////////////////////////////////
void setup() {
  Serial.begin(UNPHELF_SERIAL_SPEED);
  Serial.printf("Serial initialised at %d\n", UNPHELF_SERIAL_SPEED);
  esp_log_level_set("*", ESP_LOG_ERROR);

  pinMode(BUILTIN_LED, OUTPUT); // turn built-in LED on
  Serial.printf("Elfin: firmware is at version %d\n", firmwareVersion);
  elf.start();

  // do wifi provisionning, start the web server
/*
  Serial.printf("doing wifi manager\n");

  String apSSIDStr = myName + String("-") + String(getMAC(MAC_ADDRESS));
  const char* apSSID = apSSIDStr.c_str();
  joinmeManageWiFi(apSSID, _ELF_AP_KEY);
  Serial.printf("wifi manager done\n\n");

  // check for and perform firmware updates as needed
  vTaskDelay(2000 / portTICK_PERIOD_MS);  // let wifi settle
  joinmeOTAUpdate( // download via raw url; for private repo use _GITLAB_TOKEN
    firmwareVersion, _GITLAB_PROJ_ID, "", "examples%2FElfin%2Ffirmware%2F"
  );
*/

  delay(300); elf.blinkFeatherLED(5);       // signal we've finished config
  printf("\n"); delay(500); printf("\n");
}

/////////////////////////////////////////////////////////////////////////////
// looooooooooooooooooooop //////////////////////////////////////////////////
void loop() {
  // elf.blinkTest();
  // digitalWrite(elf.pumpPins[0], HIGH); // v1, pump
  // digitalWrite(elf.solPins[0], HIGH);  // v1, sol
  /*
  for(int i = 0; i < elf.NUM_GROW_BEDS; i++) {
    elf.valves[i].close();
    delay(2000);
    elf.valves[i].open();
    delay(2000);
  }
  */
  elf.step();
}
