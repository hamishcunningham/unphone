// unphelf.h
// definitions shared by both unPhone and WaterElf

#ifndef UNPHELF_H
#define UNPHELF_H

#include "joinme.h"             // OTA etc.

// constants
#define UNPHELF_SERIAL_SPEED 115200

// MAC address //////////////////////////////////////////////////////////////
char *getMAC(char *buf); // get the MAC address
extern char MAC_ADDRESS[13]; // MACs are 12 chars, plus the NULL terminator
String ip2str(IPAddress address); // visualise an IP addr

#endif
