import time
import board
import busio
import digitalio
from community_tca9555 import TCA9555 # downloaded from https://github.com/lesamouraipourpre/Community_CircuitPython_TCA9555

expander = TCA9555(board.I2C(),0x26)  # Use address of 0x26
expander.configuration_ports = 0x4000 # port_1_pin_6 input, rest outputs

redLED = digitalio.DigitalInOut(board.D13)
redLED.direction = digitalio.Direction.OUTPUT

while True:
    redLED.value = False # Red LED has inverted logic, False = on
    time.sleep(0.5)

    redLED.value = True
    expander.output_port_1_pin_1 = False  # Green LED has inverted logic, False = on
    time.sleep(0.5)

    expander.output_port_1_pin_1 = True
    expander.output_port_1_pin_5 = False  # Blue LED has inverted logic, False = on
    time.sleep(0.5)

    expander.output_port_1_pin_5 = True
    time.sleep(0.5)
