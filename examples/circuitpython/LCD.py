import time
import board
import busio
import digitalio
from community_tca9555 import TCA9555 # downloaded from https://github.com/lesamouraipourpre/Community_CircuitPython_TCA9555
import terminalio
import displayio
from adafruit_display_text import label
from adafruit_hx8357 import HX8357

power_switch = digitalio.DigitalInOut(board.D18)
power_switch.direction = digitalio.Direction.INPUT

expander = TCA9555(board.I2C(),0x26)  # Use address of 0x26
expander.configuration_ports = 0x4000 # port_1_pin_6 input, rest outputs
expander.output_port_0_pin_2 = True  # Backlight on

displayio.release_displays()

spi = board.SPI()
tft_cs = board.D48
tft_dc = board.D47
tft_rst = board.D46

display_bus = displayio.FourWire(spi, command=tft_dc, chip_select=tft_cs, reset=tft_rst)

display = HX8357(display_bus, width=320, height=480)

# Make the display context
splash = displayio.Group()
display.show(splash)

color_bitmap = displayio.Bitmap(320, 480, 1)
color_palette = displayio.Palette(1)
color_palette[0] = 0x00FF00  # Bright Green

bg_sprite = displayio.TileGrid(color_bitmap, pixel_shader=color_palette, x=0, y=0)
splash.append(bg_sprite)

# Draw a smaller inner rectangle
inner_bitmap = displayio.Bitmap(280, 440, 1)
inner_palette = displayio.Palette(1)
inner_palette[0] = 0xAA0088  # Purple
inner_sprite = displayio.TileGrid(inner_bitmap, pixel_shader=inner_palette, x=20, y=20)
splash.append(inner_sprite)

# Draw a label
text_group = displayio.Group(scale=3, x=37, y=160)
text = "Hello World!!"
text_area = label.Label(terminalio.FONT, text=text, color=0xFFFF00)
text_group.append(text_area)  # Subgroup for text scaling
splash.append(text_group)

while True:
    expander.output_port_0_pin_2 = power_switch.value
    pass
