/***************************************************
 Hacked so comprehensively no point/need attributing it to any one thing anymore...
 Mostly rewritten and bugs created by Gareth, original cleverness from Hamish, Gee, adafruit, other library authors, the internet etc
 ****************************************************/
#define UNPHONE_SPIN 8
#include <unphone.h>              // unphone specifics
#include <lmic.h>                 // IBM LMIC (LoraMAC-in-C) library
#include <hal/hal.h>              // hardware abstraction for LMIC on Arduino

bool slideState;
volatile int loopCounter = 0;
bool doFlash = true;
sensors_event_t a, m, g, temp;

const lmic_pinmap lmic_pins = {
  .nss = IOExpander::LORA_CS,
  .rxtx = LMIC_UNUSED_PIN,
  .rst = IOExpander::LORA_RESET,
  #if UNPHONE_SPIN == 7
    .dio = { 39, 26, LMIC_UNUSED_PIN },
  #elif UNPHONE_SPIN == 8
   .dio = { 5, 18, LMIC_UNUSED_PIN },
  #endif
};

void screenDraw(void) {
  unPhone::tftp->fillScreen(HX8357_BLACK);
  unPhone::tftp->drawRect(0, 0, 320, 480, HX8357_WHITE);
  unPhone::tftp->setTextSize(3);
  unPhone::tftp->setCursor(0,0);
  unPhone::tftp->setTextColor(HX8357_RED);
  unPhone::tftp->print("Red");
  unPhone::tftp->setCursor(230,0);
  unPhone::tftp->setTextColor(HX8357_GREEN);
  unPhone::tftp->print("Green");
  unPhone::tftp->setCursor(0,460);
  unPhone::tftp->setTextColor(HX8357_BLUE);
  unPhone::tftp->print("Blue");
  unPhone::tftp->setCursor(212,460);
  unPhone::tftp->setTextColor(HX8357_YELLOW);
  unPhone::tftp->print("Yellow");
  unPhone::rgb(1,0,1);
  delay(1000);

  unPhone::tftp->fillScreen(HX8357_BLACK);
  unPhone::tftp->setTextColor(HX8357_RED);
  unPhone::tftp->setCursor(30,50);
  unPhone::tftp->print("But3");

  unPhone::tftp->setCursor(135,50);
  unPhone::tftp->print("But2");

  unPhone::tftp->setCursor(240,50);
  unPhone::tftp->print("But1");

  unPhone::tftp->setCursor(100,100);  
  unPhone::tftp->print("Slide ");
  if (slideState) unPhone::tftp->print("<-"); else unPhone::tftp->print("->");
  unPhone::tftp->drawRect(40, 200, 250, 70, HX8357_MAGENTA);
  unPhone::tftp->setTextSize(4);
  unPhone::tftp->setCursor(70,220);
  unPhone::tftp->setTextColor(HX8357_CYAN);
  unPhone::tftp->print("Touch me");
}

void screenTouched(void) {
  unPhone::tftp->fillRect(40, 200, 250, 70, HX8357_BLACK);
  unPhone::tftp->drawRect(15, 200, 290, 70, HX8357_CYAN);
  unPhone::tftp->setTextSize(4);
  unPhone::tftp->setCursor(25,220);
  unPhone::tftp->setTextColor(HX8357_GREEN);
  unPhone::tftp->print("I'm touched");
  doFlash=false;
  unPhone::vibe(false);
  digitalWrite(unPhone::IR_LEDS,LOW);
  unPhone::rgb(0,1,0);
}

void screenError(const char* message) {
  unPhone::rgb(1,0,0);
  unPhone::tftp->fillScreen(HX8357_BLACK);
  unPhone::tftp->setCursor(0,10);
  unPhone::tftp->setTextSize(2);
  unPhone::tftp->setTextColor(HX8357_WHITE);
  unPhone::tftp->print(message);
  delay(5000);
  unPhone::backlight(false);
  unPhone::rgb(0,0,0);
  while(true);
}

void setup() {
  unPhone::begin();
  unPhone::checkPowerSwitch();
  slideState = IOExpander::digitalRead(IOExpander::POWER_SWITCH);
  Serial.print("Spin 8 test rig, spin ");
  Serial.println(String(UNPHONE_SPIN));
  Serial.print("getVersionNumber() says spin ");
  Serial.println(IOExpander::getVersionNumber());
  unPhone::backlight(true);
  screenDraw();
  Serial.println("screen displayed");
  unPhone::tftp->setTextColor(HX8357_GREEN);
  unPhone::rgb(1,1,1);
  if(!unPhone::tsp->begin()) {
    Serial.println("failed to start touchscreen controller");
    screenError("failed to start touchscreen controller");
  } else {
    unPhone::tsp->setRotation(1); // should actually be 3 I think
    Serial.println("Touchscreen started OK");
    unPhone::tftp->setCursor(30,380);
    unPhone::tftp->setTextSize(2);
    unPhone::tftp->print("Touchscreen started");
  }

  if(!unPhone::accelp->begin()) {
    Serial.println("failed to start accelerometer");
    screenError("failed to start accelerometer");
  } else {
    Serial.println("Accelerometer started OK");
    unPhone::tftp->setCursor(30,350);
    unPhone::tftp->setTextSize(2);
    unPhone::tftp->print("Accelerometer started");
  }

  // init the SD card
  // see Adafruit_ImageReader/examples/FeatherWingHX8357/FeatherWingHX8357.ino
  IOExpander::digitalWrite(IOExpander::SD_CS, LOW);
  if(!unPhone::sdp->begin(IOExpander::SD_CS, SD_SCK_MHZ(25))) { // ESP32 25 MHz limit
    Serial.println("failed to start SD card");
    screenError("failed to start SD card");
  } else {
    Serial.println("SD Card started OK");
    unPhone::tftp->setCursor(30,410);
    unPhone::tftp->setTextSize(2);
    unPhone::tftp->setTextColor(HX8357_GREEN);
    unPhone::tftp->print("SD Card started");
  }
  IOExpander::digitalWrite(IOExpander::SD_CS, HIGH);

  if (unPhone::getRegister(unPhone::BM_I2Cadd,unPhone::BM_Version)!=192){
    Serial.println("failed to start Battery management");
    screenError("failed to start Battery management");
  } else {
    Serial.println("Battery management started OK");
    unPhone::tftp->setCursor(30,440);
    unPhone::tftp->setTextSize(2);
    unPhone::tftp->print("Batt management started");
  }
  unPhone::tftp->setCursor(30,320);
  unPhone::tftp->setTextSize(2);
  unPhone::tftp->println("LoRa failed");
  os_init(); // if lora fails then will stop at this init, allowing fail to be seen
  unPhone::tftp->fillRect(30, 320, 250, 32, HX8357_BLACK);
  unPhone::tftp->setCursor(30,320);
  unPhone::tftp->println("LoRa started");
  LMIC_reset();

  unPhone::tftp->setTextSize(2);
  unPhone::tftp->setCursor(120,150);
  unPhone::tftp->print("spin: ");
  unPhone::tftp->println(UNPHONE_SPIN);

  unPhone::rgb(255,0,0);
  delay(300);
  unPhone::rgb(0,255,0);
  delay(300);
  unPhone::rgb(0,0,255);
  delay(300);
 }

void loop(void) {
  if (doFlash){
    loopCounter++;
    if (loopCounter<50) {
      unPhone::vibe(true);
      unPhone::expanderPower(true);            // enable expander power supply
      digitalWrite(unPhone::IR_LEDS,HIGH);
    } else if (loopCounter>=50) {
      unPhone::vibe(false);
      unPhone::expanderPower(false);           // disable expander power supply
      digitalWrite(unPhone::IR_LEDS,LOW);
    }
    if (loopCounter>=99) loopCounter=0;
  }
  
  if (unPhone::tsp->touched()) {
    TS_Point p = unPhone::tsp->getPoint();
    if (p.z>40 && p.x>1000 && p.x<3500 && p.y>1300 && p.y<3900){
      screenTouched();
    }
  }

  if (digitalRead(unPhone::BUTTON1)==LOW) {
    unPhone::tftp->setTextSize(3);
    unPhone::tftp->setCursor(240,50);
    unPhone::tftp->print("But1");    
  }

  if (digitalRead(unPhone::BUTTON2)==LOW) {
    unPhone::tftp->setTextSize(3);
    unPhone::tftp->setCursor(135,50);
    unPhone::tftp->print("But2");    
  }

  if (digitalRead(unPhone::BUTTON3)==LOW) {
    unPhone::tftp->setTextSize(3);
    unPhone::tftp->setCursor(30,50);
    unPhone::tftp->print("But3");    
  }

  if (IOExpander::digitalRead(IOExpander::POWER_SWITCH)!=slideState) {
    slideState=!slideState;
    unPhone::tftp->fillRect(100, 100, 150, 32, HX8357_BLACK);
    unPhone::tftp->setTextSize(3);
    unPhone::tftp->setCursor(130,100);  
    unPhone::tftp->print("Slid");
  }

  unPhone::tftp->fillRect(50, 280, 250, 32, HX8357_BLACK);
  unPhone::tftp->setTextSize(2);
  unPhone::tftp->setCursor(50,280);
  unPhone::accelp->getEvent(&a, &m, &g, &temp);
  unPhone::tftp->print("X: "); unPhone::tftp->println(a.acceleration.x);
  unPhone::tftp->setCursor(150,280);
  unPhone::tftp->print("Y: "); unPhone::tftp->println(a.acceleration.y);

  unPhone::checkPowerSwitch();
}
