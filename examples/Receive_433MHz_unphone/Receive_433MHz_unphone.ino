/*
  Simple example for receiving
  adapted by gareth coleman 2022
  
  https://github.com/sui77/rc-switch/
*/
#define UNPHONE_SPIN 7
#include <RCSwitch.h>
#include <unphone.h>

RCSwitch myRadio = RCSwitch();

void setup() {
  unPhone::begin();
  digitalWrite(IOExpander::BACKLIGHT,LOW);
  digitalWrite(unPhone::EXPANDER_POWER,LOW); // disable expander power supply
  unPhone::printWakeupReason(); // what woke us up?
  unPhone::checkPowerSwitch();  // if power switch is off, shutdown
  digitalWrite(unPhone::EXPANDER_POWER,HIGH); // enable expander power supply
  myRadio.enableReceive(digitalPinToInterrupt(15));  // Receiver on pin #15
  Serial.println("Started 433MHz reciever");
  unPhone::tftp->fillScreen(HX8357_BLACK);
  unPhone::tftp->setTextColor(HX8357_WHITE);
  unPhone::tftp->setTextSize(2);
  unPhone::tftp->setCursor(0,0);
  unPhone::tftp->print("Started 433MHz reciever");
  digitalWrite(IOExpander::BACKLIGHT,HIGH);

  // buzz a bit
  for(int i = 0; i < 3; i++) {
    unPhone::vibe(true);  delay(150);
    unPhone::vibe(false); delay(150);
  }
}

void loop() {
  if (myRadio.available()) {
    int receivedData = myRadio.getReceivedValue();
    Serial.print("Received ");
    Serial.println(receivedData);
    unPhone::tftp->print(receivedData);
    myRadio.resetAvailable();
  }
}
