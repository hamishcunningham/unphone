// MIE, derived from Blink Example

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_task_wdt.h"
#include "../feather_esp32/pins_arduino.h"
#include "Arduino.h"
#include "sdkconfig.h"

void MIESetup(); // C++...
void MIELoop();  // ...land

TaskHandle_t loopTaskHandle = NULL;
bool loopTaskWDTEnabled;

void loopTask(void *pvParameters) {
  MIESetup();
  for(;;) {
    if(loopTaskWDTEnabled) {
      esp_task_wdt_reset();
    }
    MIELoop();
    vTaskDelay(10000 / portTICK_PERIOD_MS);

    // do we need to support serial events? include HardwareSerial.h?
    // if(serialEventRun) serialEventRun();
  }
}

void app_main() {
  loopTaskWDTEnabled = false;
  initArduino();
  xTaskCreateUniversal(
    loopTask, "loopTask", 8192, NULL, 1, &loopTaskHandle, CONFIG_ARDUINO_RUNNING_CORE
  );
}
