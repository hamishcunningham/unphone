// AppEntry.cpp

// we need the board definition first of all
#include "../feather_esp32/pins_arduino.h"

#include <Arduino.h>
#include <Wire.h>
#include <esp_log.h>
#include "sdkconfig.h"
#include "unPhone.h"

extern "C" void MIESetup();
extern "C" void MIELoop();

void MIESetup() {
  Serial.begin(115200);  // init the serial line
  Serial.printf("hello from MIE\n");
  Serial.printf("SDA=%u, SCL=%u (unPhone should be 23, 22)\n", SDA, SCL);

  Wire.begin(SDA, SCL); // without the board def we need: Wire.begin(23, 22);
  Wire.setClock(100000); // rates > 100k used to trigger an IOExpander bug...?
  IOExpander::begin();

  Serial.println("inputPwrSw:");
  for(uint8_t i = 0; i < 16; i++) {
    uint8_t inputPwrSw = IOExpander::digitalRead(i | 0x40);
    Serial.printf(" %u ", inputPwrSw);
    delay(50);
  }
  Serial.println("");
  uint8_t inputPwrSw = IOExpander::digitalRead(IOExpander::POWER_SWITCH | 0x40);
  Serial.printf("power switch is %s\n", inputPwrSw ? "on" : "off");

  ESP_LOGW("setup", "hello again");
}

void MIELoop() {
  Serial.printf("running on spin %u\n", unPhone::spinNumber());

  // testing out I2C...
  byte error, address;
  int nDevices;

  ESP_LOGW("loop", "hello again again");
  Serial.println("Scanning...");

  nDevices = 0;
  for(address = 1; address < 127; address++ ) {
    // the i2c scanner uses the return value of
    // the Write.endTransmisstion to see if
    // a device did acknowledge to the address
    Wire.beginTransmission(address);
    error = Wire.endTransmission();

    if (error == 0) {
      Serial.print("I2C device found at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.print(address,HEX);
      Serial.println("  !");

      nDevices++;
    }
    else if (error==4) {
      Serial.print("Unknow error at address 0x");
      if (address<16)
        Serial.print("0");
      Serial.println(address,HEX);
    }
  }
  if (nDevices == 0)
    Serial.println("No I2C devices found\n");
  else
    Serial.println("done\n");

  delay(60000);           // wait 60 seconds for next scan
}

