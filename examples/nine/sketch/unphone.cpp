// unphone.cpp
// core library

#include "unphone.h"
#include <Preferences.h>

bool UNPHONE_DBG = true; // debug switch

// the LCD and touch screen //////////////////////////////////////////////////
Adafruit_HX8357 *unPhone::tftp;
#if UNPHONE_SPIN == 6
Adafruit_STMPE610 *unPhone::tsp;
#elif UNPHONE_SPIN >= 7
XPT2046_Touchscreen *unPhone::tsp;
#endif
void unPhone::backlight(bool on) { // turn the backlight on or off
  if(on) digitalWrite(IOExpander::BACKLIGHT, HIGH);
  else   digitalWrite(IOExpander::BACKLIGHT, LOW);
}
void unPhone::expanderPower(bool on) { // expander board power on or off
#if UNPHONE_SPIN == 6
  return;
#elif UNPHONE_SPIN >= 7
  if(on) digitalWrite(EXPANDER_POWER, HIGH);
  else   digitalWrite(EXPANDER_POWER, LOW);
#endif
}

// SD card filesystem
SdFat *unPhone::sdp;

// the accelerometer //////////////////////////////////////////////////////
#if UNPHONE_SPIN == 6
Adafruit_LSM303_Accel_Unified *unPhone::accelp;
#elif UNPHONE_SPIN == 7 || UNPHONE_SPIN == 8
Adafruit_LSM9DS1 *unPhone::accelp;
#elif UNPHONE_SPIN >= 9
Adafruit_LSM6DS3TRC *unPhone::accelp;
#endif

// task handles
void powerSwitchTask(void *param) { // check power switch every 10th of a sec
  while(true) {
    unPhone::checkPowerSwitch(); delay(100);
  }
}

// initialise unPhone hardware
void unPhone::begin() {
  Serial.begin(115200); // init the serial line
  D("UNPHONE_SPIN: %d\n", UNPHONE_SPIN)
  beginStore(); // init small persistent store (does nothing if enabled false)

  // fire up I²C, and the unPhone's IOExpander library
  recoverI2C();
  Wire.begin();
  Wire.setClock(400000); // rates > 100k used to trigger an IOExpander bug...?
  IOExpander::begin();

  // start power switch checking
  checkPowerSwitch();
  xTaskCreate(powerSwitchTask, "power switch task", 4096, NULL, 1, NULL);

  // instantiate the display...
  tftp = new Adafruit_HX8357( // spin5 moves LCD_DC to 21
    IOExpander::LCD_CS, LCD_DC, IOExpander::LCD_RESET
  );
  IOExpander::digitalWrite(IOExpander::BACKLIGHT, LOW);
  tftp->begin(HX8357D);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT, HIGH);
  tftp->setTextWrap(false);
  // TODO clear screen to start with: tftp->fillScreen(HX8357_BLACK);

  // ...and the touch screen
#if UNPHONE_SPIN == 6
  tsp = new Adafruit_STMPE610(IOExpander::TOUCH_CS);
#elif UNPHONE_SPIN >= 7
  tsp = new XPT2046_Touchscreen(IOExpander::TOUCH_CS); // no IRQ
#endif
  bool status = tsp->begin();
  if(!status) {
    E("failed to start touchscreen controller\n")
  } else {
    D("worked\n")
  }

  // init the SD card
  // see Adafruit_ImageReader/examples/FeatherWingHX8357/FeatherWingHX8357.ino
  sdp = new SdFat();
  if(!sdp->begin(IOExpander::SD_CS, SD_SCK_MHZ(25))) { // ESP32 25 MHz limit
    E("sdp->begin failed\n")
  } else {
    D("sdp->begin OK\n")
  }

  // init touch screen GPIOs (used for vibe motor)
#if UNPHONE_SPIN == 6
  tsp->writeRegister8(0x17, 12); // use GPIO 2 & 3 (bcd)
  tsp->writeRegister8(0x13, 12); // all gpios as out, minimise power on unused
  tsp->writeRegister8(0x11, 8);  // set GPIO3 LOW to stop vibe
#elif UNPHONE_SPIN >= 7
  IOExpander::digitalWrite(IOExpander::VIBE, LOW);
#endif
  // initialise the buttons
#if UNPHONE_SPIN == 6
  Serial.printf("setting b2 %d to INPUT\n", BUTTON2);
  pinMode(BUTTON1, INPUT_PULLUP);
  pinMode(BUTTON2, INPUT);
  pinMode(BUTTON3, INPUT_PULLUP);
#elif UNPHONE_SPIN >= 7
  pinMode(BUTTON1, INPUT_PULLUP);
  pinMode(BUTTON2, INPUT_PULLUP);
  pinMode(BUTTON3, INPUT_PULLUP);
// #elif UNPHONE_SPIN == 8
//   D("setting b3 %d to INPUT\n", BUTTON3)
//   pinMode(BUTTON1, INPUT_PULLUP);
//   pinMode(BUTTON2, INPUT_PULLUP);
//   pinMode(BUTTON3, INPUT);
#endif
  D("pinmodes done\n")
  D("board spin = %u\n", IOExpander::getVersionNumber())
  D("PSRAM: size=%d, free=%d\n", ESP.getPsramSize(), ESP.getFreePsram())

  // expander power control, available from spin 7
#if UNPHONE_SPIN >= 7
  pinMode(EXPANDER_POWER, OUTPUT);
  // this will default LOW, i.e. off, but let's make it explicit anyhow
  expanderPower(false);
#endif

  // the accelerometer
#if UNPHONE_SPIN == 6
  accelp = new Adafruit_LSM303_Accel_Unified(54321); // with a "unique" ID
  if(!accelp->begin())                               // problem detecting the sensor?
    E("oops, no LSM303 detected ... check your wiring?!\n")
  else
    D("accelp->begin OK\n")
#elif UNPHONE_SPIN == 7 || UNPHONE_SPIN == 8
  accelp = new Adafruit_LSM9DS1(); // on i2c
  if (!accelp->begin()) // problem detecting the sensor?
    E("oops, no LSM9DS1 detected ... check your wiring?!\n")
  else
    D("accelp->begin OK\n")
#elif UNPHONE_SPIN >= 9
  accelp = new Adafruit_LSM6DS3TRC(); // on i2c
  if (!accelp->begin_I2C()) // problem detecting the sensor?
    E("oops, no LSM6DS3TRC detected ... check your wiring?!\n")
  else
    D("accelp->begin OK\n")
#endif

  // set up IR_LED pin and RGB/red
  pinMode(IR_LEDS, OUTPUT);
  pinMode(IOExpander::LED_RED, OUTPUT);
} // begin()

void unPhone::vibe(bool on) {
#if UNPHONE_SPIN == 6
  if(on)
    tsp->writeRegister8(0x10, 8); // set GPIO3 HIGH to start vibe
  else
    tsp->writeRegister8(0x11, 8); // set GPIO3 LOW to stop vibe
#elif UNPHONE_SPIN >= 7
  if (on)
    IOExpander::digitalWrite(IOExpander::VIBE, HIGH);
  else
    IOExpander::digitalWrite(IOExpander::VIBE, LOW);
#endif
}

// IR LEDs on or off
void unPhone::ir(bool on) {
  digitalWrite(unPhone::IR_LEDS, (on) ? HIGH : LOW);
}

void unPhone::rgb(uint8_t red, uint8_t green, uint8_t blue) {
  digitalWrite(IOExpander::LED_RED, red);
  digitalWrite(IOExpander::LED_GREEN, green);
  digitalWrite(IOExpander::LED_BLUE, blue);
}

bool unPhone::button1() { return digitalRead(unPhone::BUTTON1) == LOW; }
bool unPhone::button2() { return digitalRead(unPhone::BUTTON2) == LOW; }
bool unPhone::button3() { return digitalRead(unPhone::BUTTON3) == LOW; }

// get a (spin-agnostic) accelerometer reading
void unPhone::getAccelEvent(sensors_event_t *eventp)
{
#if UNPHONE_SPIN == 6
  accelp->getEvent(eventp);
#elif UNPHONE_SPIN <= 8
  sensors_event_t m, g, temp;
  accelp->getEvent(eventp, &m, &g, &temp);
#else
  sensors_event_t gyro, temp;
  accelp->getEvent(eventp, &gyro, &temp);
#endif
}

// try to recover I2C bus in case it's locked up...
// NOTE: only do this in setup **BEFORE** Wire.begin!
void unPhone::recoverI2C() {
  pinMode(SCL, OUTPUT);
  pinMode(SDA, OUTPUT);
  digitalWrite(SDA, HIGH);

  for(int i = 0; i < 10; i++) { // 9th cycle acts as NACK
    digitalWrite(SCL, HIGH);
    delayMicroseconds(5);
    digitalWrite(SCL, LOW);
    delayMicroseconds(5);
  }

  // a STOP signal (SDA from low to high while SCL is high)
  digitalWrite(SDA, LOW);
  delayMicroseconds(5);
  digitalWrite(SCL, HIGH);
  delayMicroseconds(2);
  digitalWrite(SDA, HIGH);
  delayMicroseconds(2);

  // I2C bus should be free now... a short delay to help things settle
  delay(200);
}

// power management chip API /////////////////////////////////////////////////

float unPhone::batteryVoltage() { // get the battery voltage
  float voltage = -1;
  // in spin7 VBAT_SENSE isn't connected so we use XPT2046
#if UNPHONE_SPIN == 6
  // TODO most of this should be in begin()?
  // set up the ADC and do the read
  pinMode(VBAT_SENSE, INPUT);
  analogReadResolution(12); // 10 bit = 0-1023, 11 = 0-2047, 12 = 0-4095
  analogSetPinAttenuation(
    VBAT_SENSE,
    ADC_6db // 0db is 0-1V, 2.5db is 0-1.5V, 6db is 0-2.2v, 11db is 0-3.3v
  );
  voltage = analogRead(VBAT_SENSE);
  voltage = (voltage / 4095) * 4.4;
#elif UNPHONE_SPIN >= 7
  voltage = tsp->getVBat();
#endif
  return voltage;
}

void unPhone::printWakeupReason() {
  esp_sleep_wakeup_cause_t wakeup_reason;

  wakeup_reason = esp_sleep_get_wakeup_cause();
  char *wakeup_string = (char *) "wakeup reason unknown!";
  char buf[80];

  switch(wakeup_reason) {
    case ESP_SLEEP_WAKEUP_EXT0:
      wakeup_string = (char *) "wakeup caused by external signal using RTC_IO";
      break;
    case ESP_SLEEP_WAKEUP_EXT1:
      wakeup_string = (char *) "wakeup caused by external signal using RTC_CNTL";
      break;
    case ESP_SLEEP_WAKEUP_TIMER:
      wakeup_string = (char *) "wakeup caused by timer";
      break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD:
      wakeup_string = (char *) "wakeup caused by touchpad";
      break;
    case ESP_SLEEP_WAKEUP_ULP:
      wakeup_string = (char *) "wakeup caused by ULP program";
      break;
    case ESP_SLEEP_WAKEUP_UNDEFINED:
      wakeup_string = (char *) "wakeup cause undefined (not from deep sleep)";
      break;
    default:
      sprintf(buf, "wakeup not caused by deep sleep: %d" + wakeup_reason);
      wakeup_string = buf;
      break;
  }
  store(wakeup_string);
}

// is the power switch turned on?
bool unPhone::powerSwitchIsOn() {
  // what is the state of the power switch? (non-zero = on, which is
  // physically slid away from the USB socket)
  return (bool) IOExpander::digitalRead(IOExpander::POWER_SWITCH);
}

// is USB power connected?
bool unPhone::usbPowerConnected() {
  // bit 2 of status register indicates if USB connected
  return (bool) bitRead(getRegister(BM_I2Cadd, BM_Status), 2);
}

// check for power off states and do BM shipping mode (when on bat) or ESP
// deep sleep (when on USB 5V); if it returns then the device is switched on
void unPhone::checkPowerSwitch() {
  if(!powerSwitchIsOn()) {  // when power switch off
    // turn off expander power, LEDs, etc.
    turnPeripheralsOff();

    if(!usbPowerConnected()) { // and usb unplugged we go into shipping mode
      store("switch is off, power is OFF: going to shipping mode");
      setShipping(true); // tell BM to stop supplying power until USB connects
    } else { // power switch off and usb plugged in we sleep
      store("switch is off, but power is ON: going to deep sleep");
      wakeOnPowerSwitch();
      // TODO esp_sleep_enable_timer_wakeup(60000000); // set sleep timer for 1 minute
      esp_sleep_enable_timer_wakeup(6000000); // wake each min: USB still or shipping
      esp_deep_sleep_start(); // deep sleep, wait for wakeup on GPIO
    }
  }
}

// helper to turn off everything we can think of prior to power down or deep sleep
void unPhone::turnPeripheralsOff() {
  expanderPower(false);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT, LOW);
//  lora_shutdown();
  ir(false);            // TODO invert if logic changes!
  unPhone:rgb(1, 1, 1); // TODO invert if logic changes!
}

// set a wakeup interrupt on the power switch
void unPhone::wakeOnPowerSwitch() {
#if UNPHONE_SPIN == 6 || UNPHONE_SPIN == 7
      esp_sleep_enable_ext0_wakeup(GPIO_NUM_36, 0); // 1 = High, 0 = Low
#elif UNPHONE_SPIN == 8
      esp_sleep_enable_ext0_wakeup(GPIO_NUM_4, 0);  // 1 = High, 0 = Low
#elif UNPHONE_SPIN >= 9
      esp_sleep_enable_ext0_wakeup((gpio_num_t) IOExpander::POWER_SWITCH, 1);
      // (changed from low in 9 as is no longer on the expander)
#endif
}

// ask BM chip to shutdown or start up
void unPhone::setShipping(bool value) {
  byte result;
  if(value) {
    result=getRegister(BM_I2Cadd, BM_Watchdog);  // state of timing register
    bitClear(result, 5);                         // clear bit 5...
    bitClear(result, 4);                         // and bit 4 to disable...
    setRegister(BM_I2Cadd, BM_Watchdog, result); // WDT (REG05[5:4] = 00)

    result=getRegister(BM_I2Cadd, BM_OpCon);     // operational register
    bitSet(result, 5);                           // set bit 5 to disable...
    setRegister(BM_I2Cadd, BM_OpCon, result);    // BATFET (REG07[5] = 1)
  } else {
    result=getRegister(BM_I2Cadd, BM_Watchdog);  // state of timing register
    bitClear(result, 5);                         // clear bit 5...
    bitSet(result, 4);                           // and set bit 4 to enable...
    setRegister(BM_I2Cadd, BM_Watchdog, result); // WDT (REG05[5:4] = 01)

    result=getRegister(BM_I2Cadd, BM_OpCon);     // operational register
    bitClear(result, 5);                         // clear bit 5 to enable...
    setRegister(BM_I2Cadd, BM_OpCon, result);    // BATFET (REG07[5] = 0)
  }
}

// I2C helpers to drive the power management chip
void unPhone::setRegister(byte address, byte reg, byte value) {
  write8(address, reg, value);
}
byte unPhone::getRegister(byte address, byte reg) {
  byte result;
  result=read8(address, reg);
  return result;
}
void unPhone::write8(byte address, byte reg, byte value) {
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.write((uint8_t)value);
  Wire.endTransmission();
}
byte unPhone::read8(byte address, byte reg) {
  byte value;
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.endTransmission();
  Wire.requestFrom(address, (byte)1);
  value = Wire.read();
  Wire.endTransmission();
  return value;
}


// the LoRa board and TTN LoRaWAN ///////////////////////////////////////////
void unPhone::loraSetup() { lora_setup(); }     // init the LoRa board
void unPhone::loraLoop() { lora_loop(); }       // service LoRa transactions
void unPhone::loraSend(const char *payload) { lora_send(payload); } // send


// save short sequences of strings using the Preferences API /////////////////
// we use a ring buffer with STORE_SIZE elements stored in NVS;
// each value is keyed by its index (0 to STORE_SIZE - 1)
static Preferences prefs; // an NVS store, used for a persistent ring buffer
static const char storeName[] = "unphoneStore";         // prefs namespace
static const char storeIndexName[] = "unphoneStoreIdx"; // key for ring index
static uint8_t currentStoreIndex = 0; // next position in the ring
void unPhone::beginStore() { // init Prefs, get stored index if present //////
  prefs.begin(storeName, false);
  int8_t storedIndex = prefs.getChar(storeIndexName, -1);
  if(storedIndex != -1) // a previously stored index was found (else use 0)
    currentStoreIndex = (uint8_t) storedIndex;
}
void unPhone::store(const char *s) { // store a value at current index ///////
  char key[3]; // up to 255 max as we're using uint8_t for the index
  sprintf(key, "%d", currentStoreIndex++);
  prefs.putString(key, s); // store the value against the next ring position

  if(currentStoreIndex == STORE_SIZE) // wrap at the end of the ring buffer
    currentStoreIndex = 0;
  prefs.putChar(storeIndexName, currentStoreIndex); // store next index point
}
void unPhone::printStore() { // print all stored values //////////////////////
  Serial.println("------------------------------------------");
  Serial.println("stored messages (oldest first):");
  int printed = 0;
  char key[3]; // up to 255 max as we're using uint8_t for the index
  for(int i = currentStoreIndex; printed < STORE_SIZE; printed++) {
    sprintf(key, "%d", i);
    String value = prefs.getString(key, ""); // get value if stored
    if(value != "")
      Serial.printf("store[%d] = %s\n", i, value.c_str());
    if(++i == STORE_SIZE) i = 0; // wrap at the end of the ring buffer
  }
  Serial.println("------------------------------------------");
}
void unPhone::clearStore() { // delete all values, store 0 as index //////////
  prefs.clear();
  currentStoreIndex = 0;
  prefs.putChar(storeIndexName, currentStoreIndex);
}


// get the ESP's MAC address ///////////////////////////////////////////////
char MAC_ADDRESS[13]; // MAC addresses are 12 chars, plus the NULL terminator
char *getMAC(char *buf) { // the MAC is 6 bytes, so needs careful conversion...
  uint64_t mac = ESP.getEfuseMac(); // ...to string (high 2, low 4):
  char rev[13];
  sprintf(rev, "%04X%08X", (uint16_t) (mac >> 32), (uint32_t) mac);

  // the byte order in the ESP has to be reversed relative to normal Arduino
  for(int i=0, j=11; i<=10; i+=2, j-=2) {
    buf[i] = rev[j - 1];
    buf[i + 1] = rev[j];
  }
  buf[12] = '\0';
  return buf;
}
