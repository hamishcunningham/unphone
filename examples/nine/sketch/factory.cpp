// factory.cpp
// factory test mode, mostly by Gareth and Gee

#include "unphone.h"              // unphone specifics

static bool slideState;
static int loopCounter = 0;
static bool doFlash = true;
static sensors_event_t a, m, g, temp;
static bool doneSetup = false;
static void screenTouched(void);
static void screenError(const char* message);
static bool inFactoryTestMode = false;

bool unPhone::factoryTestMode() {           // read factory test mode
  return inFactoryTestMode;
}
void unPhone::factoryTestMode(bool mode) {  // toggle factory test mode
  inFactoryTestMode = mode;
}

void screenDraw() {
  unPhone::tftp->fillScreen(HX8357_BLACK);
  unPhone::tftp->drawRect(0, 0, 320, 480, HX8357_WHITE);
  unPhone::tftp->setTextSize(3);
  unPhone::tftp->setCursor(0,0);
  unPhone::tftp->setTextColor(HX8357_RED);
  unPhone::tftp->print("Red");
  unPhone::tftp->setCursor(230,0);
  unPhone::tftp->setTextColor(HX8357_GREEN);
  unPhone::tftp->print("Green");
  unPhone::tftp->setCursor(0,460);
  unPhone::tftp->setTextColor(HX8357_BLUE);
  unPhone::tftp->print("Blue");
  unPhone::tftp->setCursor(212,460);
  unPhone::tftp->setTextColor(HX8357_YELLOW);
  unPhone::tftp->print("Yellow");
  unPhone::rgb(1,0,1);
  delay(2000);

  unPhone::tftp->fillScreen(HX8357_BLACK);
  unPhone::tftp->setTextColor(HX8357_RED);
  unPhone::tftp->setCursor(30,50);
  unPhone::tftp->print("But3");

  unPhone::tftp->setCursor(135,50);
  unPhone::tftp->print("But2");

  unPhone::tftp->setCursor(240,50);
  unPhone::tftp->print("But1");

  unPhone::tftp->setCursor(100,100);  
  unPhone::tftp->print("Slide ");
  if (slideState) unPhone::tftp->print("<-"); else unPhone::tftp->print("->");
  unPhone::tftp->drawRect(40, 200, 250, 70, HX8357_MAGENTA);
  unPhone::tftp->setTextSize(4);
  unPhone::tftp->setCursor(70,220);
  unPhone::tftp->setTextColor(HX8357_CYAN);
  unPhone::tftp->print("Touch me");

  unPhone::tftp->setTextColor(HX8357_MAGENTA);
  unPhone::tftp->setTextSize(2);
  unPhone::tftp->setCursor(30,160);
  unPhone::tftp->print("(note power switch");
  unPhone::tftp->setCursor(30,175);
  unPhone::tftp->print("now switches power :)");
}

void screenTouched(void) {
  unPhone::tftp->fillRect(40, 200, 250, 70, HX8357_BLACK);
  unPhone::tftp->drawRect(15, 200, 290, 70, HX8357_CYAN);
  unPhone::tftp->setTextSize(4);
  unPhone::tftp->setCursor(25,220);
  unPhone::tftp->setTextColor(HX8357_GREEN);
  unPhone::tftp->print("I'm touched");
  doFlash=false;
  unPhone::vibe(false);
  unPhone::ir(false);
  unPhone::rgb(0,1,0);
}

void screenError(const char* message) {
  unPhone::rgb(1,0,0);
  unPhone::tftp->fillScreen(HX8357_BLACK);
  unPhone::tftp->setCursor(0,10);
  unPhone::tftp->setTextSize(2);
  unPhone::tftp->setTextColor(HX8357_WHITE);
  unPhone::tftp->print(message);
  delay(5000);
  unPhone::backlight(false);
  unPhone::rgb(0,0,0);
  while(true);
}

void unPhone::factoryTestSetup() {
  unPhone::checkPowerSwitch();
  slideState = IOExpander::digitalRead(IOExpander::POWER_SWITCH);
  Serial.print("Spin 8 test rig, spin ");
  Serial.println(String(UNPHONE_SPIN));
  Serial.print("getVersionNumber() says spin ");
  Serial.println(IOExpander::getVersionNumber());
  unPhone::backlight(true);

  screenDraw();

  Serial.println("screen displayed");
  unPhone::tftp->setTextColor(HX8357_GREEN);
  unPhone::rgb(1,1,1);
// TODO
  if(!unPhone::tsp->begin()) {
    Serial.println("failed to start touchscreen controller");
    screenError("failed to start touchscreen controller");
  } else {
#if UNPHONE_SPIN > 6
    unPhone::tsp->setRotation(1); // should actually be 3 I think
#endif
    Serial.println("Touchscreen started OK");
    unPhone::tftp->setCursor(30,380);
    unPhone::tftp->setTextSize(2);
    unPhone::tftp->print("Touchscreen started");
  }

#if UNPHONE_SPIN < 9
  if(!unPhone::accelp->begin()) {
#else
  if(!unPhone::accelp->begin_I2C()) {
#endif
    Serial.println("failed to start accelerometer");
    screenError("failed to start accelerometer");
  } else {
    Serial.println("Accelerometer started OK");
    unPhone::tftp->setCursor(30,350);
    unPhone::tftp->setTextSize(2);
    unPhone::tftp->print("Accelerometer started");
  }

  // init the SD card
  // see Adafruit_ImageReader/examples/FeatherWingHX8357/FeatherWingHX8357.ino
  IOExpander::digitalWrite(IOExpander::SD_CS, LOW);
  if(!unPhone::sdp->begin(IOExpander::SD_CS, SD_SCK_MHZ(25))) { // ESP32 25 MHz limit
    Serial.println("failed to start SD card");
    screenError("failed to start SD card");
  } else {
    Serial.println("SD Card started OK");
    unPhone::tftp->setCursor(30,410);
    unPhone::tftp->setTextSize(2);
    unPhone::tftp->setTextColor(HX8357_GREEN);
    unPhone::tftp->print("SD Card started");
  }
  IOExpander::digitalWrite(IOExpander::SD_CS, HIGH);

  if(unPhone::getRegister(BM_I2Cadd, BM_Version)!=192) {
    Serial.println("failed to start Battery management");
    screenError("failed to start Battery management");
  } else {
    Serial.println("Battery management started OK");
    unPhone::tftp->setCursor(30,440);
    unPhone::tftp->setTextSize(2);
    unPhone::tftp->print("Batt management started");
  }
  unPhone::tftp->setCursor(30,320);
  unPhone::tftp->setTextSize(2);
  unPhone::tftp->println("LoRa failed");
  Serial.printf("Calling unPhone::loraSetup()\n");
  unPhone::loraSetup(); // init the board and queue a hello message
  // TODO os_init(); // if lora fails then will stop at this init, allowing fail to be seen
  unPhone::tftp->fillRect(30, 320, 250, 32, HX8357_BLACK);
  unPhone::tftp->setCursor(30,320);
  unPhone::tftp->println("LoRa started");
  // LMIC_reset();

  unPhone::tftp->setTextSize(2);
  unPhone::tftp->setCursor(120,140);
  unPhone::tftp->print("spin: ");
  unPhone::tftp->println(UNPHONE_SPIN);

  unPhone::rgb(255,0,0);
  delay(300);
  unPhone::rgb(0,255,0);
  delay(300);
  unPhone::rgb(0,0,255);
  delay(300);
}

void unPhone::factoryTestLoop() {
  if (doFlash) {
    loopCounter++;
    if (loopCounter<50) {
      unPhone::vibe(true);
      unPhone::expanderPower(true);            // enable expander power supply
      unPhone::ir(true);
    } else if (loopCounter>=50) {
      unPhone::vibe(false);
      unPhone::expanderPower(false);           // disable expander power supply
      unPhone::ir(false);
    }
    if (loopCounter>=99) loopCounter=0;
  }
  
  if (unPhone::tsp->touched()) {
    TS_Point p = unPhone::tsp->getPoint();
    if (p.z>40 && p.x>1000 && p.x<3500 && p.y>1300 && p.y<3900) {
      screenTouched();
    }
  }

  if (digitalRead(unPhone::BUTTON1)==LOW) {
    unPhone::tftp->setTextSize(3);
    unPhone::tftp->setCursor(240,50);
    unPhone::tftp->print("But1");    
  }

  if (digitalRead(unPhone::BUTTON2)==LOW) {
    unPhone::tftp->setTextSize(3);
    unPhone::tftp->setCursor(135,50);
    unPhone::tftp->print("But2");    
  }

  if (digitalRead(unPhone::BUTTON3)==LOW) {
    unPhone::tftp->setTextSize(3);
    unPhone::tftp->setCursor(30,50);
    unPhone::tftp->print("But3");    
  }

  if (IOExpander::digitalRead(IOExpander::POWER_SWITCH)!=slideState) {
    slideState=!slideState;
    unPhone::tftp->fillRect(100, 100, 150, 32, HX8357_BLACK);
    unPhone::tftp->setTextSize(3);
    unPhone::tftp->setCursor(130,100);  
    unPhone::tftp->print("Slid");
  }

  unPhone::tftp->fillRect(50, 280, 250, 32, HX8357_BLACK);
  unPhone::tftp->setTextSize(2);
  unPhone::tftp->setCursor(50,280);
#if UNPHONE_SPIN == 6
  unPhone::accelp->getEvent(&a);
#elif UNPHONE_SPIN <= 8
  unPhone::accelp->getEvent(&a, &m, &g, &temp);
#else
  unPhone::accelp->getEvent(&a, &g, &temp);
#endif
  unPhone::tftp->print("X: "); unPhone::tftp->println(a.acceleration.x);
  unPhone::tftp->setCursor(150,280);
  unPhone::tftp->print("Y: "); unPhone::tftp->println(a.acceleration.y);

  // require 3 button presses to check power switch
  if(unPhone::button1() && unPhone::button2() && unPhone::button3())
    unPhone::checkPowerSwitch();
}
