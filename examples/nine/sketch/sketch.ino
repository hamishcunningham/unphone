// sketch.ino

#include <Arduino.h>
#include <freertos/task.h>
#include <Update.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <WiFiManager.h> // force inclusion to fix pio LDF WebServer.h issue
#include <HTTPClient.h>
#include <ESPmDNS.h>
#include "private.h"
#include "sketch.h"
#include "unphone.h"
#include "UIController.h"
#include <Adafruit_EPD.h>

String apSSID = String("nine-");         // SSID of the AP
String apPassword = _DEFAULT_AP_KEY;     // passkey for the AP
static const char BUILD_VERSION[] =
  ("build date: " __DATE__ " " __TIME__); // build data

bool useWifi = true;
WiFiMulti wifiMulti;
HTTPClient http;
UIController *uiCont;
int firmwareVersion = 1; // keep up-to-date! (used to check for updates)
int loopIter = 0;
void wifiSetup();               // TODO move to unPhone?
void wifiConnectTask(void *);   // TODO move to unPhone?

void setup() {
  // say hi, store MAC, blink etc.
  Serial.begin(115200);
  Serial.printf("Starting: %s\n", BUILD_VERSION);
  getMAC(MAC_ADDRESS);          // store the MAC address
  apSSID.concat(MAC_ADDRESS);   // add the MAC to the AP SSID
  Serial.printf("\nsetup...\nESP32 MAC = %s\n", MAC_ADDRESS);

  unPhone::begin();
  unPhone::store(BUILD_VERSION);
  // unPhone::clearStore();

// TODO the LED logic inverted at spin 9; change the rgb() method
/*
while(1) {
D("IR OFF\n") unPhone::ir(false);
D("red on 13 LOW\n")            IOExpander::digitalWrite(13, LOW);          delay(2000);
D("red on 13 HIGH\n")           IOExpander::digitalWrite(13, HIGH);         delay(2000);
D("green 9 | 0x40 LOW\n")       IOExpander::digitalWrite(9 | 0x40, LOW);    delay(2000);
D("green on 9 | 0x40 HIGH\n")   IOExpander::digitalWrite(9 | 0x40, HIGH);   delay(2000);
D("IR on\n") unPhone::ir(true);
D("red on 13 LOW\n")            IOExpander::digitalWrite(13, LOW);          delay(2000);
D("red on 13 HIGH\n")           IOExpander::digitalWrite(13, HIGH);         delay(2000);
D("blue 9 | 0x40 LOW\n")        IOExpander::digitalWrite(13 | 0x40, LOW);   delay(2000);
D("blue on 13 | 0x40 HIGH\n")   IOExpander::digitalWrite(13 | 0x40, HIGH);  delay(2000);
D("RGB...\n")
unPhone::ir(false);
delay(2000);
D("000 all\n")  unPhone::rgb(0, 0, 0); delay(4000); // all
D("111 off\n")  unPhone::rgb(1, 1, 1); delay(4000); // off
D("001 r+g\n")  unPhone::rgb(0, 0, 1); delay(4000); // red + green
D("111 off\n")  unPhone::rgb(1, 1, 1); delay(4000); // off
D("010 r+b\n")  unPhone::rgb(0, 1, 0); delay(4000); // red + blue
D("111 off\n")  unPhone::rgb(1, 1, 1); delay(4000); // off
D("100 g+b\n")  unPhone::rgb(1, 0, 0); delay(4000); // green + blue
D("111 off\n")  unPhone::rgb(1, 1, 1); delay(4000); // off
D("101 grn\n")  unPhone::rgb(1, 0, 1); delay(4000); // green
D("111 off\n")  unPhone::rgb(1, 1, 1); delay(4000); // off
D("110 blue\n") unPhone::rgb(1, 1, 0); delay(4000); // blue
D("111 off\n")  unPhone::rgb(1, 1, 1); delay(4000); // off
D("011 red\n")  unPhone::rgb(0, 1, 1); delay(4000); // red
D("111 off\n")  unPhone::rgb(1, 1, 1); delay(4000); // off
delay(2000);
}
*/

  // if all three buttons pressed, go into factory test mode
  if(unPhone::button1() && unPhone::button2() && unPhone::button3()) {
    unPhone::factoryTestMode(true);
    unPhone::factoryTestSetup();
    return;
  }

  // power management
  unPhone::printWakeupReason(); // what woke us up?
  unPhone::checkPowerSwitch();  // if power switch is off, shutdown
  Serial.printf("battery voltage = %3.3f\n", unPhone::batteryVoltage());
  Serial.printf("enabling expander power\n");
  unPhone::expanderPower(true); // turn expander power on

  // flash the internal RGB LED and then the IR_LEDs
  unPhone::ir(true);
  unPhone::rgb(0, 1, 1); delay(300); unPhone::rgb(1, 0, 1); delay(300);
  unPhone::expanderPower(false); // turn expander power off
  unPhone::rgb(1, 1, 0); delay(300); unPhone::rgb(0, 1, 1); delay(300);
  unPhone::expanderPower(true); // turn expander power on
  unPhone::ir(false);
  unPhone::rgb(1, 0, 1); delay(300); unPhone::rgb(1, 1, 0); delay(300);
  for(uint8_t i = 0; i<4; i++) {
    unPhone::ir(true);  delay(300);
    unPhone::expanderPower(false); // turn expander power on
    unPhone::ir(false); delay(300);
    unPhone::expanderPower(true); // turn expander power on
  }
  unPhone::rgb(0, 0, 0);
  unPhone::rgb(1, 1, 1);

  /*
  TODO
  (this is the old provisioning cycle, but we've replaced it temporarily with
  wifiMulti as below)

  // wifi provisioning
  Serial.printf("doing wifi manager\n");
  uiCont->message("(doing wifi management)");
  joinmeManageWiFi(apSSID.c_str(), apPassword.c_str());
  uiCont->message("");
  Serial.printf("wifi manager done\n\n");
  uiCont->redraw();          // perhaps we're on wifi now, redraw

  // init the web server, say hello
  initWebServer();
  Serial.printf("firmware is at version %d\n", firmwareVersion);

  // check for and perform firmware updates as needed
  delay(2000); // let wifi settle
  uiCont->message("(firmware update check)");
  joinmeOTAUpdate(
    firmwareVersion, _GITLAB_PROJ_ID, "", // use _GITLAB_TOKEN if private repo
    "examples%2FBigDemoIDF%2Ffirmware%2F"
  );
  uiCont->message("");
  if(!MDNS.begin("sketch"))
    Serial.println("Error setting up MDNS responder!");
  */

  // display the first screen
  uiCont = new UIController(ui_configure);
  if(uiCont == NULL || !uiCont->begin())
    E("WARNING: ui.begin failed!\n")

  // buzz a bit
  for(int i = 0; i < 3; i++) {
    unPhone::vibe(true);  delay(150);
    unPhone::vibe(false); delay(150);
  }
  unPhone::printStore(); // print out stored messages

  // get a connection
  if(useWifi) {
    // run the wifi connection task
    Serial.println("trying to connect to wifi...");
    wifiSetup();
    xTaskCreate(wifiConnectTask, "wifi connect task", 4096, NULL, 1, NULL);
  }

  UIController::provisioned = true; // tell the controller done provisioning
  uiCont->redraw();                 // perhaps we're on wifi now, redraw

  // send a LoRaWAN message to TTN
  Serial.printf("Calling unPhone::loraSetup()\n");
  unPhone::loraSetup(); // init the board and queue a hello message

  // WiFi.printDiag(Serial);
  Serial.println("Done with setup()");
}

bool sentLoopMessage = false;   // loop lora message...
int loopMessageIter = 0;        // ...control

void loop() {
  if(unPhone::factoryTestMode()) {
    unPhone::factoryTestLoop();
    return;
  }
  if(loopIter == 0) D("entering main loop\n")
  uiCont->run();

  loopIter++;
  if(loopIter % 25000 == 0)    // allow the protocol CPU IDLE task to run
    delay(100);                // 100 appears min to allow IDLE task to fire
  if(loopIter % 500000 == 0) { // send a lora message
    if(!sentLoopMessage) {
      Serial.println("sending loop lora message");
      char buf[1024];
      sprintf(buf, "hello from the loop, MAC is %s", MAC_ADDRESS);
      unPhone::loraSend(buf);
      sentLoopMessage = true;
    }
  }
  unPhone::loraLoop(); // service lora transactions
}

void wifiSetup() {
// TODO move these to a credentials store, manage with WifiMgr
#ifdef _MULTI_SSID1
  Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID1);
  wifiMulti.addAP(_MULTI_SSID1, _MULTI_KEY1);
#endif
#ifdef _MULTI_SSID2
  Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID2);
  wifiMulti.addAP(_MULTI_SSID2, _MULTI_KEY2);
#endif
#ifdef _MULTI_SSID3
  Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID3);
  wifiMulti.addAP(_MULTI_SSID3, _MULTI_KEY3);
#endif
#ifdef _MULTI_SSID4
  Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID4);
  wifiMulti.addAP(_MULTI_SSID4, _MULTI_KEY4);
#endif
#ifdef _MULTI_SSID5
  Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID5);
  wifiMulti.addAP(_MULTI_SSID5, _MULTI_KEY5);
#endif
#ifdef _MULTI_SSID6
  Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID6);
  wifiMulti.addAP(_MULTI_SSID6, _MULTI_KEY6);
#endif
#ifdef _MULTI_SSID7
  Serial.printf("wifiMulti.addAP %s\n", _MULTI_SSID7);
  wifiMulti.addAP(_MULTI_SSID7, _MULTI_KEY7);
#endif
#ifdef _MULTI_SSID8
  Serial.printf("wifiMulti.addAP 8\n");
  wifiMulti.addAP(_MULTI_SSID8, _MULTI_KEY8);
#endif
}
static bool wifiConnected = false;
void wifiConnectTask(void *param) {
/* TODO probably time to start using std:: to manage this!
#include <string>
#include <map>
#include <iterator>
using namespace std;
map<string, string> ssidMap;
ssidMap.insert(make_pair(_MULTI_SSID1, _MULTI_KEY1));
...
map<string, string>::iterator it = ssidMap.begin();
for (pair<string, string> element : ssidMap) {
  string name = element.first;
  string key = element.second;
  Serial.printf("wifiMulti.addAP %s\n", name);
  wifiMulti.addAP(name, key);
}
*/
  while(true) {
    bool previousWifiState = wifiConnected;
    if(wifiMulti.run() == WL_CONNECTED)
      wifiConnected = true;
    else
      wifiConnected = false;

    // call back to UI controller if state has changed
    if(previousWifiState != wifiConnected) {
      previousWifiState = wifiConnected;
      uiCont->redraw();
    }

    delay(1000);
  }
}
