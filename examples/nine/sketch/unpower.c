// unpower.c

#include "unpower.h"

const byte BM_I2Cadd   = 0x6b; // the chip lives here on I²C
const byte BM_Watchdog = 0x05; // charge termination/timer cntrl reg
const byte BM_OpCon    = 0x07; // misc operation control register
const byte BM_Status   = 0x08; // system status register
const byte BM_Version  = 0x0a; // vender / part / revision status reg
