// unpower.h

#pragma once
#ifdef __cplusplus
extern "C" {
#endif

#include <Arduino.h>

extern const byte BM_I2Cadd;   // the chip lives here on I²C
extern const byte BM_Watchdog; // charge termination/timer control register
extern const byte BM_OpCon;    // misc operation control register
extern const byte BM_Status;   // system status register
extern const byte BM_Version;  // vender / part / revision status register

#ifdef __cplusplus
}
#endif
