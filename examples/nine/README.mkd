An unphone example for spins 6 through 9
===

**Note:** unphone builds require that `../../lib` are all made available, e.g. by
copying them to `Arduino/libraries` or etc.

Build this using PlatformIO core, e.g. (in docker, for all spins):


```
.../the-internet-of-things/support/magic.sh -D
magic.sh test
```
