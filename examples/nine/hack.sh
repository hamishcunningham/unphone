#!/bin/bash

# workarounds for bugs in pio build

[ -L lib ] && rm lib
#[ -L sketch/WebServer.h ] && rm sketch/WebServer.h

[ -e sketch/private.h ] || cp $HOME/the-internet-of-things/support/private-template.h sketch/private.h

[ -e lib ] || ln -s $HOME/unphone/lib
#[ -e sketch/WebServer.h ] || \
#  cd sketch && ln -s $HOME/the-internet-of-things/support/tooling/platformio/packages/framework-arduinoespressif32/libraries/WebServer/src/WebServer.h
