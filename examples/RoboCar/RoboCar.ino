// RoboThing

#include <WiFi.h>
#include <aREST.h>
#include <Wire.h>
#include <IOExpander.h>          // unPhone's IOExpander (controlled via I²C)
#include <Adafruit_MotorShield.h>

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 
 
// And connect 2 DC motors to port M3 & M4 !
Adafruit_DCMotor *L_MOTOR = AFMS.getMotor(4);
Adafruit_DCMotor *R_MOTOR = AFMS.getMotor(3);

byte I2Cadd = 0x6b;      // the I2C address of the battery management chip
byte BM_Watchdog = 0x05; // Charge Termination/Timer Control Register
byte BM_OpCon    = 0x07; // Misc Operation Control Register
byte BM_Status   = 0x08; // System Status Register 
byte BM_Version  = 0x0a; // Vender / Part / Revision Status Register 

// Create aREST instance
aREST rest = aREST();

// The port to listen for incoming TCP connections 
#define LISTEN_PORT           80

// Create an instance of the server
WiFiServer server(LISTEN_PORT);

// Movement functions
int stop(String message);
int forward(String message);
int right(String message);
int left(String message);
int backward(String message);

void setup() {
  Wire.setClock(100000);
  Wire.begin();
  IOExpander::begin();
  checkPowerSwitch();   // Check if the power switch is now off and if so, shutdown

  // Start Serial
  Serial.begin(115200);

  // Init motor shield
  AFMS.begin();  

  // Functions          
  rest.function("stop", stop);
  rest.function("forward", forward);
  rest.function("left", left);
  rest.function("right", right);
  rest.function("backward", backward);
      
  // Give name and ID to device
  rest.set_id("1");
  rest.set_name("robot");
  
  // Connect to WiFi
  WiFi.begin("PiAccessPoint","PIATilapia~2");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
 
  // Start the server
  server.begin();
  Serial.println("Server started");
  
  // Print the IP address
  Serial.println(WiFi.localIP());
  
}

void loop() {
  checkPowerSwitch();   // Check if the power switch is now off and if so, shutdown
  // Handle REST calls
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  while(!client.available()){
    delay(1);
  }
  rest.handle(client);
}

int stop(String command) {
  L_MOTOR->setSpeed(0);
  L_MOTOR->run( RELEASE );
 
  R_MOTOR->setSpeed(0);
  R_MOTOR->run( RELEASE );
}

int forward(String command) {
  L_MOTOR->setSpeed(255);
  L_MOTOR->run( FORWARD );
 
  R_MOTOR->setSpeed(255);
  R_MOTOR->run( FORWARD );
}

int left(String command) {
  L_MOTOR->setSpeed(100);
  L_MOTOR->run( BACKWARD );
 
  R_MOTOR->setSpeed(100);
  R_MOTOR->run( FORWARD );
}

int right(String command) {
  L_MOTOR->setSpeed(100);
  L_MOTOR->run( FORWARD );
 
  R_MOTOR->setSpeed(100);
  R_MOTOR->run( BACKWARD );
}

int backward(String command) {
  L_MOTOR->setSpeed(150);
  L_MOTOR->run( BACKWARD );
 
  R_MOTOR->setSpeed(150);
  R_MOTOR->run( BACKWARD );
}
void checkPowerSwitch() {
  
  uint8_t inputPwrSw = IOExpander::digitalRead(IOExpander::POWER_SWITCH);
  // Serial.print("power switch says ");
  // Serial.println(inputPwrSw);

  bool powerGood = bitRead(getRegister(BM_Status),2); // bit 2 of status register indicates if USB connected

  if (!inputPwrSw) {  // when power switch off
    if (!powerGood) { // and usb unplugged we go into shipping mode
      Serial.println("Setting shipping mode to true");
      Serial.print("Status is: ");
      Serial.println(getRegister(BM_Status));
      delay(500); // allow serial buffer to empty
      setShipping(true);
    } else { // power switch off and usb plugged in we sleep
      Serial.println("sleeping now");
      Serial.print("Status is: ");
      Serial.println(getRegister(BM_Status));
      delay(500); // allow serial buffer to empty
      esp_sleep_enable_timer_wakeup(1000000); // sleep time is in uSec
      esp_deep_sleep_start();
    }
  }
}

void setShipping(bool value) {
  byte result;
  if (value) {
    result=getRegister(BM_Watchdog);   // Read current state of timing register
    bitClear(result, 5);               // clear bit 5
    bitClear(result, 4);               // and bit 4
    setRegister(BM_Watchdog,result); // to disable the watchdog timer (REG05[5:4] = 00)

    result=getRegister(BM_OpCon);      // Read current state of operational register
    bitSet(result, 5);                 // Set bit 5
    setRegister(BM_OpCon,result);   // to disable BATFET (REG07[5] = 1)
  } else {
    result=getRegister(BM_Watchdog);  // Read current state of timing register
    bitClear(result, 5);               // clear bit 5
    bitSet(result, 4);                 // and set bit 4
    setRegister(BM_Watchdog,result); // to enable the watchdog timer (REG05[5:4] = 01)

    result=getRegister(BM_OpCon);     // Read current state of operational register
    bitClear(result, 5);               // Clear bit 5
    setRegister(BM_OpCon,result);    // to enable BATFET (REG07[5] = 0)
  }
}

void setRegister(byte reg, byte value) {
  write8(I2Cadd,reg, value);
}

byte getRegister(byte reg) {
  byte result;
  result=read8(I2Cadd,reg);
  return result;
}

void write8(byte address, byte reg, byte value) {
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.write((uint8_t)value);
  Wire.endTransmission();
}

byte read8(byte address, byte reg) {
  byte value;
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.endTransmission();
  Wire.requestFrom(address, (byte)1);
  value = Wire.read();
  Wire.endTransmission();
  return value;
}
