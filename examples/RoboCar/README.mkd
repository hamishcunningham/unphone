# Robot Car

This project was a great basis for confident students who had ideas to extend
it - by adding ultrasonic distance sensors, or enabling bluetooth control, or
autonomously seeking a strong wifi signal.

To get the demo working, once you have programmed your esp32 with the .ino
file you can discover it’s IP address either on the serial port or via your
hotspot. Using a laptop, join the same network as the esp32. Edit the
script.js file inside the interface directory to replace the IP address with
that for your ESP32. Now you can run the demo.html file in a browser for wifi
control of the robot!
