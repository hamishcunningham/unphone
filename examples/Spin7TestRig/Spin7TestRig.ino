/***************************************************
 Hacked so comprehensively no point/need attributing it to any one thing anymore...
 Mostly rewritten and bugs created by Gareth, original cleverness from Hamish, Gee, adafruit, other library authors, the internet etc
 ****************************************************/

#include <unphone.h>              // unphone specifics
#if UNPHONE_SPIN == 6
#  define SPIN6
#elif UNPHONE_SPIN == 7
#  define SPIN7
#  define EXPANDER_POWER unPhone::EXPANDER_POWER // power to expander when HIGH
#endif

#include <lmic.h>                 // IBM LMIC (LoraMAC-in-C) library
#include <hal/hal.h>              // hardware abstraction for LMIC on Arduino
#include <dummy.h>                // fool the build into including all of ...
#include <ideetron/dummy2.h>      // ... lib/arduino-lmic/aes

#define IR_LED  unPhone::IR_LEDS
#define LCD_DC  unPhone::LCD_DC
#define BUT1    unPhone::BUTTON1
#define BUT2    unPhone::BUTTON2
#define BUT3    unPhone::BUTTON3

#ifdef SPIN7
int spin = 7;
// TODO move to setup if using unphone.begin()
XPT2046_Touchscreen ts(IOExpander::TOUCH_CS);  // no IRQ
Adafruit_LSM9DS1 accel = Adafruit_LSM9DS1();   // i2c
sensors_event_t a, m, g, temp;
#else
int spin = 6;
// TODO move to setup if using unphone.begin()
Adafruit_STMPE610 ts = Adafruit_STMPE610(IOExpander::TOUCH_CS);
Adafruit_LSM303_Accel_Unified accel = Adafruit_LSM303_Accel_Unified(54321);
sensors_event_t event;
#endif

Adafruit_HX8357 tft = Adafruit_HX8357(IOExpander::LCD_CS, LCD_DC, IOExpander::LCD_RESET);

#include <SdFat.h>                // SD card & FAT filesystem library
SdFat SD;

byte read8(byte address, byte reg);
byte getRegister(byte address, byte reg);

bool slideState;
volatile int loopCounter = 0;
bool doFlash = true;

byte BM_I2Cadd = 0x6b;
byte BM_Status   = 0x08; // System Status Register 
byte BM_Version  = 0x0a; // Vender / Part / Revision Status Register 

const lmic_pinmap lmic_pins = {
     .nss = IOExpander::LORA_CS,
     .rxtx = LMIC_UNUSED_PIN,
     .rst = IOExpander::LORA_RESET,
     .dio = { 39, 26, LMIC_UNUSED_PIN },
     .rxtx_rx_active = 0,
     .rssi_cal = 0,
     .spi_freq = 8000000,
};

byte getRegister(byte address, byte reg) {
  byte result;
  result=read8(address,reg);
  return result;
}

byte read8(byte address, byte reg) {
  byte value;
  Wire.beginTransmission(address);
  Wire.write((uint8_t)reg);
  Wire.endTransmission();
  Wire.requestFrom(address, (byte)1);
  value = Wire.read();
  Wire.endTransmission();
  return value;
}

void screenDraw(void) {
  tft.fillScreen(HX8357_BLACK);
  tft.drawRect(0, 0, 320, 480, HX8357_WHITE);
  tft.setTextSize(3);
  tft.setCursor(0,0);
  tft.setTextColor(HX8357_RED);
  tft.print("Red");
  tft.setCursor(230,0);
  tft.setTextColor(HX8357_GREEN);
  tft.print("Green");
  tft.setCursor(0,460);
  tft.setTextColor(HX8357_BLUE);
  tft.print("Blue");
  tft.setCursor(212,460);
  tft.setTextColor(HX8357_YELLOW);
  tft.print("Yellow");
  IOExpander::digitalWrite(IOExpander::LED_GREEN, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, LOW);
  IOExpander::digitalWrite(IOExpander::LED_RED, LOW);

  delay(1000);

  tft.fillScreen(HX8357_BLACK);
  tft.setTextColor(HX8357_RED);
  tft.setCursor(30,50);
  tft.print("But3");

  tft.setCursor(135,50);
  tft.print("But2");

  tft.setCursor(240,50);
  tft.print("But1");

  tft.setCursor(100,100);  
  tft.print("Slide ");
  if (slideState) tft.print("<-"); else tft.print("->");
  tft.drawRect(40, 200, 250, 70, HX8357_MAGENTA);
  tft.setTextSize(4);
  tft.setCursor(70,220);
  tft.setTextColor(HX8357_CYAN);
  tft.print("Touch me");
}

void screenTouched(void) {
  tft.fillRect(40, 200, 250, 70, HX8357_BLACK);
  tft.drawRect(15, 200, 290, 70, HX8357_CYAN);
  tft.setTextSize(4);
  tft.setCursor(25,220);
  tft.setTextColor(HX8357_GREEN);
  tft.print("I'm touched");
  doFlash=false;
  #ifdef SPIN7
  IOExpander::digitalWrite(IOExpander::VIBE,LOW);
  #else
  ts.writeRegister8(0x11, 8);   // set GPIO3 LOW to stop vibe
  #endif
  digitalWrite(IR_LED,LOW);
  IOExpander::digitalWrite(IOExpander::LED_GREEN, LOW);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_RED, HIGH);
}

void screenError(const char* message) {
  IOExpander::digitalWrite(IOExpander::LED_GREEN, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_RED, LOW);
  tft.fillScreen(HX8357_BLACK);
  tft.setCursor(0,10);
  tft.setTextSize(2);
  tft.setTextColor(HX8357_WHITE);
  tft.print(message);
  delay(5000);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT,LOW);
  while(true);
}

void setup() {
  // TODO we should be using unPhone::begin here, but that would mean changing
  // all uses of tft and ts...
  Wire.begin();
  IOExpander::begin();
  pinMode(BUT1,INPUT_PULLUP);
  pinMode(BUT2,INPUT_PULLUP);
  pinMode(BUT3,INPUT_PULLUP);
  slideState = IOExpander::digitalRead(IOExpander::POWER_SWITCH);
  Serial.begin(115200);
  #ifndef SPIN7
  Serial.println("Spin 7 test rig in spin 6 mode");
  #else
  Serial.println("Spin 7 test rig in true spin 7 mode");
  // TODO delete if using unPhone::begin
  pinMode(EXPANDER_POWER,OUTPUT);
  #endif

  Serial.printf("setting IR_LED at %d to OUTPUT\n", IR_LED);
  // TODO delete if using unPhone::begin
  pinMode(IR_LED,OUTPUT);
  
  Serial.print("getVersionNumber() says spin ");
  Serial.println(IOExpander::getVersionNumber());
  
  // TODO delete if using unPhone::begin
  tft.begin(HX8357D);
  IOExpander::digitalWrite(IOExpander::BACKLIGHT,HIGH);
  screenDraw();
  Serial.println("screen displayed");
  tft.setTextColor(HX8357_GREEN);
  IOExpander::digitalWrite(IOExpander::LED_GREEN, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_RED, HIGH);
  if(! ts.begin()) {
    Serial.println("failed to start touchscreen controller");
    screenError("failed to start touchscreen controller");
  } else {
    #ifdef SPIN7
    ts.setRotation(1); // should actually be 3 I think
    #endif
    Serial.println("Touchscreen started OK");
    tft.setCursor(30,380);
    tft.setTextSize(2);
    tft.print("Touchscreen started");
  }
  #ifndef SPIN7
  ts.writeRegister8(0x17, 12);  // use GPIO2 & 3 (bcd)
  ts.writeRegister8(0x13, 12); // all gpio's as outputs to minimise power consumption on unused
  #endif

  if(!accel.begin()) {
    Serial.println("failed to start accelerometer");
    screenError("failed to start accelerometer");
  } else {
    Serial.println("Accelerometer started OK");
    tft.setCursor(30,350);
    tft.setTextSize(2);
    tft.print("Accelerometer started");
  }

  // init the SD card
  // see Adafruit_ImageReader/examples/FeatherWingHX8357/FeatherWingHX8357.ino
  IOExpander::digitalWrite(IOExpander::SD_CS, LOW);
  if(!SD.begin(IOExpander::SD_CS, SD_SCK_MHZ(25))) { // ESP32 25 MHz limit
    Serial.println("failed to start SD card");
    screenError("failed to start SD card");
  } else {
    Serial.println("SD Card started OK");
    tft.setCursor(30,410);
    tft.setTextSize(2);
    tft.setTextColor(HX8357_GREEN);
    tft.print("SD Card started");
  }
  IOExpander::digitalWrite(IOExpander::SD_CS, HIGH);

  if (getRegister(BM_I2Cadd,BM_Version)!=192){
    Serial.println("failed to start Battery management");
    screenError("failed to start Battery management");
  } else {
    Serial.println("Battery management started OK");
    tft.setCursor(30,440);
    tft.setTextSize(2);
    tft.print("Batt management started");
  }
    tft.setCursor(30,320);
    tft.setTextSize(2);
    tft.println("LORA failed");
    os_init(); // if lora fails then will stop at this init, allowing fail to be seen
    tft.fillRect(30, 320, 250, 32, HX8357_BLACK);
    tft.setCursor(30,320);
    tft.println("LoRa started");
    LMIC_reset();

  tft.setTextSize(2);
  tft.setCursor(120,150);
  tft.print("spin: ");
  tft.println(spin);

  IOExpander::digitalWrite(IOExpander::LED_GREEN, LOW);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, LOW);
  IOExpander::digitalWrite(IOExpander::LED_RED, HIGH);
  delay(300);
  IOExpander::digitalWrite(IOExpander::LED_GREEN, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, LOW);
  IOExpander::digitalWrite(IOExpander::LED_RED, LOW);
  delay(300);
  IOExpander::digitalWrite(IOExpander::LED_GREEN, LOW);
  IOExpander::digitalWrite(IOExpander::LED_BLUE, HIGH);
  IOExpander::digitalWrite(IOExpander::LED_RED, LOW);
  delay(300);
 }

void loop(void) {
  // TODO no IR LEDs on my spin 7 test board :(
  /*
  Serial.printf("setting %d high\n", IR_LED);
  digitalWrite(IR_LED,HIGH);
  delay(500);
  Serial.printf("setting %d low\n", IR_LED);
  digitalWrite(IR_LED,LOW);
  delay(500);
  return;
  */

  if (doFlash){
    loopCounter++;
    if (loopCounter<50) {
      #ifdef SPIN7
      IOExpander::digitalWrite(IOExpander::VIBE,HIGH);
      digitalWrite(EXPANDER_POWER,HIGH); // enable expander power supply
      #else
      ts.writeRegister8(0x10, 8);   // set GPIO3 HIGH to start vibe
      #endif
      digitalWrite(IR_LED,HIGH);
    } else if (loopCounter>=50) {
      #ifdef SPIN7
      IOExpander::digitalWrite(IOExpander::VIBE,LOW);
      digitalWrite(EXPANDER_POWER,HIGH); // disable expander power supply
      #else
      ts.writeRegister8(0x11, 8);   // set GPIO3 LOW to stop vibe
      #endif
      digitalWrite(IR_LED,LOW);
    }
    if (loopCounter>=99) loopCounter=0;
  }
  
  if (ts.touched()) {
    TS_Point p = ts.getPoint();
    if (p.z>40 && p.x>1000 && p.x<3500 && p.y>1300 && p.y<3900){
      screenTouched();
    }
  }

  if (digitalRead(BUT1)==LOW) {
    tft.setTextSize(3);
    tft.setCursor(240,50);
    tft.print("But1");    
  }

  #ifdef SPIN7
  if (digitalRead(BUT2)==LOW) {
  #else
  if (IOExpander::digitalRead(BUT2)==LOW) {
  #endif
    tft.setTextSize(3);
    tft.setCursor(135,50);
    tft.print("But2");    
  }

  if (digitalRead(BUT3)==LOW) {
    tft.setTextSize(3);
    tft.setCursor(30,50);
    tft.print("But3");    
  }

  if (IOExpander::digitalRead(IOExpander::POWER_SWITCH)!=slideState) {
    slideState=!slideState;
    tft.fillRect(100, 100, 150, 32, HX8357_BLACK);
    tft.setTextSize(3);
    tft.setCursor(130,100);  
    tft.print("Slid");
  }

  tft.fillRect(50, 280, 250, 32, HX8357_BLACK);
  tft.setTextSize(2);
  tft.setCursor(50,280);
  #ifdef SPIN7
  accel.getEvent(&a, &m, &g, &temp);
  tft.print("X: "); tft.println(a.acceleration.x);
  tft.setCursor(150,280);
  tft.print("Y: "); tft.println(a.acceleration.y);
  #else
  accel.getEvent(&event);
  tft.print("X: "); tft.println(event.acceleration.x);
  tft.setCursor(150,280);
  tft.print("Y: "); tft.println(event.acceleration.y);
  #endif
}
