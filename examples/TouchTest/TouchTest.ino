#include <XPT2046_Touchscreen.h>
#include <Wire.h>
#include <SPI.h>
#include "IOExpander7.h"          // unPhone's IOExpander (controlled via I²C)
XPT2046_Touchscreen ts(IOExpander::TOUCH_CS);  // Param 2 - NULL - No interrupts

void setup() {
  Serial.begin(115200);
  Wire.begin();
  IOExpander::begin();
  ts.begin();
  ts.setRotation(1);
  while (!Serial && (millis() <= 1000));
}

void loop() {
  if (ts.touched()) {
    TS_Point p = ts.getPoint();
    Serial.print("Pressure = ");
    Serial.print(p.z);
    Serial.print(", x = ");
    Serial.print(p.x);
    Serial.print(", y = ");
    Serial.print(p.y);
    delay(30);
    Serial.println();
  }
}

