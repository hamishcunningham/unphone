// lora.cpp
// adapted from
// https://github.com/mcci-catena/arduino-lmic/tree/master/examples/ttn-otaa-feather-us915
// for unPhone 2022 by Gareth Coleman & Hamish Cunningham
// API details are here:
// https://github.com/mcci-catena/arduino-lmic/blob/master/doc/LMIC-v4.1.0.pdf
//
// (notes on upgrading to https://github.com/mcci-catena/arduino-lorawan at end)

/*******************************************************************************
 * Copyright (c) 2015 Thomas Telkamp and Matthijs Kooijman
 * Copyright (c) 2018 Terry Moore, MCCI
 * Adapted for use on UnPhone 2022 Gareth Coleman & Hamish Cunningham

 * Permission is hereby granted, free of charge, to anyone
 * obtaining a copy of this document and accompanying files,
 * to do whatever they want with them without any restriction,
 * including, but not limited to, copying, modification and redistribution.
 * NO WARRANTY OF ANY KIND IS PROVIDED.
 *
 * This example sends a valid LoRaWAN packet with payload "Hello,
 * world!", using frequency and encryption settings matching those of
 * the The Things Network. It's pre-configured for the Adafruit
 * Feather M0 LoRa.
 *
 * This uses OTAA (Over-the-air activation), where where a DevEUI and
 * application key is configured, which are used in an over-the-air
 * activation procedure where a DevAddr and session keys are
 * assigned/generated for use with all further communication.
 *
 * Note: LoRaWAN per sub-band duty-cycle limitation is enforced (1% in
 * g1, 0.1% in g2), but not the TTN fair usage policy (which is probably
 * violated by this sketch when left running for longer)!

 * To use this sketch, first register your application and device with
 * the things network, to set or generate an AppEUI, DevEUI and AppKey.
 * Multiple devices can use the same AppEUI, but each device has its own
 * DevEUI and AppKey.
 *
 * TODO Do not forget to define the radio type correctly in platformio.ini,
 * arduino-lmic/project_config/lmic_project_config.h or from your BOARDS.txt.
 *
 *******************************************************************************/

#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <unphone.h>

// key data: copy these values from TTN, as lsb
// (gareth's, test out v3, unphone-spin7-test)
#define _LORA_DEV_EUI { 0x79, 0xF7, 0x04, 0xD0, 0x7E, 0xD5, 0xB3, 0x70 }
#define _LORA_APP_EUI { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }
#define _LORA_APP_KEY { 0xB5, 0xD2, 0x59, 0x69, 0xA0, 0xD2, 0x0D, 0xBD, 0xF1, 0x20, 0x8B, 0x3D, 0x28, 0xCE, 0xCD, 0xE3 }

// if we hit mkesp issue with missing dependencies:
//#include <dummy.h>
//#include <aes/ideetron/dummy2.h>

// TODO defined in radio.c; useful for turning the module off
static void requestModuleActive(bit_t);
static void moduleOff() { requestModuleActive(0); }
// TODO LMIC_shutdown

void do_send(osjob_t* j);
void do_send(osjob_t* j, uint8_t payload[], uint8_t payloadSize);

// The DevEUI must be in little-endian format, so least-significant-byte
// first. When copying an EUI from ttnctl output, this means to reverse
// the bytes. For TTN issued EUIs the last bytes should be 0xD5, 0xB3,
// 0x70.
// e.g. #define _LORA_DEV_EUI { 0x01, 0x02, 0x03, 0x04, 0x05, 0xD5, 0xB3, 0x70 }

static const u1_t PROGMEM DEVEUI[8]= _LORA_DEV_EUI;
void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}

// The JoinEUI  should also be in little endian format, see above.
// previously known as the AppEUI. Apparently it's ok to use all zeros?
// e.g. #define _LORA_APP_EUI { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 }

static const u1_t PROGMEM APPEUI[8]= _LORA_APP_EUI;
void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}

// The AppKey should be in big endian format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from the TTN console can be copied as-is.
// e.g. #define _LORA_APP_KEY { 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10 }

static const u1_t PROGMEM APPKEY[16] = _LORA_APP_KEY;
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}

static uint8_t mydata[] = "Gareth says: hello world!!!!";

static osjob_t sendjob;

// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
const unsigned TX_INTERVAL = 60;

// Pin mapping
const lmic_pinmap lmic_pins = {
 .nss = IOExpander::LORA_CS,
 .rxtx = LMIC_UNUSED_PIN,
 .rst = IOExpander::LORA_RESET,
  // moved to pins_arduino.h: .dio = { 39, 26, LMIC_UNUSED_PIN },
 .dio = {
   unPhone::LMIC_DIO0, unPhone::LMIC_DIO1, LMIC_UNUSED_PIN
 },
 .rxtx_rx_active = 0,
 .rssi_cal = 0,
 .spi_freq = 8000000,
};

void printHex2(unsigned v) {
    v &= 0xff;
    if (v < 16)
        Serial.print('0');
    Serial.print(v, HEX);
}

void onEvent (ev_t ev) {
    Serial.print(os_getTime());
    Serial.print(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            Serial.println(F("EV_SCAN_TIMEOUT"));
            break;
        case EV_BEACON_FOUND:
            Serial.println(F("EV_BEACON_FOUND"));
            break;
        case EV_BEACON_MISSED:
            Serial.println(F("EV_BEACON_MISSED"));
            break;
        case EV_BEACON_TRACKED:
            Serial.println(F("EV_BEACON_TRACKED"));
            break;
        case EV_JOINING:
            Serial.println(F("EV_JOINING"));
            break;
        case EV_JOINED:
            Serial.println(F("EV_JOINED"));
            {
              u4_t netid = 0;
              devaddr_t devaddr = 0;
              u1_t nwkKey[16];
              u1_t artKey[16];
              LMIC_getSessionKeys(&netid, &devaddr, nwkKey, artKey);
              Serial.print("netid: ");
              Serial.println(netid, DEC);
              Serial.print("devaddr: ");
              Serial.println(devaddr, HEX);
              Serial.print("AppSKey: ");
              for (size_t i=0; i<sizeof(artKey); ++i) {
                if (i != 0)
                  Serial.print("-");
                printHex2(artKey[i]);
              }
              Serial.println("");
              Serial.print("NwkSKey: ");
              for (size_t i=0; i<sizeof(nwkKey); ++i) {
                      if (i != 0)
                              Serial.print("-");
                      printHex2(nwkKey[i]);
              }
              Serial.println();
            }
            // Disable link check validation (automatically enabled
            // during join, but because slow data rates change max TX
	    // size, we don't use it in this example.
            LMIC_setLinkCheckMode(0);
            break;
        /*
        || This event is defined but not used in the code. No
        || point in wasting codespace on it.
        ||
        || case EV_RFU1:
        ||     Serial.println(F("EV_RFU1"));
        ||     break;
        */
        case EV_JOIN_FAILED:
            Serial.println(F("EV_JOIN_FAILED"));
            break;
        case EV_REJOIN_FAILED:
            Serial.println(F("EV_REJOIN_FAILED"));
            break;
            break;
        case EV_TXCOMPLETE:
            Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
            if (LMIC.txrxFlags & TXRX_ACK)
              Serial.println(F("Received ack"));
            if (LMIC.dataLen) {
              Serial.println(F("Received "));
              Serial.println(LMIC.dataLen);
              Serial.println(F(" bytes of payload"));
            }
/*
HC: don't queue another send, we'll only do another if asked!
            // Schedule next transmission
            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
*/
            break;
        case EV_LOST_TSYNC:
            Serial.println(F("EV_LOST_TSYNC"));
            break;
        case EV_RESET:
            Serial.println(F("EV_RESET"));
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            Serial.println(F("EV_RXCOMPLETE"));
            break;
        case EV_LINK_DEAD:
            Serial.println(F("EV_LINK_DEAD"));
            break;
        case EV_LINK_ALIVE:
            Serial.println(F("EV_LINK_ALIVE"));
            break;
        /*
        || This event is defined but not used in the code. No
        || point in wasting codespace on it.
        ||
        || case EV_SCAN_FOUND:
        ||    Serial.println(F("EV_SCAN_FOUND"));
        ||    break;
        */
        case EV_TXSTART:
            Serial.println(F("EV_TXSTART"));
            break;
        case EV_TXCANCELED:
            Serial.println(F("EV_TXCANCELED"));
            break;
        case EV_RXSTART:
            /* do not print anything -- it wrecks timing */
            break;
        case EV_JOIN_TXCOMPLETE:
            Serial.println(F("EV_JOIN_TXCOMPLETE: no JoinAccept"));
            break;

        default:
            Serial.print(F("Unknown event: "));
            Serial.println((unsigned) ev);
            break;
    }
}

void do_send(osjob_t* j) {
    uint8_t payload[] = "default lora payload (unphone lora_setup)";
    uint8_t payloadSize = strlen((const char *) payload);
    do_send(j, payload,  payloadSize);
}

void do_send(osjob_t* j, uint8_t payload[], uint8_t payloadSize) {
    // Check if there is not a current TX/RX job running
    if (LMIC.opmode & OP_TXRXPEND) {
        Serial.println(F("OP_TXRXPEND, not sending"));
    } else {
        // Prepare upstream data transmission at the next possible time.
        LMIC_setTxData2(1, payload, payloadSize, 0);
        Serial.println(F("Packet queued"));
    }
    // Next TX is scheduled after TX_COMPLETE event.
}

void lora_setup() {
    // LMIC init
    Serial.println("doing os_init()...");
    os_init();
    // Reset the MAC state. Session and pending data transfers will be discarded.
    Serial.println("doing LMIC_reset()...");
    LMIC_reset();

    Serial.println("doing LMIC_setLinkCheckMode()...");
    LMIC_setLinkCheckMode(0);
    Serial.println("doing LMIC_setDrTxpow()...");
    LMIC_setDrTxpow(DR_SF7,14);
    // TODO https://github.com/forrestfiller/-arduino-feather-32u4-lora/issues/1
    Serial.println("NOT doing LMIC_selectSubBand()...");
    // LMIC_selectSubBand(1);

    // Start job (sending automatically starts OTAA too)
    Serial.println("doing do_send()...");
    do_send(&sendjob);
}

void lora_loop() {
    os_runloop_once();
}

void lora_send(const char * payload) {
    do_send(&sendjob, (uint8_t *)payload, strlen(payload));
}


////////////////////////////////////////////////////////////////////////////
// MCCI do another higher-level library that might be good to replace this
// one... some notes on upgrading below, and see
// https://github.com/mcci-catena/arduino-lorawan

/*
// lmic_project_config.h //////////////////////////////////////////////////
// project-specific definitions
#define CFG_eu868 1
#define CFG_sx1276_radio 1


// new code for lora.cpp //////////////////////////////////////////////////
#include <Arduino_LoRaWAN_ttn.h>

class cMyLoRaWAN : public Arduino_LoRaWAN_ttn {
public:
  cMyLoRaWAN(){};

protected:
  // you'll need to provide implementations for this.
  virtual bool
    GetOtaaProvisioningInfo(Arduino_LoRaWAN::OtaaProvisioningInfo *) override;
  // if you have persistent storage, you can provide implementations for these:
  virtual void NetSaveSessionInfo(
    const SessionInfo &Info, const uint8_t *pExtraInfo, size_t nExtraInfo
  ) override;
  virtual void NetSaveSessionState(const SessionState &State) override;
  virtual bool NetGetSessionState(SessionState &State) override;
};

// set up the data structures.
cMyLoRaWAN myLoRaWAN{};

// provide pin mapping, including the ones on the expander
const cMyLoRaWAN::lmic_pinmap myPinMap = {
  .nss = IOExpander::LORA_CS,
  .rxtx = cMyLoRaWAN::lmic_pinmap::LMIC_UNUSED_PIN,
  .rst = IOExpander::LORA_RESET,
  .dio = {39, 26, cMyLoRaWAN::lmic_pinmap::LMIC_UNUSED_PIN},
  .rxtx_rx_active = 0,
  .rssi_cal = 0,
  .spi_freq = 1000000,
};

void lorawanUplinkDone(void *pCtx, bool fSuccess) {
  Serial.println("uplink done.");
}

// this method is called when the LMIC needs OTAA info.
// return false to indicate "no provisioning", otherwise
// fill in the data and return true.
bool cMyLoRaWAN::GetOtaaProvisioningInfo(OtaaProvisioningInfo *pInfo) {
    // deveui, little-endian aka lsb
    static const uint8_t DEVEUI[8] =
      {0xAA, 0xD6, 0x04, 0xD0, 0x7E, 0xD5, 0xB3, 0x70};

    // appeui, little-endian aka lsb
    static const uint8_t APPEUI[8] =
      {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    // appkey: just a string of bytes, sometimes referred to as "big endian" aka msb
    static const uint8_t APPKEY[16] =
      {0x09, 0xF8, 0x42, 0xA8, 0xB6, 0x99, 0xDF, 0x4A,
       0x9A, 0x27, 0x4C, 0x58, 0x04, 0x14, 0x45, 0x4A};

    if (pInfo) {
        memcpy(pInfo->AppKey, APPKEY, sizeof(APPKEY));
        memcpy(pInfo->DevEUI, DEVEUI, sizeof(DEVEUI));
        memcpy(pInfo->AppEUI, APPEUI, sizeof(APPEUI));
    }
    return true;
}

void cMyLoRaWAN::NetSaveSessionInfo(
    const SessionInfo &Info,
    const uint8_t *pExtraInfo,
    size_t nExtraInfo)
{
    // save Info somewhere. not implemented yet
}

void cMyLoRaWAN::NetSaveSessionState(const SessionState &State)
{
    // save State somewhere. Note that it's often the same;
    // often only the frame counters change. not implemented yet
}

bool cMyLoRaWAN::NetGetSessionState(SessionState &State)
{
    // either fetch SessionState from somewhere and return true or...
    // not implemented yet, so we return false
    return false;
}


// new code for sketch.ino //////////////////////////////////////////////////
  Serial.printf("Calling myLoRaWAN.begin()\n");
  myLoRaWAN.begin(myPinMap);
  if (!myLoRaWAN.IsProvisioned()) {
    Serial.printf("LoRaWAN not provisioned yet. Use the commands to set it up.\n");
  } else {
    // send a confirmed uplink
    Serial.printf("LoRaWAN provisioned. Trying to send a message\n");
    const uint8_t uplinkText[] = "Hello world from the unphone!\n";
    if(myLoRaWAN.SendBuffer(
      uplinkText, sizeof(uplinkText), lorawanUplinkDone, nullptr, true
    ))
      Serial.printf("gfTxStarted!\n");
    else
      Serial.printf("SendBuffer failed!\n");
  }
  Serial.printf("...done (TTN)\n");
*/
