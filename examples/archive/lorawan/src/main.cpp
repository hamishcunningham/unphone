#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include "../../../lib/unPhone_Library/src/IOExpander.h"
#include <Arduino_LoRaWAN_ttn.h>

class cMyLoRaWAN : public Arduino_LoRaWAN_ttn
{
public:
    cMyLoRaWAN(){};

protected:
    // you'll need to provide implementations for this.
    virtual bool GetOtaaProvisioningInfo(Arduino_LoRaWAN::OtaaProvisioningInfo *) override;
    // if you have persistent storage, you can provide implementations for these:
    virtual void NetSaveSessionInfo(const SessionInfo &Info, const uint8_t *pExtraInfo, size_t nExtraInfo) override;
    virtual void NetSaveSessionState(const SessionState &State) override;
    virtual bool NetGetSessionState(SessionState &State) override;
};

// set up the data structures.
cMyLoRaWAN myLoRaWAN{};

// provide pin mapping, including the ones on the expander
const cMyLoRaWAN::lmic_pinmap myPinMap = {
    .nss = IOExpander::LORA_CS,
    .rxtx = cMyLoRaWAN::lmic_pinmap::LMIC_UNUSED_PIN,
    .rst = IOExpander::LORA_RESET,
    .dio = {39, 26, cMyLoRaWAN::lmic_pinmap::LMIC_UNUSED_PIN},
    .rxtx_rx_active = 0,
    .rssi_cal = 0,
    .spi_freq = 1000000,
};

void uplinkDone(void *pCtx, bool fSuccess)
{
    Serial.println("uplink done.");
}

void setup()
{
    Serial.begin(115200);
    Serial.println("LoRaWAN test sketch");
    Wire.begin();
    //IOExpander::begin();

    Serial.println("Calling myLoRaWAN.begin()");
    myLoRaWAN.begin(myPinMap);

    if (!myLoRaWAN.IsProvisioned())
        Serial.println("LoRaWAN not provisioned yet. Use the commands to set it up.\n");
    else
    {
        // send a confirmed uplink
        Serial.println("LoRaWAN provisioned. Trying to send a message");
        const uint8_t uplinkText[] = "Hello world from the unphone!";
        if (myLoRaWAN.SendBuffer(uplinkText, sizeof(uplinkText), uplinkDone, nullptr, true))
            Serial.println("gfTxStarted!\n");
        else
            Serial.println("SendBuffer failed!\n");
    }
}

void loop()
{
    Serial.println("That's all folks!");
    delay(1000);
    myLoRaWAN.loop();
}

// this method is called when the LMIC needs OTAA info.
// return false to indicate "no provisioning", otherwise
// fill in the data and return true.
bool cMyLoRaWAN::GetOtaaProvisioningInfo(
    OtaaProvisioningInfo *pInfo)
{
    // deveui, little-endian aka lsb
    static const uint8_t DEVEUI[8] = {0x82, 0xD6, 0x04, 0xD0, 0x7E, 0xD5, 0xB3, 0x70};

    // appeui, little-endian aka lsb
    static const uint8_t APPEUI[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

    // appkey: just a string of bytes, sometimes referred to as "big endian" aka msb
    static const uint8_t APPKEY[16] = {0xA7, 0xA5, 0x5D, 0x4D, 0x6F, 0x5C, 0x00, 0x64, 0xA9, 0xB2, 0xBA, 0x8F, 0x45, 0xF8, 0xED, 0xA2};

    if (pInfo)
    {
        memcpy(pInfo->AppKey, APPKEY, sizeof(APPKEY));
        memcpy(pInfo->DevEUI, DEVEUI, sizeof(DEVEUI));
        memcpy(pInfo->AppEUI, APPEUI, sizeof(APPEUI));
    }
    return true;
}

void cMyLoRaWAN::NetSaveSessionInfo(
    const SessionInfo &Info,
    const uint8_t *pExtraInfo,
    size_t nExtraInfo)
{
    // save Info somewhere. not implemented yet
}

void cMyLoRaWAN::NetSaveSessionState(const SessionState &State)
{
    // save State somewhere. Note that it's often the same;
    // often only the frame counters change. not implemented yet
}

bool cMyLoRaWAN::NetGetSessionState(SessionState &State)
{
    // either fetch SessionState from somewhere and return true or...
    // not implemented yet, so we return false
    return false;
}
