/***************************************************
 Hacked so comprehensively no point/need attributing it to any one thing anymore...
 Mostly rewritten and bugs created by Gareth, original cleverness from Hamish, Gee, adafruit, other library authors, the internet etc
 ****************************************************/
#define UNPHONE_SPIN 7
#include "unphone.h"

sensors_event_t event;
volatile int mappedX, mappedY, mappedP;

void irFlash(){
  Serial.printf("setting %d high\n", unPhone::IR_LEDS);
  digitalWrite(unPhone::IR_LEDS,HIGH);
  delay(500);
  Serial.printf("setting %d low\n", unPhone::IR_LEDS);
  digitalWrite(unPhone::IR_LEDS,LOW);
  delay(500);
}

void drawTouch(){
  TS_Point p = unPhone::tsp->getPoint();
  mappedX = map(p.y, 3700, 400, 0, 320);
  mappedY = map(p.x, 3700, 300, 0, 480);
  mappedP = map(p.z, 1200, 3000, 0, 24);
  unPhone::tftp->fillCircle(mappedX,mappedY,mappedP,HX8357_MAGENTA);
  delay(30);
}

void printAccel(){
  unPhone::tftp->fillRect(50, 280, 212, 14, HX8357_BLACK);
  unPhone::tftp->setTextSize(2);
  unPhone::tftp->setCursor(50,280);
  unPhone::getAccelEvent(&event);
  unPhone::tftp->print("X: "); unPhone::tftp->print(event.acceleration.x);
  unPhone::tftp->setCursor(162,280);
  unPhone::tftp->print("Y: "); unPhone::tftp->print(event.acceleration.y);
}

void setup() {
  unPhone::begin();
  Serial.begin(115200);
  Serial.print("Spin ");
  Serial.print(UNPHONE_SPIN);  
  Serial.println(" test accel & ts");
  
  unPhone::backlight(false);
  unPhone::tftp->fillScreen(HX8357_BLACK);
  unPhone::tftp->drawRect(0, 20, unPhone::tftp->width(), unPhone::tftp->height()-40, HX8357_WHITE);
  unPhone::tftp->setTextSize(3);
  unPhone::tftp->setCursor(0,30);
  unPhone::tftp->setTextColor(HX8357_GREEN);
  unPhone::backlight(true);
  Serial.println("screen displayed");
}

void loop(void) {
  //irFlash();
  if (unPhone::tsp->touched()) drawTouch();
  printAccel();
  }
