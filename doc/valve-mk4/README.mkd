Low cost flow control valve for aquaponic flood/drain, version 4
===


A new version of the super-cheapo flow
control valve for the upcoming ponics at Regather...

Currently using:

- [Polypipe Biocote
  WS12W](https://www.polypipe.com/housing/above-ground-drainage/waste/solvent-weld-waste-system/waste-solvent-weld-abs-40mm-3m-pipe-white-ws12w)
  43mm solvent weld pipe (ABS with an antibacterial coating) 
- [Floplast
  PVC-U](https://www.screwfix.com/p/floplast-sp3g-single-socket-soil-pipe-black-110mm-x-3m/49565)
  110mm soil pipe
- [Kockney Koi](https://www.amazon.co.uk/gp/product/B007UN5SQQ) 1.5" Tank /
  Liner Connector

The outer needs to be around 45cm; the inner 16mm shorter:

[ ![](pics/relative-pipe-lengths-680x1209.jpg "relative pipe lengths") ](pics/relative-pipe-lengths.jpg)

Some pics:

[ ![](pics/pipe-examples-01-680x510.jpg "pipe example 01") ](pics/pipe-examples-01.jpg)

[ ![](pics/pipe-examples-02-680x510.jpg "pipe example 01") ](pics/pipe-examples-02.jpg)

[ ![](pics/proto-2020-08-10--18-03-06-680x510.jpg "valve prototype, 2020") ](pics/proto-2020-08-10--18-03-06.jpg)

[ ![](pics/proto-2020-09-29--10-21-57-680x510.jpg "valve prototype, 2020") ](pics/proto-2020-09-29--10-21-57.jpg)

[ ![](pics/proto-2020-09-29--10-22-00-680x510.jpg "valve prototype, 2020") ](pics/proto-2020-09-29--10-22-00.jpg)

[ ![](pics/proto-2020-09-29--10-22-04-680x510.jpg "valve prototype, 2020") ](pics/proto-2020-09-29--10-22-04.jpg)

[ ![](pics/proto-2020-09-29--10-22-08-680x510.jpg "valve prototype, 2020") ](pics/proto-2020-09-29--10-22-08.jpg)

[ ![](pics/proto-2020-09-29--10-22-17-680x907.jpg "valve prototype, 2020") ](pics/proto-2020-09-29--10-22-17.jpg)

[ ![](pics/proto-2020-10-11--18-17-24-680x510.jpg "valve prototype, 2020") ](pics/proto-2020-10-11--18-17-24.jpg)

[ ![](pics/proto-2020-10-11--18-17-29-680x510.jpg "valve prototype, 2020") ](pics/proto-2020-10-11--18-17-29.jpg)

[ ![](pics/proto-2020-11-12--09-02-20-680x510.jpg "valve prototype, 2020") ](pics/proto-2020-11-12--09-02-20.jpg)


[A blurry video](https://photos.app.goo.gl/mFrX1vuaJXboEWXVA)

[The model files (.stl)](../../waterelf/valve-v4/)

