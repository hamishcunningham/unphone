// ValveScript.ino

#include <Wire.h>
#include <Adafruit_MotorShield.h>

#define trigPin A6   // CHANGE FOR BOARD
#define echoPin A5   // CHANGE FOR BOARD
#define limitPin A4  // CHANGE FOR BOARD

#define LowLim 5 //Water line from sensor
#define UpLim 30 //Water line from sensor

Adafruit_MotorShield AFMS = Adafruit_MotorShield(0x60);  // Create shield object
//default I2C addess of 0x60, increase with stacked boards

Adafruit_DCMotor *myMotor = AFMS.getMotor(1);            // DC motor on 'port' 1

void setup() {
  pinMode(trigPin, OUTPUT);                              //trigger pin for ultrasonic sensor
  pinMode(echoPin, INPUT);                               //echo pin for ultrasonic sensor
  pinMode(limitPin, INPUT);                              //digital input limit switches in parallel

  AFMS.begin();
  Serial.begin(9600);
}

void loop() {
  int val;
  val = analogRead(limitPin);
  long distance;

  distance = UltraSonic();                              //ultrasonic read
  
  if(distance < LowLim){                                //if water level too high, open valve
    OpenValve();
  }else if(distance > UpLim){                           //if water level too low, close valve
    CloseValve();
  }else if((distance > LowLim) && (distance < UpLim)){  //if water level is goldilocks, close valve
    myMotor->run(RELEASE); 
  }
}

void CloseValve(){
  myMotor->run(BACKWARD);                               //Close valve, run motor
  myMotor->setSpeed(230);                               //set speed for motor
  delay(200);                                           //wait 1sec for limit switch to open again
  while(analogRead(limitPin) < 4000){}                  //wait until other switch is closed
  myMotor->run(RELEASE);                                //and then stop the motor
}

void OpenValve(){                         
  myMotor->run(FORWARD);                                //Open valve, run motor
  myMotor->setSpeed(230);                               //Set speed for motor
  delay(200);                                           //wait 1sec for limit switch to open again
  while(analogRead(limitPin) < 4000){}                  //wait until other swtich is closed
  myMotor->run(RELEASE);                                //stop the motor
}

long UltraSonic(){
  long duration, distance[3], AvDist;
  
  for(int i=0; i<3; i++){
    digitalWrite(trigPin, LOW);                         //ensure no pulse is being sent out
    delayMicroseconds(5);
  
    digitalWrite(trigPin, HIGH);                        //send pulse
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);     
  
    duration = pulseIn(echoPin, HIGH);                  //read length of time for pulse to come back
  
    distance[i] = duration * 0.0343/2;                  //evaluate distance in cm
    delay(100);
  }                                                     //repeat 3 times for an average, in case of water splashing etc.

  AvDist = (distance[0] + distance[1] + distance[2])/3; //average the values  
  Serial.println(AvDist);
  return(AvDist);                                       //return averaged distance
}
