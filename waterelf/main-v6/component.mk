# "main" pseudo-component makefile

UNPHONE_INC_DIR=../../../src
UNPHONE_PARENT_DIR=../../../..
UNPHONE_SRC_DIR=../../src
UNPHONE_LIB_DIR=../../lib

CPPFLAGS += -I$(UNPHONE_INC_DIR) -I$(UNPHONE_PARENT_DIR) \
  -Wno-all \
  -Wno-error=return-type -Wno-write-strings -Wno-conversion-null \
  -Wno-return-type -Wno-pointer-arith -Wno-cpp -Wno-unused-variable \

WATERELF_LIBS := $(UNPHONE_SRC_DIR) \
  $(UNPHONE_LIB_DIR)/ESPAsyncWebServer/src \
  $(UNPHONE_LIB_DIR)/AsyncTCP/src/ \
  $(UNPHONE_LIB_DIR)/OneWire \
  $(UNPHONE_LIB_DIR)/ArduinoJson/src \
  $(UNPHONE_LIB_DIR)/WiFiManager \
  $(UNPHONE_LIB_DIR)/RCSwitch \
  $(UNPHONE_LIB_DIR)/Arduino-Temperature-Control-Library \
  $(UNPHONE_LIB_DIR)/Adafruit_TSL2591_Library \
  $(UNPHONE_LIB_DIR)/Adafruit_Sensor \
  $(UNPHONE_LIB_DIR)/EmonLib \
  $(UNPHONE_LIB_DIR)/ESP32-IRremote \
  $(UNPHONE_LIB_DIR)/Adafruit_LSM303DLHC \
  $(UNPHONE_LIB_DIR)/Adafruit_ImageReader \
  $(UNPHONE_LIB_DIR)/Adafruit_STMPE610 \
  $(UNPHONE_LIB_DIR)/Adafruit_HX8357_Library \
  $(UNPHONE_LIB_DIR)/Adafruit-GFX-Library \
  $(UNPHONE_LIB_DIR)/arduino-lmic/src \
  $(UNPHONE_LIB_DIR)/arduino-lmic/src/aes \
  $(UNPHONE_LIB_DIR)/arduino-lmic/src/aes/ideetron \
  $(UNPHONE_LIB_DIR)/arduino-lmic/src/hal \
  $(UNPHONE_LIB_DIR)/arduino-lmic/src/lmic \

COMPONENT_SRCDIRS += $(WATERELF_LIBS)
COMPONENT_ADD_INCLUDEDIRS := $(WATERELF_LIBS)
