#!/bin/bash
#
# template.sh

# standard locals
alias cd='builtin cd'
P="$0"
USAGE="`basename ${P}` [-h(elp)] [-d(ebug)] [-n(no xyz)] [-i [1|2|3|...]"
DBG=:
OPTIONSTRING=hdni:
R='\033[0;31m' # red (use with echo -e)
G='\033[0;32m' # green
B='\033[1;34m' # blue
N='\033[0m'    # no color

# specific locals
ABC=0
USEXYZ="1"

# message & exit if exit num present
e() { (
  C=""; case $1 in R|G|B|N) C=${!1}; shift; ;; esac; echo -e "${C}$*${N}";
) }
usage() { e G "Usage: $USAGE"; [ ! -z "$1" ] && exit $1; }

# process options
while getopts $OPTIONSTRING OPTION
do
  case $OPTION in
    h)	usage 0 ;;
    d)	DBG=echo ;;
    n)	USEXYZ="" ;;
    i)	ABC="${OPTARG}" ;;
    *)	usage 1 ;;
  esac
done 
shift `expr $OPTIND - 1`

# TODO
doit() {
  echo hello
}

cd ../../build
echo first crash:
for a in \
0x4019f61a:0x3ffbdf00 0x400d30fa:0x3ffbdf20 0x400946f9:0x3ffbdf40 0x400931e1:0x3ffbdf60 0x40084e39:0x3ffc1700 0x40117781:0x3ffc1720 0x40117885:0x3ffc1740 0x40117959:0x3ffc18a0 0x400f5b72:0x3ffc18c0 0x400fe5a5:0x3ffc18e0 0x400e6b1d:0x3ffc1910 0x400931e1:0x3ffc1930
do
  xtensa-esp32-elf-addr2line -pfiaC -e waterelf.elf $a
done
echo

echo second crash:
for a in \
0x4019f61a:0x3ffbdf00 0x400d30fa:0x3ffbdf20 0x400946f9:0x3ffbdf40 0x400931e1:0x3ffbdf60 0x40084e22:0x3ffc1700 0x40117781:0x3ffc1720 0x40117885:0x3ffc1740 0x40117959:0x3ffc18a0 0x400f5b72:0x3ffc18c0 0x400fe5a5:0x3ffc18e0 0x400e6b1d:0x3ffc1910 0x400931e1:0x3ffc1930
do
  xtensa-esp32-elf-addr2line -pfiaC -e waterelf.elf $a
done
echo

echo third crash:
for a in \
0x4019f61a:0x3ffbdf00 0x400d30fa:0x3ffbdf20 0x400946f9:0x3ffbdf40 0x400931e1:0x3ffbdf60 0x40084e24:0x3ffc1670 0x40117781:0x3ffc1690 0x40117885:0x3ffc16b0 0x40117959:0x3ffc1810 0x400f5b72:0x3ffc1830 0x400fe5a5:0x3ffc1850 0x400e6b1d:0x3ffc1880 0x400931e1:0x3ffc18a0
do
  xtensa-esp32-elf-addr2line -pfiaC -e waterelf.elf $a
done
echo

