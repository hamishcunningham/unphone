EESchema Schematic File Version 4
LIBS:pumperv3-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Pump, solenoid and ultrasonic breakout board"
Date "2019-02-06"
Rev "3"
Comp "BitFIXit"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L pumperv3-rescue:8P8C-Connector J1
U 1 1 5BD9AE2E
P 3700 3250
F 0 "J1" H 3500 3800 50  0000 R CNN
F 1 "8P8C" H 4000 3800 50  0000 R CNN
F 2 "boardlibrary:TE-Connectivity-5520259-4" V 3700 3275 50  0001 C CNN
F 3 "~" V 3700 3275 50  0001 C CNN
	1    3700 3250
	1    0    0    1   
$EndComp
$Comp
L pumperv3-rescue:Conn_01x04_Female-Connector J4
U 1 1 5BD9B228
P 5600 2450
F 0 "J4" H 5500 2150 50  0000 L CNN
F 1 "Conn_01x04_Female" H 5400 2150 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 5600 2450 50  0001 C CNN
F 3 "~" H 5600 2450 50  0001 C CNN
	1    5600 2450
	1    0    0    1   
$EndComp
$Comp
L pumperv3-rescue:Conn_01x02_Female-Connector J2
U 1 1 5BD9B351
P 5550 3350
F 0 "J2" H 5450 3150 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5350 3450 50  0001 L CNN
F 2 "Connector_JST:JST_XH_B02B-XH-A_1x02_P2.50mm_Vertical" H 5550 3350 50  0001 C CNN
F 3 "~" H 5550 3350 50  0001 C CNN
	1    5550 3350
	1    0    0    1   
$EndComp
$Comp
L pumperv3-rescue:Conn_01x02_Female-Connector J3
U 1 1 5BD9B3AF
P 5550 3950
F 0 "J3" H 5450 3750 50  0000 L CNN
F 1 "Conn_01x02_Female" H 5350 4050 50  0001 L CNN
F 2 "Connector_JST:JST_XH_B02B-XH-A_1x02_P2.50mm_Vertical" H 5550 3950 50  0001 C CNN
F 3 "~" H 5550 3950 50  0001 C CNN
	1    5550 3950
	1    0    0    1   
$EndComp
Text Label 4200 3250 0    50   ~ 0
5V
Text Label 4200 3050 0    50   ~ 0
RX
Text Label 4200 3150 0    50   ~ 0
TX
Text Label 4200 2950 0    50   ~ 0
GND
Text Label 4200 3450 0    50   ~ 0
3V
Wire Wire Line
	4450 3650 4100 3650
Text Label 4200 3550 0    50   ~ 0
PMP
Text Label 4200 3650 0    50   ~ 0
SOL
$Comp
L pumperv3-rescue:1N4001-Diode D1
U 1 1 5BD9EB90
P 4950 3300
F 0 "D1" H 4850 3500 50  0000 C CNN
F 1 "1N4148" H 4950 3400 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 4950 3125 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 4950 3300 50  0001 C CNN
	1    4950 3300
	0    1    1    0   
$EndComp
$Comp
L pumperv3-rescue:1N4001-Diode D2
U 1 1 5BD9F07E
P 4950 3900
F 0 "D2" H 4850 4100 50  0000 C CNN
F 1 "1N4148" H 4950 4000 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 4950 3725 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88503/1n4001.pdf" H 4950 3900 50  0001 C CNN
	1    4950 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 2750 4750 2750
Wire Wire Line
	4450 2250 4450 2850
Wire Wire Line
	4450 2950 4100 2950
Wire Wire Line
	4100 3050 4550 3050
Wire Wire Line
	4550 3050 4550 2450
Wire Wire Line
	4650 2350 4650 3150
Wire Wire Line
	4100 3450 4850 3450
Wire Wire Line
	4650 3150 4100 3150
Wire Wire Line
	4100 3250 4750 3250
Wire Wire Line
	4450 2250 5400 2250
Wire Wire Line
	4750 2550 5400 2550
$Comp
L LED:ASMB-MTB1-0A3A2 D3
U 1 1 5BDC90CF
P 6700 3300
F 0 "D3" H 6700 3797 50  0000 C CNN
F 1 "LED_RABG" H 6700 3706 50  0000 C CNN
F 2 "boardlibrary:LED_Lite-On_LTST-S310F3WT_3.2x1.5mmx1_Side_View" H 6700 3250 50  0001 C CNN
F 3 "~" H 6700 3250 50  0001 C CNN
	1    6700 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 3300 6900 2750
$Comp
L pumperv3-rescue:R-Device R2
U 1 1 5BDC9CDC
P 6250 3300
F 0 "R2" V 6050 3250 50  0000 C CNN
F 1 "200R" V 6150 3300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6180 3300 50  0001 C CNN
F 3 "~" H 6250 3300 50  0001 C CNN
	1    6250 3300
	0    1    1    0   
$EndComp
Wire Wire Line
	4750 2550 4750 2750
Wire Wire Line
	6500 3300 6400 3300
Wire Wire Line
	6100 3300 6050 3300
Wire Wire Line
	6050 3300 6050 2850
Connection ~ 4450 2850
Wire Wire Line
	4450 2850 4450 2950
Connection ~ 4750 2750
Wire Wire Line
	4750 2750 4750 3250
$Comp
L pumperv3-rescue:R-Device R1
U 1 1 5BDCE990
P 6250 3000
F 0 "R1" V 6050 2950 50  0000 C CNN
F 1 "330R" V 6150 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6180 3000 50  0001 C CNN
F 3 "~" H 6250 3000 50  0001 C CNN
	1    6250 3000
	0    1    1    0   
$EndComp
$Comp
L pumperv3-rescue:R-Device R3
U 1 1 5BDCE9C2
P 6250 3600
F 0 "R3" V 6050 3550 50  0000 C CNN
F 1 "200R" V 6150 3600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6180 3600 50  0001 C CNN
F 3 "~" H 6250 3600 50  0001 C CNN
	1    6250 3600
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 3000 6450 3000
Wire Wire Line
	6450 3000 6450 3100
Wire Wire Line
	6450 3100 6500 3100
Wire Wire Line
	6500 3500 6450 3500
Wire Wire Line
	6450 3500 6450 3600
Wire Wire Line
	6450 3600 6400 3600
Wire Wire Line
	6100 3000 5850 3000
Wire Wire Line
	5850 3000 5850 3550
Wire Wire Line
	6050 4150 6050 3600
Wire Wire Line
	6050 3600 6100 3600
NoConn ~ 4150 3350
Text Label 4200 3350 0    50   ~ 0
NC
Wire Wire Line
	4150 3350 4100 3350
Text Notes 2850 2950 0    50   ~ 0
Orange/White
Text Notes 2850 3050 0    50   ~ 0
Orange
Text Notes 2850 3150 0    50   ~ 0
Green/White
Text Notes 2850 3450 0    50   ~ 0
Green
Text Notes 2850 3250 0    50   ~ 0
Blue
Text Notes 2850 3350 0    50   ~ 0
Blue/White
Text Notes 2850 3550 0    50   ~ 0
Brown/White
Text Notes 2850 3650 0    50   ~ 0
Brown
Text Notes 2850 2800 0    50   ~ 0
T568B
Text Notes 5400 3450 0    50   Italic 0
GRN
Wire Wire Line
	4100 3550 4950 3550
Wire Wire Line
	4950 3450 4950 3550
Connection ~ 4950 3550
Wire Wire Line
	4450 4150 4450 3650
Wire Wire Line
	4450 4150 4950 4150
Wire Wire Line
	4850 3050 4850 3450
Wire Wire Line
	4850 3450 4850 3650
Wire Wire Line
	4850 3650 4950 3650
Connection ~ 4850 3450
Wire Wire Line
	4950 3750 4950 3650
Wire Wire Line
	4950 4150 4950 4050
Connection ~ 4950 4150
Wire Wire Line
	6050 2850 4450 2850
Wire Wire Line
	5400 2450 4550 2450
Wire Wire Line
	4650 2350 5400 2350
Wire Wire Line
	4950 3550 5300 3550
Wire Wire Line
	4850 3050 4950 3050
Wire Wire Line
	4950 3050 4950 3150
Wire Wire Line
	5350 3350 5300 3350
Wire Wire Line
	5300 3350 5300 3550
Connection ~ 5300 3550
Wire Wire Line
	5300 3550 5850 3550
Wire Wire Line
	4950 4150 5300 4150
Wire Wire Line
	5350 3250 5300 3250
Wire Wire Line
	5300 3250 5300 3050
Wire Wire Line
	5300 3050 4950 3050
Connection ~ 4950 3050
Wire Wire Line
	5350 3850 5300 3850
Wire Wire Line
	5300 3850 5300 3650
Wire Wire Line
	5300 3650 4950 3650
Connection ~ 4950 3650
Wire Wire Line
	5350 3950 5300 3950
Wire Wire Line
	5300 3950 5300 4150
Connection ~ 5300 4150
Wire Wire Line
	5300 4150 6050 4150
$EndSCHEMATC
