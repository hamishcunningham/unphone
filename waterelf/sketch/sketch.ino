// app_main.c - from RainMaker GPIO example

#include <string.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_log.h>
#include <nvs_flash.h>

#include <esp_rmaker_core.h>
#include <esp_rmaker_standard_types.h>
extern "C" {
#include "app_wifi.h"
#include "app_reset.h"
}

#include <stdint.h>
#include <stdbool.h>
#include <esp_err.h>

#include <sdkconfig.h>
#include <string.h>
#include <esp_log.h>

#include "sketch.h"

// run RM? (comment if not)
// #define DO_RAINMAKER 1

// app driver stuff
#define RMT_TX_CHANNEL RMT_CHANNEL_0
#define BUTTON_GPIO          (gpio_num_t) 0
#define BUTTON_ACTIVE_LEVEL  (button_active_t) 0
// TODO 14 clashes! 19 is the one currently working on the valve board (LED)
#define OUTPUT_GPIO_RED   (gpio_num_t) 19ULL
// TODO 14 swapped for 34
#define OUTPUT_GPIO_GREEN (gpio_num_t) 34ULL
#define OUTPUT_GPIO_BLUE  (gpio_num_t) 15ULL
#define WIFI_RESET_BUTTON_TIMEOUT       3
#define FACTORY_RESET_BUTTON_TIMEOUT    10

// app driver interface
void app_driver_init(void);
esp_err_t app_driver_set_gpio(const char *name, bool state);

// arduino land interface
void flipv();
void setup_rainmaker();

// logging
static const char *TAG = "app_main";

// callback to handle commands received from the RainMaker cloud
static esp_err_t write_cb(
  const esp_rmaker_device_t *device, const esp_rmaker_param_t *param,
  const esp_rmaker_param_val_t val, void *priv_data,
  esp_rmaker_write_ctx_t *ctx
) {
  if(
    app_driver_set_gpio(esp_rmaker_param_get_name(param), val.val.b) == ESP_OK
  ) {
    esp_rmaker_param_update_and_report(param, val);
  }
  return ESP_OK;
}

// rainmaker entry point
void setup_rainmaker() {
#ifdef DO_RAINMAKER
  Serial.println("doing ESP RainMaker...");

  // initialize application specific hardware drivers and set initial state
  app_driver_init();

  // initialize NVS
  esp_err_t err = nvs_flash_init();
  if(
    err == ESP_ERR_NVS_NO_FREE_PAGES ||
    err == ESP_ERR_NVS_NEW_VERSION_FOUND
  ) {
    ESP_ERROR_CHECK(nvs_flash_erase());
    err = nvs_flash_init();
  }
  ESP_ERROR_CHECK( err );

  // initialize Wi-Fi; note this should be called before esp_rmaker_init()
  app_wifi_init();
  
  // initialize the ESP RainMaker agent
  // note should be called after app_wifi_init() but before app_wifi_start()
  esp_rmaker_config_t rainmaker_cfg = {
    .enable_time_sync = false,
  };
  esp_rmaker_node_t *node =
    esp_rmaker_node_init(&rainmaker_cfg, "RainElf", "ESP32 Feather Huzzah");
  if(!node) {
    ESP_LOGE(TAG, "Could not initialise node. Aborting!!!");
    vTaskDelay(5000/portTICK_PERIOD_MS);
    abort();
  }

  // create a device and add the relevant parameters to it
  esp_rmaker_device_t *gpio_device =
    esp_rmaker_device_create("RainElf", NULL, NULL);
  esp_rmaker_device_add_cb(gpio_device, write_cb, NULL);

  esp_rmaker_param_t *red_param = esp_rmaker_param_create(
    "Red", NULL, esp_rmaker_bool(false), PROP_FLAG_READ | PROP_FLAG_WRITE
  );
  esp_rmaker_param_add_ui_type(red_param, ESP_RMAKER_UI_TOGGLE);
  esp_rmaker_device_add_param(gpio_device, red_param);

  esp_rmaker_param_t *green_param = esp_rmaker_param_create(
    "Valve 1", NULL, esp_rmaker_bool(false), PROP_FLAG_READ | PROP_FLAG_WRITE
  );
  esp_rmaker_param_add_ui_type(green_param, ESP_RMAKER_UI_TOGGLE);
  esp_rmaker_device_add_param(gpio_device, green_param);

  esp_rmaker_param_t *blue_param = esp_rmaker_param_create(
    "Valve 2", NULL, esp_rmaker_bool(false), PROP_FLAG_READ | PROP_FLAG_WRITE
  );
  esp_rmaker_param_add_ui_type(blue_param, ESP_RMAKER_UI_TOGGLE);
  esp_rmaker_device_add_param(gpio_device, blue_param);

  esp_rmaker_node_add_device(node, gpio_device);

  // start the ESP RainMaker agent
  esp_rmaker_start();

  // start Wi-Fi; if node is provisioned, will start connection attempts,
  // else, will start Wi-Fi provisioning; will return
  // after a connection has been successfully established
  err = app_wifi_start(POP_TYPE_RANDOM);
  if(err != ESP_OK) {
    ESP_LOGE(TAG, "Could not start Wifi. Aborting!!!");
    vTaskDelay(5000/portTICK_PERIOD_MS);
    abort();
  }

  Serial.println("rainmaker setup complete");
#endif
} // setup_rainmaker()

// helper to toggle GPIO, used by the cloud callback handler
esp_err_t app_driver_set_gpio(const char *name, bool state) {
  if(strcmp(name, "Red") == 0) {
    gpio_set_level(OUTPUT_GPIO_RED, state);
  } else if(strcmp(name, "Valve 1") == 0) {
    // TODO
    // gpio_set_level(OUTPUT_GPIO_GREEN, state);
    flipv();
  } else if(strcmp(name, "Valve 2") == 0) {
    // TODO
    // gpio_set_level(OUTPUT_GPIO_BLUE, state);
  } else {
    return ESP_FAIL;
  }
  return ESP_OK;
}

// setup GPIO etc.
void app_driver_init() {
  app_reset_button_register(
    app_reset_button_create(BUTTON_GPIO, BUTTON_ACTIVE_LEVEL),
    WIFI_RESET_BUTTON_TIMEOUT,
    FACTORY_RESET_BUTTON_TIMEOUT
  );

  // configure power
  gpio_config_t io_conf = {
    .mode = GPIO_MODE_OUTPUT,
    .pull_up_en = (gpio_pullup_t) 1,
  };
  uint64_t pin_mask = (((uint64_t)1 << OUTPUT_GPIO_RED ) |
    ((uint64_t)1 << OUTPUT_GPIO_GREEN ) | ((uint64_t)1 << OUTPUT_GPIO_BLUE ));
  io_conf.pin_bit_mask = pin_mask;

  // configure the GPIO
  gpio_config(&io_conf);
  gpio_set_level(OUTPUT_GPIO_RED, false);
  gpio_set_level(OUTPUT_GPIO_GREEN, false);
  gpio_set_level(OUTPUT_GPIO_BLUE, false);
}
