// ard_land.cpp

/*
TODO

- farm setup: L (v0) on black; MID (v1) on red; R (v2) on blue
- MID and L generate bot travel switch errors when flooding
  ==> coat the switches
- timer cycling needs checking via the LEDs across the motors
- MID switching is suspect, check


- adapt to dckrbldr.sh and containerise
- swap rainmaker for old style web interface?
- remove prints from motor library
- do we sometimes lose events on travel switch close, and hence leave a motor
  running in open or closed states? should we cache atTop/atBottom reads and
  intervene somehow?


NOTES

valve switch GPIO handling:

- almost everything is static, with events mapping GPIO numbers to valves in
  Valve::gpioEventQueueTask and then calling the (non-static)
  Valve.handleGpioEvent
- sequence:
  - the travel and trigger switches are registered with the ISR service to
    interrupt on falling edge (they're all pull-downs)
  - the ISR calls gpio_isr_handler which queues the GPIO number
  - Valve::gpioEventQueueTask polls the queue, and uses Valve::pin2vnum to
    identify which valve owns this event (and discards other events as
    debounce #1)
  - the respective valve then responds via Valve.handleGpioEvent
  - Valve.handleGpioEvent checks the status of the switches (debounce #2) and
    calls complete() or release() or flip(top,bot) as appropriate; clicks very
    soon after TRIGGER_SINGLECLICK are discarded (debounce #3)


board wiring notes:

- v1 green, grey/brown
- v2 orange, white/purple
- v3 yellow, blue/green

*/

#include <Arduino.h>
#include <Adafruit_MotorShield.h>

extern "C" { // interface for app_main.c
}
void flipv(); // TODO temporary! (interface to app_main)
void setup_rainmaker();

/////////////////////////////////////////////////////////////////////////////
// utilities
//
// delay/yield macros
#define WAIT_A_SEC   vTaskDelay(    1000/portTICK_PERIOD_MS); // 1 second
#define WAIT_SECS(n) vTaskDelay((n*1000)/portTICK_PERIOD_MS); // n seconds
#define WAIT_MS(n)   vTaskDelay(       n/portTICK_PERIOD_MS); // n millis
#define WAIT_A_TICK  vTaskDelay(1);                           // 1 tick
//
int firmwareVersion = 400; // keep up-to-date! (used to check for updates)
#define ECHECK ESP_ERROR_CHECK_WITHOUT_ABORT

/////////////////////////////////////////////////////////////////////////////
// gpio interrupt events; timer interrupt events (flood/drain cycling)
static xQueueHandle gpio_evt_queue = NULL;   // switch event queue
static void IRAM_ATTR gpio_isr_handler(void *arg) {
  uint32_t gpio_num = (uint32_t) arg;
  xQueueSendFromISR(gpio_evt_queue, &gpio_num, NULL);
}
static bool isLow(uint8_t gpioNum) { return digitalRead(gpioNum) == LOW; }
static void cycleCallback(TimerHandle_t timer); // flood/drain timer callback

/////////////////////////////////////////////////////////////////////////////
// valve abstraction
class Valve {
public:
  static Adafruit_MotorShield AFMS; // featherwing motor driver
  Adafruit_DCMotor *myMotor;        // the motor abstraction instance

  static constexpr uint8_t NUM_VALVES = 3; // the set of valves
  static constexpr uint8_t PINS_ARITY = 4; // how many pins in a pinmap entry
  static uint8_t VALVE_PINS[][PINS_ARITY]; // pin defs
  static constexpr uint8_t TOP_IDX = 0, BOT_IDX = 1, TRIG_IDX = 2, MOT_IDX = 3;
  static Valve *valves[];                  // the valves themselves
  static int8_t pin2vnum(uint8_t pin);     // map GPIO pin to valve number
  static void begin();                     // set up statics, e.g. valves
  static void startCycling();              // kick off valve cycling
  static bool initialised;                 // has begin() run yet?
  static void gpioEventQueueTask(void *);  // drains the GPIO event queue

  uint8_t travelTop;            // GPIO of sensor switch at travel top
  uint8_t travelBottom;         // GPIO of sensor switch at travel bottom
  uint8_t trigger;              // GPIO of state change trigger switch
  uint8_t motorNumber;          // featherwing connection number
  uint8_t vnum;                 // cardinal number of this valve
  uint8_t floodMins = 20;       // how long to flood
  uint8_t drainMins = 40;       // how long to drain
  bool selfTest = false;        // run self test

  typedef enum { UNKNOWN, OPEN, CLOSED, OPENING, CLOSING } State; // lifecycle
  const char *stateName(); // usual C++ enum nightmare... maintain names below
  State state = UNKNOWN;   // state is not known when we boot up
  const uint8_t MOTOR_START_SPEED = 255; // what speed to start from

  Valve(uint8_t, uint8_t, uint8_t, uint8_t);    // construction
  void open();                                  // shift to draining
  void close();                                 // shift to filling
  void run(uint8_t);                            // run the motor
  void release();                               // stop the motor
  void complete();                              // finish a state change
  void flip();                                  // flip state
  void flip(bool, bool);                        // flip depending on switches
  bool atTop();                                 // travel switch pressed
  bool atBottom();                              // travel switch pressed
  bool triggered();                             // trigger switch pressed
  esp_err_t configGpio(uint64_t);               // set up GPIO
  bool handleGpioEvent(uint32_t);               // travel & trigger switches
  long lastTrigger = -1;                        // millis at previous trigger
  const int TRIGGER_DOUBLECLICK = 500;          // 2@ < than this: doubleclick
  const int TRIGGER_SINGLECLICK =  50;          // 2@ <= than this: singleclick
  void printme();                               // print state to serial
  int mins2ticks(int mins);                     // durations for cycle timers
};

uint8_t Valve::VALVE_PINS[NUM_VALVES][PINS_ARITY] { // gpio pinmap
  { GPIO_NUM_27, GPIO_NUM_33, GPIO_NUM_13, 3 }, //
  { GPIO_NUM_4,  GPIO_NUM_36, GPIO_NUM_39, 4 }, // top, bot, trig, motor #
  { GPIO_NUM_14, GPIO_NUM_32, GPIO_NUM_21, 1 }, //
};
Valve *Valve::valves[NUM_VALVES];

const char *Valve::stateName() {
  switch(state) {
    case UNKNOWN: return "UNKNOWN";
    case OPEN:    return "OPEN";
    case CLOSED:  return "CLOSED";
    case OPENING: return "OPENING";
    case CLOSING: return "CLOSING";
    default:      return "UNRECOGNISED";
  }
}

Adafruit_MotorShield Valve::AFMS; // featherwing motor driver
bool Valve::initialised = false;  // has begin() run yet?

int8_t Valve::pin2vnum(uint8_t pin) {
  for(int i = 0, j = 0; i < NUM_VALVES; ) {
    if(Valve::VALVE_PINS[i][j++] == pin) {
      return i;
    } else if(j == PINS_ARITY - 1 /* don't test the motor #! */) {
      i++;
      j = 0;
    }
  }
  return -1;
}

// set up statics, e.g. valves
void Valve::begin() {
  if(initialised) return;
  initialised = true;

  // install gpio isr service
  gpio_install_isr_service(0); // prints an error if already there; ignore!

  // create a queue to handle gpio event from isr
  gpio_evt_queue = xQueueCreate(10, sizeof(uint32_t));

  // the motor controller featherwing
  AFMS.begin();

  // the set of Valve objects
  for(int i = 0; i < NUM_VALVES; i++) {
    uint8_t
      top =  VALVE_PINS[i][TOP_IDX],
      bot =  VALVE_PINS[i][BOT_IDX],
      trig = VALVE_PINS[i][TRIG_IDX],
      mot =  VALVE_PINS[i][MOT_IDX];
    valves[i] = new Valve(top, bot, trig, mot);
  }

  // a task to drain the queue and call the valve handlers
  xTaskCreate(
    &gpioEventQueueTask,
    "gpioEventQueueTask",
    configMINIMAL_STACK_SIZE + 1024, // min is too small, triggers canary
    NULL, // we used to pass this here, but now we're static
    (configMAX_PRIORITIES - 1),
    NULL
  );

  // initiate the valve timer sequence
  startCycling();
} // Valve::begin()

// start flood/drain timers for the valves
void Valve::startCycling() {
  bool firstValve = true;
  int offset = 0; // time until start accumulator
  for(int i = 0; i < NUM_VALVES; i++) {
    Valve *v = valves[i];

    // the first valve we set to flood, the others to drain,
    // and each starts after the next flip for the previous
    int duration = -1; // mins until next state change for v
    if(firstValve) {
      firstValve = false;
      if(v->state != CLOSED) v->close();
      duration = v->mins2ticks(v->floodMins);
      offset += duration;
    } else {
      if(v->state != OPEN) v->open();
      offset += v->mins2ticks(v->floodMins);
      duration = offset;
    }

    // create and start repeat timer
    TimerHandle_t cycleTimer = xTimerCreate(
      "floodDrainTimer", // name
      duration,          // timer period in ticks
      pdTRUE,            // this is an autoreload timer
      (void *) v->vnum,  // pass the valve number through to the callback
      &cycleCallback     // the callback
    );
    if(cycleTimer == NULL)
      Serial.printf("cycleTimer creation failed, v%d!\n", v->vnum);
    else if(xTimerStart(cycleTimer, 0) != pdPASS)
      Serial.printf("cycleTimer start failed, v%d!\n", v->vnum);
    else
      Serial.printf(
        "cycleTimer started, duration %d, v%d\n", duration, v->vnum
      );
  }
}

Valve::Valve(
  uint8_t travelTop, uint8_t travelBottom, uint8_t trigger, uint8_t motorNumber
) {
  // the state sensing switches
  this->travelTop = travelTop;
  this->travelBottom = travelBottom;
  this->trigger = trigger;

  // which valve am I?
  vnum = pin2vnum(trigger);

  // the motor and driver
  this->motorNumber = motorNumber;
  myMotor = AFMS.getMotor(motorNumber);
  Serial.printf("v%d got motor for %d\n", vnum, motorNumber);

  // configure the switches to interrupt on falling edge
  configGpio(
    (1ULL << travelTop) | (1ULL << travelBottom) | (1ULL << trigger)
  );

  // hook up isr handlers
  gpio_isr_handler_add(
    (gpio_num_t) travelTop,    gpio_isr_handler, (void *) travelTop
  );
  gpio_isr_handler_add(
    (gpio_num_t) travelBottom, gpio_isr_handler, (void *) travelBottom
  );
  gpio_isr_handler_add(
    (gpio_num_t) trigger,      gpio_isr_handler, (void *) trigger
  );

  // if trigger is held at boot set off self-test
  if(triggered()) {
    Serial.printf("\n\n\n****\nv%d running self test...\n****\n\n\n", vnum);
    selfTest = true;
  }

  // make sure motor is off, and guess state
  release();
  state = UNKNOWN;
  bool t = atTop(), b = atBottom();
  if(! (t && b) ) { // both closed!
    if(t)
      state = OPEN;
    else if(b)
      state = CLOSED;
  }
} // Valve::Valve(...)

// gpio event queue drain task
void Valve::gpioEventQueueTask(void *pvParameter) {
  // we used to have a valve context: Valve *vp = (Valve *) pvParameter;
  // but now is static

  while(1) {
    // deal with events from the travel sensors and trigger switch
    uint32_t gpioNum;
    if(xQueueReceive(gpio_evt_queue, &gpioNum, (TickType_t) 0)) {
      Serial.printf("event on %d...", gpioNum);
      xQueueReset(gpio_evt_queue); // dump any pending events (debounce #1)

      // choose the valve based on pin2vnum(gpioNum)
      int8_t vn = pin2vnum(gpioNum);
      if(vn == -1) {  // phantom! a GPIO that isn't used by valve!
        Serial.printf("...unknown gpio[%d], ignoring\n", vn);
      } else {          // ask the valve to deal with the event
        if(Valve::valves[vn]->handleGpioEvent(gpioNum))
          Serial.printf("...gpio[%d] interrupt processed\n", gpioNum);
      }
    }

    WAIT_A_TICK // yield (at least one tick)
  }
} // Valve::gpioEventQueueTask()

// travel & trigger switch event handler; false on discard
bool Valve::handleGpioEvent(uint32_t gpioNum) {
  Serial.printf(" v%d handling event ", vnum);

  // verify switch state (debounce #2)
  bool
    top  = ( atTop()     && atTop()     ),
    bot  = ( atBottom()  && atBottom()  ),
    trig = ( triggered() && triggered() );
  if(! (top || bot || trig) ) { // dump this one (= a phantom event)
    Serial.printf("...%d discarded (1)\n", gpioNum);
    return false;
  }
  Serial.printf(
    "[top(%d)=%d,bot(%d)=%d,trig(%d)=%d,mot(%d)] ",
    travelTop, top, travelBottom, bot, trigger, trig, motorNumber
  );

  // if we're moving and have hit the end, stop
  if( (state == OPENING && top) || (state == CLOSING && bot) )
    complete();
  else if(trig) { // change state if appropriate
    long now = millis();

    // if the trigger is less recent (<) than doubleclick mS (e.g. 400)
    // and more recent than (>) singleclick mS (e.g. 50) then pause; else flip
    long sinceLast = now - lastTrigger;
    Serial.printf("sinceLast(%d), lastTrigger(%d) ", sinceLast, lastTrigger);
    if(sinceLast < TRIGGER_SINGLECLICK) {
      Serial.printf("...%d discarded (2)\n", gpioNum);
      return false; // (debounce #3)
    } else if(
      (lastTrigger > -1) &&
      ( sinceLast < TRIGGER_DOUBLECLICK ) &&
      ( sinceLast > TRIGGER_SINGLECLICK )
    ) {
      release(); // effectively a PAUSE state; single click resumes
      Serial.printf("...%d: pausing... \n", gpioNum);
    } else {
      flip(top, bot);
    }

    lastTrigger = now;
  }

  // Serial.printf(" v%d event on %d has been handled\n", vnum, gpioNum);
  return true;
} // Valve::handleGpioEvent()

// end a state change
void Valve::complete() {
  release();
  switch(state) {
    case OPENING: state = OPEN;   break;
    case CLOSING: state = CLOSED; break;
    default:                      break;
  }
  Serial.printf("state is %s; ", stateName());
}

// if we're draining, close; if we're filling, open
void Valve::flip() {
  flip(( atTop() && atTop() ), ( atBottom() && atBottom() )); // (debounce #4)
}

// if we're draining, close; if we're filling, open
void Valve::flip(bool top, bool bot) {
  switch(state) {
    case OPENING:
    case OPEN:    if(!bot) close(); break;
    case CLOSING:
    case CLOSED:  if(!top) open();  break;
    case UNKNOWN: if(!top) open();  break;
  }
  Serial.printf("state is %7s; ", stateName());
}

// read the switches
bool Valve::atTop()     { return isLow(travelTop);     }
bool Valve::atBottom()  { return isLow(travelBottom);  }
bool Valve::triggered() { return isLow(trigger);       }

// movement (motor) control
void Valve::open()  {
  state = OPENING; run(BACKWARD); WAIT_A_TICK run(BACKWARD);
}
void Valve::close() {
  state = CLOSING; run(FORWARD); WAIT_A_TICK  run(FORWARD);
}
void Valve::run(uint8_t direction) {
  myMotor->setSpeed(MOTOR_START_SPEED);
  myMotor->run(direction);
}
void Valve::release() {
  Serial.printf(" v%d release motor %d ", vnum, motorNumber);
  myMotor->run(RELEASE);
  WAIT_A_TICK
  myMotor->run(RELEASE);
}

// configure the switch pins (INPUT, falling edge interrupts)
esp_err_t Valve::configGpio(uint64_t pinsBitMask) {
  gpio_config_t io_conf;                        // params for switches
  io_conf.mode = GPIO_MODE_INPUT;               // set as input mode
  io_conf.pin_bit_mask = pinsBitMask;           // bit mask of pins to set
  io_conf.pull_up_en = GPIO_PULLUP_DISABLE;     // disable pull-up mode
  io_conf.pull_down_en = GPIO_PULLDOWN_DISABLE; // disable pull-down mode
  io_conf.intr_type = GPIO_INTR_NEGEDGE;        // interrupt on falling edge
  return gpio_config(&io_conf);                 // do the configuration
}

// print state to serial
void Valve::printme() {
  Serial.printf(
    "v%d (with motor %d) state = %7s; ", vnum, motorNumber, stateName()
  );
  Serial.printf(
    "top(%2d) bot(%2d) trig(%2d)\n", travelTop, travelBottom, trigger
  );
}

// the flood/drain timer interrupt callback
void cycleCallback(TimerHandle_t cycleTimer) {
  uint32_t vidx = (uint32_t) pvTimerGetTimerID(cycleTimer);
  Valve *v = Valve::valves[vidx];

  // calculate duration dependent on what will the next state be
  int duration = -1;
  if(v->state == Valve::OPEN) // we're open, flood comes next
    duration = v->mins2ticks(v->floodMins);
  else if(v->state == Valve::CLOSED) // we're closed, drain comes next
    duration = v->mins2ticks(v->drainMins);
  else // neither open nor closed, split the difference!
    duration = v->mins2ticks( (v->floodMins + v->drainMins) / 2);

  // change the timer period appropriate for the next cycle
  if(xTimerChangePeriod(cycleTimer, duration, 0) == pdFAIL)
    Serial.printf("cycleTimer duration change failed, v%d!\n", v->vnum);
  else
    Serial.printf("cycleTimer duration is now %d, v%d\n", duration, v->vnum);

  v->flip(); // flip to the next state
} // cycleCallback(TimerHandle_t)

// derive a duration for valve timers; a bit of a HACK means we can do the
// selfTest functionality here too by reducing the durations returned
int Valve::mins2ticks(int mins) {
  if(selfTest)
    return (mins * 1000) / portTICK_PERIOD_MS;
  else
    return (mins * 1000 * 60) / portTICK_PERIOD_MS;
}

/////////////////////////////////////////////////////////////////////////////
// arduino-land entry points
void setup() {
  Wire.setPins(SDA, SCL);
  Wire.begin();
  Serial.begin(115200);
  Serial.printf("wire: sda=%d scl=%d\n", SDA, SCL);
  Serial.println("arduino started");

  setup_rainmaker();

  // IDF version
  Serial.printf(
    "internal IDF version: %d.%d.%d\n",
    ESP_IDF_VERSION_MAJOR, ESP_IDF_VERSION_MINOR, ESP_IDF_VERSION_PATCH
  );

  // memory stats
  Serial.printf(
    "minimum free heap size: %d bytes\n\n", esp_get_minimum_free_heap_size()
  );

  // the flow control valves
  Valve::begin();
} // setup

void loop() {
  Valve *v0 = Valve::valves[0];
  Valve *v1 = Valve::valves[1];
  Valve *v2 = Valve::valves[2];
  v0->printme(); v1->printme(); v2->printme(); Serial.println();

  WAIT_SECS(5)
} // loop

/////////////////////////////////////////////////////////////////////////////
// temporary
void flipv() {
  Valve::valves[0]->flip();
}
