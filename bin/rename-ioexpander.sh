#!/bin/bash
# rename-ioexpander.sh

UNBIN="$(dirname $(realpath -e $0))"
echo $UNBIN
for d in ~/unphone/unzipped-libs ~/unphone/bin ~/unPhoneLibrary
do
  for f in \
    $(egrep -l 'IOExpander|IO_EXPANDER_INJECTED' \
      $(find $d -type f |egrep -v \
        'rename-ioexpander.sh|\/.git\/|archive|E-Ink|LongFileName|boardlibrary.pretty|\.pio\/'))
  do
    #echo $f
    #grep IOExpander $f
    #(cd $(dirname $f) && echo -n $(dirname $f) && git ls-files -t --stage $(basename $f))
    echo sed -i -e 's,IOExpander,unPhoneTCA,g' -e 's,IO_EXPANDER_INJECTED,UNPHONE_TCA_INJECTED,g' $f
    sed -i -e 's,IOExpander,unPhoneTCA,g' -e 's,IO_EXPANDER_INJECTED,UNPHONE_TCA_INJECTED,g' $f
    #(cd $(dirname $f) && git ls-files -t --stage $(basename $f) && git diff $(basename $f))
  done
done
